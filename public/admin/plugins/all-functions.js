$(function () {
    // Sidebar toggle behavior
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content, .page-content').toggleClass('active');
    });

    if ($(".datatable").length)
    {

    }
});

if ($.isFunction($.fn.lazyload))
{
    $(document).ready(function () {
        $('img.lazy').lazyload({
                                   effect: "fadeIn"
                               });
    });
}
// ------------------------------------------------
//  Owl Carousel v2
// ------------------------------------------------

if ($.isFunction($.fn.owlCarousel))
{
    $('.owl_slider').owlCarousel({
                                     margin        : 20,
                                     nav           : true,
                                     navText       : ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                                     animateOut    : 'slideOutUp',
                                     animateIn     : 'slideInUp',
                                     backAnimateOut: 'slideOutDown',
                                     backAnimateIn : 'slideInDown',
                                     dots          : false,
                                     responsive    : {
                                         0   : {
                                             items: 1,
                                             nav  : false,
                                             dots : true
                                         },
                                         600 : {
                                             items: 1,
                                         },
                                         1000: {
                                             items: 1,
                                         }
                                     }
                                 });
}
// ------------------------------------------------
//  Dense Retina Display
// ------------------------------------------------

if ($.isFunction($.fn.dense))
{
    $('.img-retina').dense();
}

// ------------------------------------------------
//  Bootstrap Select
// ------------------------------------------------

if ($.isFunction($.fn.selectpicker))
{
    $('.selectpicker').selectpicker({
                                        style: 'btn-default'
                                    });
}

////Prevent Scroll
var body = document.getElementsByTagName('body')[0];
$(".dropdown-menu").mouseover(function disableBodyScroll() {
    body.style.overflow = 'hidden';
});
$(".dropdown-menu").mouseout(function enableBodyScroll() {
    body.style.overflow = 'auto';
});

// ------------------------------------------------
//  Ellipsis text limit <p>
// ------------------------------------------------

if ($.isFunction($.fn.trunk8))
{
    $('.t8-ellipsis').trunk8({
                                 lines: 2
                             });
}