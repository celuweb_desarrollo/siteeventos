$(function () {

    
    if ($(".event_provider").length)
        $(".event_provider").multiSelect({
                                             selectableHeader: "<div class='custom-header'>Proveedores disponibles</div><input type='text' class='search-input form-control m-t-sm m-b-sm' autocomplete='off' placeholder='Buscar...'>",
                                             selectionHeader : "<div class='custom-header'>Proveedores seleccionados</div><input type='text' class='search-input form-control m-t-sm m-b-sm' autocomplete='off' placeholder='Buscar...'>",
                                             afterInit       : function (ms) {
                                                 var that                   = this,
                                                     $selectableSearch      = that.$selectableUl.prev(),
                                                     $selectionSearch       = that.$selectionUl.prev(),
                                                     selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                                     selectionSearchString  = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                                                 that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                                                             .on('keydown', function (e) {
                                                                                 if (e.which === 40)
                                                                                 {
                                                                                     that.$selectableUl.focus();
                                                                                     return false;
                                                                                 }
                                                                             });

                                                 that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                                                            .on('keydown', function (e) {
                                                                                if (e.which == 40)
                                                                                {
                                                                                    that.$selectionUl.focus();
                                                                                    return false;
                                                                                }
                                                                            });
                                             },
                                             afterSelect     : function () {
                                                 this.qs1.cache();
                                                 this.qs2.cache();
                                             },
                                             afterDeselect   : function () {
                                                 this.qs1.cache();
                                                 this.qs2.cache();
                                             }
                                         })

    if ($('#summernote').length)
    {
        $('#summernote').summernote({
                                        height : 250,
                                        toolbar: [
                                            ['style', ['bold', 'italic', 'underline', 'clear']],
                                            ['font', ['strikethrough', 'superscript', 'subscript']],
                                            ['fontsize', ['fontsize']],
                                            ['color', ['color']],
                                            ['para', ['ul', 'ol', 'paragraph']]
                                        ]
                                    });

        if (disable_summer)
            setTimeout(function () {
                $('#summernote').summernote('disable')
            }, 200)
    }

    /**
     * Permite validar que la fecha final no sea mayor a la fecha inicial
     */
    $('input[name="dat[final_date]"]').change(function () {

        var startDate = $('input[name="dat[start_date]"]').val();
            endDate   = this.value;

        if (Date.parse(endDate) < Date.parse(startDate)) {
            $(this).val('');
        }
    });
})

function add_provider()
{
    $.get(get_providers_url + '/' + event_id, {}, function (data) {

        $.confirm({
                      'title'       : 'Proveedor del evento',
                      'content'     : '<form action="" method="post" class="form-meetings overflow-h">\
                                            <input type="hidden" name="id" value="0" >\
                                            <div class="row">\
                                                <div class="form-group col-lg-12">\
                                                    <label>Proveedor</label>\
                                                    <select name="dat[provider_id]" class="form-control provider-select"></select>\
                                                </div>\
                                                <div class="form-group col-lg-6">\
                                                    <label>ID Reunión ZOOM</label>\
                                                    <input type="text" name="dat[zoom_code]" class="form-control" placeholder="ID Reunión"/>\
                                                </div>\
                                                <div class="form-group col-lg-6">\
                                                    <label>Número ZOOM</label>\
                                                    <input type="text" name="dat[zoom_number]" class="form-control" placeholder="Número Zoom"/>\
                                                </div>\
                                                <div class="form-group col-lg-12">\
                                                     <label>Región</label>\
                                                     <textarea maxlength="40" name="dat[region]" class="form-control" placeholder="..."/>\
                                                     <label class="text-muted"><small>Max: 40 Caracteres</small></label>\
                                                 </div>\
                                            </div>\
                                        </form>',
                      onContentReady: function () {
                          $.each(data, function (i, item) {
                              $('.provider-select').append($('<option>', {value: item.id, text: item.name}))
                          })
                      },
                      type          : 'blue',
                      columnClass   : 'col-md-4 col-md-offset-3',
                      buttons       : {
                          ok    : {
                              text    : 'Asignar Salas',
                              btnClass: 'btn-primary',
                              action  : function () {

                                  $.post(save_event_provider_url + event_id, $(".form-meetings").serialize(), function () {
                                      generate_datatable()
                                  })
                              }
                          },
                          cancel: {
                              text    : 'Cancelar',
                              btnClass: 'btn-default'
                          }
                      }
                  })
    })

}

function edit_provider(prov_id)
{
    $.get(get_provider_info_url + '/' + prov_id, {}, function (data) {

        $.get(get_providers_url + '/' + event_id, {}, function (providers) {

            $.confirm({
                      'title'       : 'Proveedor del evento',
                      'content'     : '<form action="" method="post" class="form-meetings overflow-h">\
                                        <input type="hidden" name="id" value="' + data.id + '" >\
                                        <div class="row">\
                                            <div class="form-group col-lg-12">\
                                                <label>Proveedor</label>\
                                                <select name="dat[provider_id]" class="form-control provider-select"></select>\
                                            </div>\
                                            <div class="form-group col-lg-6">\
                                                <label>ID Reunión ZOOM</label>\
                                                <input type="text" name="dat[zoom_code]" class="form-control" placeholder="ID Reunión" value="' + (data.zoom_code ? data.zoom_code : '') + '"/>\
                                            </div>\
                                            <div class="form-group col-lg-6">\
                                                <label>Número ZOOM</label>\
                                                <input type="text" name="dat[zoom_number]" class="form-control" placeholder="Número Zoom" value="' + (data.zoom_number ? data.zoom_number : '') + '"/>\
                                            </div>\
                                            <div class="form-group col-lg-12">\
                                              <label>Región</label>\
                                                <textarea maxlength="40" name="dat[region]" rows="2" class="form-control">' + (data.region ? data.region : '') + '</textarea>\
                                                <label class="text-muted"><small>Max: 40 Caracteres</small></label>\
                                            </div>\
                                        </div>\
                                    </form>',

                      onContentReady: function () {
                          $.each(providers, function (i, item) {
                              $('.provider-select').append($('<option>', {value: item.id, text: item.name, selected: data.provider_id == item.id}))
                          })
                      },
                      type          : 'blue',
                      columnClass   : 'col-md-4 col-md-offset-3',
                      buttons       : {
                          ok    : {
                              text    : 'Asignar Salas',
                              btnClass: 'btn-primary',
                              action  : function () {

                                  $.post(save_event_provider_url + event_id, $(".form-meetings").serialize(), function () {
                                      generate_datatable()
                                  })
                              }
                          },
                          cancel: {
                              text    : 'Cancelar',
                              btnClass: 'btn-default'
                          }
                      }
                  })
        })
    })
}