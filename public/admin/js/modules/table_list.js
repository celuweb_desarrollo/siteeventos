$(function () {
    if ($("#datatable").length)
        generate_datatable()
})

function delete_row(id)
{
    var delete_crtl = (typeof del_ctrl !== 'undefined') ? del_ctrl : 'delete';
    delete_list_function('¿Deseas inactivar este registro?', id, 'datatable', delete_crtl)
}

/**
 *
 * @param message
 * @param id
 * @param table_id
 * @param control
 */
function delete_list_function(message, id, table_id, control)
{
    $.confirm({
                  title       : 'Confirmación',
                  content     : message,
                  type        : 'red',
                  typeAnimated: true,
                  buttons     : {
                      accept   : {
                          text    : 'Si, Eliminar',
                          btnClass: 'btn-red',
                          action  : function () {
                              $.ajax({
                                         url   : control + '/' + id,
                                         method: 'DELETE'
                                     }).done(function (data) {
                                  show_message("Información", 'Registro inactivado con éxito', 'blue');
                                  generate_datatable()
                              })
                          }
                      }, cancel: {
                          text: 'No, Cancelar'
                      }
                  }
              })
}

function generate_datatable()
{
    $('#datatable').initDataTables(table_settings, {
        searching   : true,
        destroy     : true,
        paging      : true,
        lengthChange: true,
        pagingType  : "full_numbers",
        language    : {
            sSearch      : "<span>Buscar:</span> ",
            sInfo        : "Mostrando <span>_START_</span> a <span>_END_</span> de <span>_TOTAL_</span> Registros",
            sLengthMenu  : "<span>Mostrando</span> _MENU_ <span>Registros</span>",
            sInfoEmpty   : "Mostrando 0 a 0 de 0 Registros",
            sInfoFiltered: "(Filtrado de _MAX_ total)",
            sZeroRecords : "No se encontraron registros",
            oPaginate    : {"sFirst": "&laquo;", "sPrevious": "&lsaquo;", "sNext": "&rsaquo;", "sLast": "&raquo;"},
            sProcessing  : "Procesando..."
        },
        initComplete: function () {
            loadPopup()
        }
    })
}