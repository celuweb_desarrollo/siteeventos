function generate_report()
{
    var event_id = $("#event_id").val()

    if (event_id != '')
        location.href = generate_report_url + event_id
    else
        show_message('Selecciona un evento', 'Para generar el reporte debes seleccionar un evento', 'blue');
}