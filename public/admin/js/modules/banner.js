$(function () {
    if ($(".dropzone-slide").length)
    {
        var t    = "banner/s",
            size = typeof img_size !== 'undefined' ? img_size : '1150px ancho x 512px'

        Dropzone.autoDiscover = false

        var a = new Dropzone(".dropzone-slide", {
            url               : url_upload_file + banner_id,
            dictDefaultMessage: "Arrastra las imágenes a la zona punteada o presiona el botón para cargarlas.<br/>\
                <strong>Dimensiones recomendadas de " + size + " de alto, un formato .jpg, .png o .gif. y tamaño máximo 3MB</strong></br>\
                <button class='btn btn-info mt-2' type='button'><i class='fa fa-image'></i> Subir archivos</button>",
            parallelUploads   : true,
            maxFilesize       : 10,
            autoProcessQueue  : true,
            acceptedFiles     : 'image/*'
        });

        a.on("success", function (e, n) {

            $("#banner_id").val(n.banner_id)
            $(".sortable-image").append('<li id="image_' + n.id + '" data-id="' + n.id + '">\
                                            <div class="ImageWrapper chrome-fix">\
                                            <div class="caption-file">\
                                                <div class="center"><div class="content"></div></div>\
                                            </div>\
                                            <img src="' + upload_url + t + n.filename + '" class="img-responsive" width="150"/>\
                                            <div class="ImageOverlayH"></div>\
                                                <div class="Buttons StyleTi">\
                                                  <span class="WhiteRounded"><a onclick="editImg(' + n.id + ')"><i class="fa fa-pencil icon sinister"></i></a></span>\
                                                  <span class="WhiteRounded"><a href="' + upload_url + "banner/b" + n.filename + '" title="" class="popup-gallery"><i class="fa fa-search sinister icon"></i></a></span>\
                                                  <span class="WhiteRounded"><a onclick="deleteImg(' + n.id + ')"><i class="fa fa-trash sinister sinister-delete icon"></i></a></span>\
                                                </div>\
                                            </div>\
                                           </li>')
            loadPopup()
            a.removeFile(e)
            this.options.url = url_upload_file + n.banner_id

        }), loadPopup();

        var i    = document.getElementById("sortable_slide");
        sortable = Sortable.create(i, {
            forceFallback: true,
            onSort       : function () {
                $.post(url_sort, {data: sortable.toArray()}, function () {

                })
            }
        })
    }
})

function editImg(id)
{
    $.get(url_get_file + id, {}, function (data) {

        $.confirm({
                      title      : 'Información de la imagen',
                      content    : '<form action="" method="post" class="form-banner overflow-h">\
                                        <div class="row">\
                                            <div class="form-group col-lg-12">\
                                                <label>Descripción de la imagen</label>\
                                                <input type="text" name="dat[description]" class="form-control" placeholder="Descripción" value="' + (data.description ? data.description : '') + '"/>\
                                            </div>\
                                            <div class="form-group col-lg-12">\
                                                <label>Link del banner</label>\
                                                <input type="text" name="dat[link]" class="form-control" placeholder="Link" value="' + (data.link ? data.link : '') + '"/>\
                                            </div>\
                                        </div>\
                                      </form>',
                      type       : 'blue',
                      columnClass: 'col-md-4 col-md-offset-3',
                      buttons    : {
                          ok    : {
                              text    : 'Guardar información',
                              btnClass: 'btn-primary',
                              action  : function () {

                                  $.post(url_update + id, $(".form-banner").serialize(), function () {
                                      show_message('Información almacenada', 'La información de tu imagen ha sido almacenada satisfactoriamente.', 'green')
                                  })
                              }
                          },
                          cancel: {
                              text    : 'Cancelar',
                              btnClass: 'btn-default'
                          }
                      }
                  })
    })
}

function saveImg()
{
    var img_id = $("#image_id").val()

    $.post(url_edit + img_id, $("#form-img").serialize(), function () {
        $.post(url_sort + banner_id, {data: sortable.toArray()}, function () {
            show_message("Imagen almacenada", "La imagen ha sido almacenada con éxito", "dark"),
                $(".close").trigger("click")
        })
    })
}

function deleteImg(id)
{
    delete_function("Confirmación", "¿Deseas eliminar esta imagen?", "red", function () {
        $.ajax({
                   url   : url_delete_file + id,
                   method: "DELETE"
               }).done(function () {
            $("#image_" + id).remove();
        })
    })
}
