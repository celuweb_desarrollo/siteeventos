var modified = false;

$(function () {

    $("input, select, textarea").change(function () {
        modified = true;
    });

    if (message)
        show_message('Información', message, 'green')

    if ($(".detail-form").length)
        $(".detail-form .btn").addClass('btn-md btn-primary').text('Guardar');

    if ($(".update-form").length)
        $(".update-form button").addClass('btn-md btn-primary');

    $("#file").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    })


    if(jQuery.validator){
        /**
         * JQuery vaidator
         */
         jQuery.validator.setDefaults({
             ignore: [],
             rules          : {
                 email                 : {
                     required : true,
                     email    : true,
                     remote   : {
                         url  : (typeof url_verify_email != 'undefined'? url_verify_email: '')  + '/' + (typeof object_id != 'undefined'? object_id:''),
                         type : "post",
                         data : {
                             '_token' : $('meta[name="csrf-token"]').attr('content'),
                         },
                     }
                 }
             },
             onfocusout: function (e) {
                 this.element(e);
             },
             onkeyup: false,
             highlight: function (element) {
                 $('.btn-submit').prop('disabled', false);
                 jQuery(element).closest('.form-control').addClass('is-invalid');
             },
             unhighlight: function (element) {
                 jQuery(element).closest('.form-control').removeClass('is-invalid');
             },
             errorElement: 'div',
             errorClass: 'invalid-feedback',
             errorPlacement: function (error, element) {
                 if(element.hasClass('select2') && element.next('.select2-container').length) {
                     error.insertAfter(element.next('.select2-container'));
                 } else if (element.parent('.input-group').length) {
                     error.insertAfter(element.parent());
                 }else if (element.parent('.input-group-prepend').length) {
                     $(element).siblings(".invalid-feedback").append(error);
                 } else {
                     error.insertAfter(element);
                 }
             },
             messages: {
                 accept: "Por favor, introduzca un valor con una extensión válida.",
                 email: {
                     required: "Este campo es obligatorio",
                     remote: 'El Email ya está en uso.',
                 },
             }
        });
    }

    if($('.btn-submit').length){
        /**
         * Acción para almancenar un registro.
         */
        $('.btn-submit').on('click', function(e){
            e.preventDefault();
            $(this).attr('disabled', true);

            if($('.form').valid()){
                modified = false;
                $('.form').submit();
            }else{
                $(this).attr('disabled', false);
            }
        });
    }

    if($('.times').length){
        /** 
         * Datetimepicker
         */
        $('.times').datetimepicker({
           format: 'LT'
       });
    }

    /**
     * Validador mensaje de salida
     */
    window.addEventListener("beforeunload", function (e) {

        var confirmationMessage = "\o/";

        if( $('.before-unload').length &&  modified) {
            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage;
        }
    });
})

/**
 * Muestra un message de alerta
 * @param titulo
 * @param message
 * @param type
 */
function show_message(title, msg, color)
{
    $.alert({
                title          : title,
                content        : msg,
                confirmButton  : 'Ok',
                cancelButton   : 'Cancel',
                theme          : 'white',
                keyboardEnabled: true,
                type           : color,
                typeAnimate    : true,
                buttons        : {
                    ok: {
                        text    : 'Cerrar',
                        btnClass: 'btn-' + color
                    }
                }
            });
}

function delete_function(title, msg, color, callback)
{
    $.confirm({
                  title       : title,
                  content     : msg,
                  type        : color,
                  theme       : 'material',
                  typeAnimated: true,
                  buttons     : {
                      accept   : {
                          text    : 'Si, Eliminar',
                          btnClass: 'btn-danger',
                          action  : function () {
                              callback();
                          }
                      }, cancel: {
                          text: 'No, Cancelar'
                      }
                  }
              });
}

function loadPopup()
{
    $('.popup-gallery').magnificPopup({
                                          type     : 'image',
                                          tLoading : 'Cargando imagen...',
                                          mainClass: 'mfp-fade',
                                          gallery  : {
                                              enabled           : true,
                                              navigateByImgClick: true,
                                              preload           : [0, 1] // Will preload 0 - before current, and 1 after the current image
                                          },
                                          image    : {
                                              tError  : '<a href="%url%">La imagen</a> no pudo ser cargada.',
                                              titleSrc: function (item) {
                                                  return item.el.attr('title') + '<small>por Coopidrogas</small>';
                                              }
                                          }
                                      });
}


function closeSession()
{
    $.confirm({
                  title       : 'Confirmación',
                  content     : '¿Estás seguro que deseas cerrar sesión?',
                  type        : 'orange',
                  theme       : 'material',
                  typeAnimated: true,
                  buttons     : {
                      accept   : {
                          text    : '<strong>Si, Continuar</strong>',
                          btnClass: 'btn-warning',
                          action  : function () {
                              window.location.href =  admin_logout;
                          }
                      }, cancel: {
                          text: 'No, Cancelar'
                      }
                  }
              });
}



