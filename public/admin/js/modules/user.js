$(function () {

    if($('.form-user').length)
    {
        jQuery('.form-user').validate({
              rules          : {
                  password              : {
                      minlength : 6
                  },
                  password_confirm : {
                      minlength : 6,
                      equalTo   : "#password"
                  },
                  email                 : {
                      required : true,
                      email    : true,
                      remote   : {
                          url  : url_verify_email + '/' + object_id,
                          type : "post",
                          data : {
                              '_token' : $('meta[name="csrf-token"]').attr('content'),
                          },
                      }
                  }
              },
              onfocusout     : function (e) {
                  this.element(e);
              },
              onkeyup        : false,
              highlight      : function (element) {
                  $('#btn-submit-user').attr('disabled', false);
                  jQuery(element).closest('.form-control').addClass('is-invalid');
              },
              unhighlight    : function (element) {
                  jQuery(element).closest('.form-control').removeClass('is-invalid');
              },
              errorElement   : 'div',
              errorClass     : 'invalid-feedback',
              errorPlacement : function (error, element) {
                  if (element.hasClass('select2') && element.next('.select2-container').length)
                  {
                      error.insertAfter(element.next('.select2-container'));
                  } else if (element.parent('.input-group').length)
                  {
                      error.insertAfter(element.parent());
                  } else if (element.parent('.input-group-prepend').length)
                  {
                      $(element).siblings(".invalid-feedback").append(error);
                  } else
                  {
                      error.insertAfter(element);
                  }
              },
        });
    }

    if($('.btn-submit-user').length){

        /**
         * Boton submit para enviar el formulario.
         */
        $('.btn-submit-user').on('click', function(e){
            e.preventDefault();
            $(this).attr('disabled', true);
            if($('.form-user').valid()){
                modified = false;
                $('.form-user').submit();
            }else{
                $(this).attr('disabled', false);
            }
        });
    }
});
