function filter_provider(event) {

    id = event.options[event.selectedIndex].value;

    if (id != 0) {

        $.ajax({
           url: url_offer_provider,
           method: "POST",
           data: {
               id: id
           },
           dataType: "json",
           success: function(data) {

               results = data;
               var result = '<option value="0" selected>Seleccione un proveedor</option>';

               for (var i = 0; i < results.length; i++) {
                   result += '<option value="' + results[i]['id'] + '">' + results[i]['name'] + '</option>';
               }

               $('#selectProvider').html(result);
           }
       });
    }
}