// Execute this command on console npm install gulp gulp-sass gulp-uglifyjs gulp-concat gulp-cssnano gulp-notify gulp-plumber gulp-jsvalidate gulp-cache

const gulp       = require('gulp');
const sass       = require('gulp-sass');
const uglify     = require('gulp-uglifyjs');
const concat     = require('gulp-concat');
const cssnano    = require('gulp-cssnano');
const notify     = require('gulp-notify');
const plumber    = require('gulp-plumber');
const jsValidate = require('gulp-jsvalidate');
const cache      = require('gulp-cache');

var onError = function (err) {
    notify({
               title  : 'Gulp Task Error',
               message: 'Check the console.'
           }).write(err);
    console.log(err.toString());
    this.emit('end');
};

//------------------
// JS COMPILER
//------------------
gulp.task('scripts', function () {

    return gulp.src([
                        'js/modules/*.js'
                    ])
               .pipe(concat('scripts.min.js'))
               .pipe(jsValidate())
               .on("error", notify.onError(function (error) {
                   return error.message;
               }))
               .pipe(uglify())
               .pipe(gulp.dest('./js'))
               .pipe(notify({
                    message: 'JavaScript complete'
                }));
});

/*
gulp.task('vendor', function () {
    return gulp.src([
                        'plugins/vendors/jquery.min.js',                //  jQuery              v2.2.4
                        'plugins/vendors/jquery-quicksearch.js',        //  jQuery Quicksearch             v2.2.4
                        'plugins/vendors/popper.js',                    //  Popper              v1.14.3
                        'plugins/vendors/bootstrap.min.js',             //  Bootstrap           v4.3.1
                        'plugins/libs/owl.carousel.js',                 //  Owl Carousel        v2.3.4
                        'plugins/libs/header-fix.js',                   //  Header-fix          v1.0
                        'plugins/libs/html5.js',                        //  HTML5 Shiv          v3.7.3
                        'plugins/libs/slicknav.js',                     //  Responsive Menu     v1.0.10
                        'plugins/libs/dense-retina.js',                 //  Dense               v0.0.1
                        'plugins/libs/parallax.js',                     //  Parallax
                        'plugins/libs/waves.js',                        //  Waves
                        'plugins/libs/slidebars.js',                    //  TouchSwipe          v1.6.17
                        'plugins/libs/touchspin.js',                    //  Bootstrap TouchSpin
                        'plugins/libs/fill.box.js',                     //  Fill Box image
                        'plugins/libs/trunk8.js',                       //  trunk8 v1.3.2
                        'plugins/libs/bootstrap-select.js',             //  Bootstrap Select    v1.12.2
                        'plugins/libs/colourbrightness.js',             //  Colour Detect: 1.2
                        'plugins/libs/lightgallery-all.js',             //  LightGallery
                        'plugins/libs/jquery.mousewheel.min.js',        //  LightGallery Mousewhel
                        'plugins/libs/datatables/datatables.min.js',    //  Datatables
                        'plugins/libs/jquery-confirm/dist/jquery-confirm.min.js',  //  Jquery Confirm
                        'plugins/libs/jquery-validation/jquery.validate.js',  //  Jquery validation
                        'plugins/libs/jquery-validation/additional-methods.js',  //  Jquery Confirm
                        'plugins/libs/jquery-validation/localization/messages_es.min.js',
                        'plugins/all-functions.js',                      //  Plugins Functions
                        'plugins/libs/dropzone/dropzone.min.js',         //  DropZone
                        'plugins/libs/jquery.magnific-popup.min.js',     //  Magnific Popup
                        'plugins/libs/sortablejs/Sortable.js',           //  Sortable
                    ])
               .pipe(concat('vendor.min.js'))
               .pipe(jsValidate())
               .on("error", notify.onError(function (error) {
                   return error.message;
               }))
               .pipe(uglify())
               .pipe(gulp.dest('./js'))
               .pipe(notify({
                                message: 'JavaScript complete'
                            }));
});*/

// ------------------------------------------------
//  FONT SASS
// ------------------------------------------------

var fontFiles = 'sass/myFonts.scss',
    fontDest  = 'css/';

gulp.task('fonts', function () {
    return gulp.src(fontFiles)
               .pipe(plumber({errorHandle: onError}))
               .pipe(sass())
               .on('error', onError)
               .pipe(cssnano({
                                 discardComments: {removeAll: true},
                                 discardUnused  : {fontFace: false}
                             }))
               .pipe(concat('fonts.min.css'))
               .pipe(gulp.dest(fontDest))
               .pipe(notify({
                                message: 'SASS complete'
                            }));
});

//--------------
// SASS COMPILER
//--------------
var sassFiles = 'sass/style.scss',
    cssDest   = 'css/';

gulp.task('sass', function () {
    return gulp.src(sassFiles)
               .pipe(plumber({errorHandle: onError}))
               .pipe(sass())
               .on('error', onError)
               .pipe(cssnano({
                                 discardComments: {removeAll: true}
                             }))
               .pipe(concat('style.min.css'))
               .pipe(gulp.dest(cssDest))
               .pipe(notify({
                                message: 'SASS complete'
                            }))
});

gulp.task('clear', function (done) {
    return cache.clearAll(done);
});

// ------------------------------------------------
//  gulp task watcher
// ------------------------------------------------

gulp.task('default', function () {
    gulp.watch('sass/**/*.scss', gulp.series('sass'));
    //gulp.watch(['plugins/**/*.js'], gulp.series('vendor'));
    gulp.watch(['js/modules/*.js'], gulp.series('scripts'));
    gulp.watch('sass/fonts/*.scss', gulp.series('fonts'));
});
