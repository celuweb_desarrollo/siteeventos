var API_KEY    = "v03maXFcSp2xeMDp6AyzTA";
var API_SECRET = "spsvslgXXXlU9P43YXr88MdlrcFZDauHG3vJ";

window.addEventListener('DOMContentLoaded', function (event) {
    websdkready();
});

function websdkready()
{
    var testTool = window.testTool;

    if (testTool.isMobileDevice())
        vConsole = new VConsole();

    ZoomMtg.preLoadWasm();

    document.getElementById("display_name").value   = testTool.getCookie(
        "display_name"
    );
    document.getElementById("meeting_number").value = testTool.getCookie(
        "meeting_number"
    );
    document.getElementById("meeting_pwd").value    = testTool.getCookie(
        "meeting_pwd"
    );
    if (testTool.getCookie("meeting_lang"))
        document.getElementById("meeting_lang").value = testTool.getCookie(
            "meeting_lang"
        );

    document
        .getElementById("meeting_number")
        .addEventListener("input", function (e) {
            var tmpMn = e.target.value.replace(/([^0-9])+/i, "");
            if (tmpMn.match(/([0-9]{9,11})/))
                tmpMn = tmpMn.match(/([0-9]{9,11})/)[1];

            var tmpPwd = e.target.value.match(/pwd=([\d,\w]+)/);

            if (tmpPwd)
            {
                document.getElementById("meeting_pwd").value = tmpPwd[1];
                testTool.setCookie("meeting_pwd", tmpPwd[1]);
            }
            document.getElementById("meeting_number").value = tmpMn;
            testTool.setCookie(
                "meeting_number",
                document.getElementById("meeting_number").value
            );
        })
}

function joinZoom()
{
    var meetingConfig = testTool.getMeetingConfig();
    if (!meetingConfig.mn || !meetingConfig.name)
    {
        alert("Meeting number or username is empty");
        return false;
    }

    testTool.setCookie("meeting_number", meetingConfig.mn);
    testTool.setCookie("meeting_pwd", meetingConfig.pwd);

    var path = window.location.href.includes("/coopidrogas-event/public") ? "/coopidrogas-event/public" : "";

    ZoomMtg.generateSignature({
                                  meetingNumber: meetingConfig.mn,
                                  apiKey       : API_KEY,
                                  apiSecret    : API_SECRET,
                                  role         : meetingConfig.role,
                                  success      : function (res) {
                                      meetingConfig.signature = res.result;
                                      meetingConfig.apiKey    = API_KEY;
                                      var joinUrl             = path + "/zoom/join/1/meeting?" + testTool.serialize(meetingConfig);
                                      window.open(joinUrl, '_self');
                                  },
                              });
}