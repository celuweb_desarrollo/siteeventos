window.addEventListener('DOMContentLoaded', function (event) {
    websdkMeetingready();
});

window.addEventListener("beforeunload", function (e) {
    localStorage.setItem("DATA_ZOOM_FINISH_HOUR", moment().format('YYYY-MM-DD HH:mm:ss'));
    return true
});



function websdkMeetingready()
{

    var testTool = window.testTool;
    var path     = window.location.href.includes("/coopidrogas-event/public") ? "/coopidrogas-event/public" : "";

    var user_zoom        = localStorage.getItem("DATA_ZOOM_CODE");
    var event_zoom       = localStorage.getItem("DATA_ZOOM_EVENT");
    var provider_zoom    = localStorage.getItem("DATA_ZOOM_PROVIDER");
    var ev_provider_zoom = localStorage.getItem("DATA_ZOOM_EVENT_PROVIDER");

    // get meeting args from url
    var tmpArgs       = testTool.parseQuery();
    var meetingConfig = {
        apiKey       : tmpArgs.apiKey,
        meetingNumber: tmpArgs.mn,
        userName     : (function () {
            if (tmpArgs.name)
            {
                try
                {
                    return testTool.b64DecodeUnicode(tmpArgs.name);
                } catch (e)
                {
                    return tmpArgs.name;
                }
            }
            return (
                tmpArgs.version + testTool.detectOS() + testTool.getBrowserInfo()
            );
        })(),
        passWord     : tmpArgs.pwd,
        leaveUrl     : path + "/asociado/proveedores?ev=" + event_zoom + "&pr=" + provider_zoom + "&us=" + user_zoom + "&ev_pr=" + ev_provider_zoom,
        role         : parseInt(tmpArgs.role, 10),
        userEmail    : (function () {
            try
            {
                return testTool.b64DecodeUnicode(tmpArgs.email);
            } catch (e)
            {
                return tmpArgs.email;
            }
        })(),
        lang         : tmpArgs.lang,
        signature    : tmpArgs.signature || "",
        china        : tmpArgs.china === "1",
    };

    // a tool use debug mobile device
    if (testTool.isMobileDevice())
        vConsole = new VConsole();

    //console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

    // it's option if you want to change the WebSDK dependency link resources. setZoomJSLib must be run at first
    // ZoomMtg.setZoomJSLib("https://source.zoom.us/1.9.1/lib", "/av"); // CDN version defaul
    if (meetingConfig.china)
        ZoomMtg.setZoomJSLib("https://jssdk.zoomus.cn/1.9.1/lib", "/av"); // china cdn option
    ZoomMtg.preLoadWasm();
    ZoomMtg.prepareJssdk();

    function beginJoin(signature)
    {
        ZoomMtg.init({
                         leaveUrl   : meetingConfig.leaveUrl,
                         webEndpoint: meetingConfig.webEndpoint,
                         success    : function () {

                             ZoomMtg.i18n.load(meetingConfig.lang);
                             ZoomMtg.i18n.reload(meetingConfig.lang);
                             ZoomMtg.join({
                                              meetingNumber: meetingConfig.meetingNumber,
                                              userName     : meetingConfig.userName,
                                              signature    : signature,
                                              apiKey       : meetingConfig.apiKey,
                                              userEmail    : meetingConfig.userEmail,
                                              passWord     : meetingConfig.passWord,
                                              success      : function (res) {

                                                  ZoomMtg.getAttendeeslist({});
                                                  ZoomMtg.getCurrentUser({
                                                                             success: function (res) {
                                                                                 console.log("success getCurrentUser", res.result.currentUser);
                                                                             },
                                                                         });
                                              },
                                              error        : function (res) {
                                                  console.log(res);
                                              },
                                          });
                         },
                         error      : function (res) {
                             console.log(res);
                         },
                     });

        ZoomMtg.inMeetingServiceListener('onUserJoin', function (data) {

        });

        ZoomMtg.inMeetingServiceListener('onUserLeave', function (data) {
            //console.log('inMeetingServiceListener onUserLeave', data);
        });

        ZoomMtg.inMeetingServiceListener('onUserIsInWaitingRoom', function (data) {
            //console.log('inMeetingServiceListener onUserIsInWaitingRoom', data);
        });

        ZoomMtg.inMeetingServiceListener('onMeetingStatus', function (data) {
            //console.log('inMeetingServiceListener onMeetingStatus', data);
        });
    }

    beginJoin(meetingConfig.signature);
};
