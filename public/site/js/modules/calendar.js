"use strict";

$(function () {

    var Calendar   = FullCalendar.Calendar;
    var calendarEl = document.getElementById('fullcalendar');

    if($('#fullcalendar').length){

        $.get(urlGetEvents, function(events){

            var calendar = new Calendar(calendarEl, {
                headerToolbar: {
                    left  : 'title',
                    center: '',
                    right : 'prev,next',
                },
                themeSystem: 'bootstrap',
                displayEventTime : false,
                events: events,
                editable  : false,
                droppable : true,
                drop      : function(info) {
                    if (checkbox.checked) {
                        info.draggedEl.parentNode.removeChild(info.draggedEl);
                    }
                },
                eventClick:  function(info) {

                    checkTermsAndConditions(info.event.id);
                },
            });

            calendar.setOption('locale', 'es');
            calendar.render();
            // $('#calendar').fullCalendar();
        });
    }

    /**
     * Botón aceptar termnos y condiciones
     */
    $('#btn_accept').on('click', function(){

        $('.accept-terms').show();
        $(this).prop('disabled',true);

        $.post(saveTerms, { 'event_id': $(this).attr('data-id') }).done(function (response) {

            if(response > 0){
                logUserAndEvent(response);
            }
        });
    });

    $('.close-modal').on('click', function () {
        $('#spinner').hide();
        $('#modal_event').modal('hide');
    });

});

/**
 * Permite almacenar el log de usuario y evento
 * @param event_id
 */
function logUserAndEvent(event_id)
{
    $.post(logUserEvent, { 'event_id': event_id }).done(function (response) {
        if(response > 0){
            $('.accept-terms').hide();
            $('#spinner').hide();
            window.location.href = urlMenuEvents;
        }
    });
}

function closeSession()
{
    $.confirm({
                  title       : 'Confirmación',
                  content     : '¿Estas seguro que deseas Cerrar Sesión?',
                  type        : 'danger',
                  theme       : 'material',
                  typeAnimated: true,
                  buttons     : {
                      accept   : {
                          text    : '<strong>Si, Continuar</strong>',
                          btnClass: 'btn-warning',
                          action  : function () {
                              if(typeSession == 'admin'){
                                  window.location.href =  '/admin/logout';
                              }else{

                              }
                          }
                      }, cancel: {
                          text: 'No, Cancelar'
                      }
                  }
              });
}


function checkTermsAndConditions(id)
{
    $('#spinner').show();

    $.get(getEventData,{'event_id':id }, function(data){

        if(data.status){

            $('#title').html(data.title);
            $('#description').html(data.description);
            $('#start_date').text(data.start_date);
            $('#final_date').text(data.final_date);
            $('.download').attr('href', '/upload/terms/'+ data.fileurl);
            $('#btn_accept').attr('data-id',  data.id);

            if(data.fileurl != null){
                $('.div-download').show();
                $('.download').attr('href', '/upload/terms/'+ data.fileurl);

            }else{
                $('.div-download').hide();
            }
            $('.accept-terms').hide();
            $('#modal_event').modal('show');

        }else{

            logUserAndEvent(data.id);
        }
    });
}

