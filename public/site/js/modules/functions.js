$(function () {
    var ev_provider_zoom_hour = localStorage.getItem("DATA_ZOOM_FINISH_HOUR")

    if (typeof ev_provider_zoom_hour !== 'undefined' && ev_provider_zoom_hour != null && ev_provider_zoom_hour != 'null')
        $.post(finish_hour_url, {
            finish_hour     : ev_provider_zoom_hour,
            zoom_event      : localStorage.getItem("DATA_ZOOM_EVENT"),
            zoom_provider   : localStorage.getItem("DATA_ZOOM_PROVIDER"),
            zoom_ev_provider: localStorage.getItem("DATA_ZOOM_EVENT_PROVIDER"),
        }, function () {
            localStorage.setItem("DATA_ZOOM_FINISH_HOUR", null)
        })

    if (premiaton_banner && parseInt(['menu_events', 'associate_providers'].indexOf(current_route)) >= 0)
    {
        $('body').append('<div class="hidden"><a href="' + url_path + 'premiaton/' + premiaton_banner + '"  title="Ganadores Premiaton" class="image-link "></div>')

        $('.image-link').magnificPopup({
                                           type     : 'image',
                                           tLoading : 'Cargando imagen...',
                                           mainClass: 'mfp-fade',
                                           image    : {
                                               tError  : '<a href="%url%">La imagen</a> no pudo ser cargada.',
                                               titleSrc: function (item) {
                                                   return item.el.attr('title') + '<small>por Coopidrogas</small>';
                                               }
                                           },
                                           callbacks: {
                                               open: function () {
                                                   $.magnificPopup.instance.close = function () {
                                                       $.magnificPopup.proto.close.call(this);
                                                       show_incentive_banner()
                                                   }
                                               }
                                           }
                                       });

        $('.image-link').trigger('click')

        setTimeout(function () {
            $(".mfp-img").on('click', function () {
                location.href = '/asociado/premiaton'
            })
        }, 300)

    } else
        show_incentive_banner()

    var user_zoom        = localStorage.getItem("DATA_ZOOM_CODE");
    var event_zoom       = localStorage.getItem("DATA_ZOOM_EVENT");
    var provider_zoom    = localStorage.getItem("DATA_ZOOM_PROVIDER");
    var ev_provider_zoom = localStorage.getItem("DATA_ZOOM_EVENT_PROVIDER");

})

function show_incentive_banner()
{
    if (incentive_banner && parseInt(['menu_events', 'associate_providers'].indexOf(current_route)) >= 0)
    {
        $('body').append('<div class="hidden"><a href="' + url_path + 'incentive/' + incentive_banner + '"  title="Incentivos proveedores" class="image-link_provider"></div>')

        $('.image-link_provider').magnificPopup({
                                                    type     : 'image',
                                                    tLoading : 'Cargando imagen...',
                                                    mainClass: 'mfp-fade',
                                                    image    : {
                                                        tError  : '<a href="%url%">La imagen</a> no pudo ser cargada.',
                                                        titleSrc: function (item) {
                                                            return item.el.attr('title') + '<small>por Coopidrogas</small>';
                                                        }
                                                    },
                                                    callbacks: {
                                                        open: function () {
                                                            $.magnificPopup.instance.close = function () {
                                                                $.magnificPopup.proto.close.call(this)
                                                            }
                                                        }
                                                    }
                                                });

        $('.image-link_provider').trigger('click')

        setTimeout(function () {
            $(".mfp-img").on('click', function () {
                location.href = '/asociado/incentivos'
            })
        }, 300)
    }
}

/**
 * Muestra un message de alerta
 * @param titulo
 * @param message
 * @param type
 */
function show_message(title, msg, color)
{
    $.alert({
                title          : title,
                content        : msg,
                confirmButton  : 'Ok',
                cancelButton   : 'Cancel',
                theme          : 'white',
                keyboardEnabled: true,
                type           : color,
                typeAnimate    : true,
                buttons        : {
                    ok: {
                        text    : 'Cerrar',
                        btnClass: 'btn-' + color
                    }
                }
            });
}

function closeSessionAssociated()
{
    $.confirm({
                  title       : 'Confirmación',
                  content     : '¿Estás seguro que deseas cerrar sesión?',
                  type        : 'orange',
                  theme       : 'material',
                  typeAnimated: true,
                  buttons     : {
                      accept   : {
                          text    : '<strong>Si, Continuar</strong>',
                          btnClass: 'btn-warning',
                          action  : function () {
                              localStorage.clear()
                              window.location.href = associated_logout;
                          }
                      }, cancel: {
                          text: 'No, Cancelar'
                      }
                  }
              })
}