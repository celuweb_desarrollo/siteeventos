$(function () {

    if (typeof share_provider_id !== 'undefined' && share_provider_id)
    {
        $(".provider_link_" + share_provider_id).trigger('click')
        var modal = $('#provider-details')
        load_provider_info(event_id, share_provider_id, modal)
    }

    $('#provider-details').on('show.bs.modal', function (event) {

        var button      = $(event.relatedTarget)
        var provider_id = button.data('provider')
        var modal       = $(this)

        $(".preview-load").removeClass('hidden')
        $(".preview-content").addClass('hidden')

        load_provider_info(event_id, provider_id, modal)
    })

    if ($('.typeahead').length)
        $.get(get_providers_name_url, {}, function (data) {

            var providers = data
            var providers = new Bloodhound({
                                               datumTokenizer: Bloodhound.tokenizers.whitespace,
                                               queryTokenizer: Bloodhound.tokenizers.whitespace,
                                               local         : providers
                                           })

            $('.typeahead').typeahead({
                                          hint     : true,
                                          highlight: true,
                                          minLength: 1
                                      },
                                      {
                                          name  : 'providers',
                                          source: providers
                                      });

        })
})

function load_provider_info(event_id, ev_provider_id, modal)
{
    var path = window.location.href.includes("/coopidrogas-event/public") ? "/coopidrogas-event/public" : ""

    $.get(path + '/api/provider/' + event_id + '/' + ev_provider_id, {}, function (data) {

        if (data)
        {
            $(".preview-load").addClass('hidden')
            $(".preview-content").removeClass('hidden')

            var provider   = data.provider
            var event      = data.event
            var image_path = provider.image ? path + "/upload/logos/" + provider.image : path + "/upload/default/no-image.jpg"

            modal.find('.detail-provider-name').html('<strong>Proveedor:</strong> ' + provider.name)
            modal.find('.detail-provider-phone').html('<strong>Contacto:</strong> ' + provider.phone)
            modal.find('.detail-provider-image').attr("src", image_path)
            modal.find('.detail-provider-zoom').attr("href", path + "/zoom/join/" + ev_provider_id)
            modal.find('.detail-provider-urlzoom').text('https://zoom.us/j/' + provider.zoom_code)
            modal.find('.detail-provider-codezoom').html('<strong>ID REUNIÓN: </strong>' + provider.zoom_code)

            var trasnfer     = data.trasnfer
            var htmlTransfer = ''
            var share_text   = 'Proveedor: ' + provider.name + '\nContacto: ' + provider.phone

            if (trasnfer.length)
            {
                share_text += '\n\nTransferencistas a nivel nacional\n\n'

                modal.find(".transferreds").removeClass('hidden')
                htmlTransfer = '<table class="table-transfers" border="0">'

                trasnfer.forEach(function (element, i) {
                    htmlTransfer += '<tr><td><strong class="text-capitalize">' + element.transfer + '</strong></td><td>' + element.phone + '</td></tr>'
                    share_text += element.transfer + ' : ' + element.phone + '\n'
                })

                htmlTransfer += '</table>'
            } else
                modal.find(".transferreds").addClass('hidden')

            modal.find('.detail-provider-transfer').html(htmlTransfer)
            modal.find('.detail-links .phone').prop('href', 'tel:' + provider.phone)
            modal.find('.detail-links .contact').prop('href', 'mailto:' + provider.email + '?subject=' + event.title)

            modal.find('.detail-links .share').unbind('click')
            modal.find('.detail-links .share').on('click', function (e) {

                e.preventDefault();
                document.addEventListener('copy', function (e) {
                    e.clipboardData.setData('text/plain', share_text)
                    e.preventDefault();
                }, true);

                document.execCommand('copy')

                show_message('Enlace copiado', 'La información del proveedor ha sido copiada a tu portapapeles.', 'green')
            })
        }
    })
}
