$(function () {
    if ($("#fullcalendar").length)
    {
        var calendarEl = document.getElementById('fullcalendar');
        var calendar   = new FullCalendar.Calendar(calendarEl, {
            themeSystem: 'bootstrap'
        });
        calendar.render();
    }

})

if ($.isFunction($.fn.lazyload))
{
    $(document).ready(function () {
        $('img.lazy').lazyload({
                                   effect: "fadeIn"
                               });
    });
}

// ------------------------------------------------
//  Owl Carousel v2
// ------------------------------------------------

if ($.isFunction($.fn.owlCarousel))
{
    $('.owl_slider').owlCarousel({
                                     margin         : 0,
                                     nav            : true,
                                     navText        : ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                                     animateOut     : 'fadeOut',
                                     // animateIn      : 'slideInUp',
                                     // backAnimateOut : 'slideOutDown',
                                     // backAnimateIn  : 'slideInDown',
                                     dots           : false,
                                     autoplay       : 3000,
                                     loop           : true,
                                     transitionStyle: "fadeUp",
                                     responsive     : {
                                         0   : {
                                             items: 1,
                                             nav  : false,
                                             dots : false
                                         },
                                         600 : {
                                             items: 1,
                                         },
                                         1000: {
                                             items: 1,
                                         }
                                     }
                                 });
}
// ------------------------------------------------
//  Dense Retina Display
// ------------------------------------------------

if ($.isFunction($.fn.dense))
{
    $('.img-retina').dense();
}

////Prevent Scroll
var body = document.getElementsByTagName('body')[0];
$(".dropdown-menu").mouseover(function disableBodyScroll() {
    body.style.overflow = 'hidden';
});
$(".dropdown-menu").mouseout(function enableBodyScroll() {
    body.style.overflow = 'auto';
});

