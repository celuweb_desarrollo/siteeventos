# COOPIDROGAS

Plataforma de eventos que busca integrar las actividades comerciales como Ferias Regionales y Megaferia en un solo sitio web.

## INSTALACIÓN

```
composer install
npm install
```

## MIGRACIONES

Crea las migraciones

```
php bin/console make:migration
```

Ejecuta las migraciones

```
php bin/console doctrine:migrations:migrate
```

```
USE coopidrogas_event;
SET FOREIGN_KEY_CHECKS=0;

TRUNCATE banner;
TRUNCATE banner_image;
TRUNCATE content;
TRUNCATE drugstore;
TRUNCATE event_provider;
TRUNCATE event;
TRUNCATE incentive_provider;
TRUNCATE premiaton;
TRUNCATE provider_user_event;
TRUNCATE transerrs;
TRUNCATE winners_provider;
TRUNCATE winner;
TRUNCATE pqrs;
TRUNCATE offer;
TRUNCATE zoom;

SET FOREIGN_KEY_CHECKS=1;
```