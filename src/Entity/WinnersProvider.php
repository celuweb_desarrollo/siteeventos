<?php

namespace App\Entity;

use App\Repository\WinnersProviderRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=WinnersProviderRepository::class)
 */
class WinnersProvider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $created_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deleted_by_id;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity=IncentiveProvider::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $incentive;

    /**
     * @ORM\ManyToOne(targetEntity=Associated::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $associated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getCreatedById(): ?int
    {
        return $this->created_by_id;
    }

    public function setCreatedById(?int $created_by_id): self
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    public function getDeletedById(): ?int
    {
        return $this->deleted_by_id;
    }

    public function setDeletedById(int $deleted_by_id): self
    {
        $this->deleted_by_id = $deleted_by_id;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getIncentive(): ?IncentiveProvider
    {
        return $this->incentive;
    }

    public function setIncentive(?IncentiveProvider $incentive): self
    {
        $this->incentive = $incentive;

        return $this;
    }

    public function getAssociated(): ?Associated
    {
        return $this->associated;
    }

    public function setAssociated(?Associated $associated): self
    {
        $this->associated = $associated;

        return $this;
    }

      /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        //$this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));

    }
}
