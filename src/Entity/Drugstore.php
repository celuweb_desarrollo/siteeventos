<?php

namespace App\Entity;

use App\Repository\DrugstoreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DrugstoreRepository::class)
 */
class Drugstore
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code_drugstore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code_responsible;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_drugstore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_responsible;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $nit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cost_center;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email_drugstore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email_solicitant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeDrugstore(): ?int
    {
        return $this->code_drugstore;
    }

    public function setCodeDrugstore(?int $code_drugstore): self
    {
        $this->code_drugstore = $code_drugstore;

        return $this;
    }

    public function getCodeResponsible(): ?int
    {
        return $this->code_responsible;
    }

    public function setCodeResponsible(?int $code_responsible): self
    {
        $this->code_responsible = $code_responsible;

        return $this;
    }

    public function getNameDrugstore(): ?string
    {
        return $this->name_drugstore;
    }

    public function setNameDrugstore(?string $name_drugstore): self
    {
        $this->name_drugstore = $name_drugstore;

        return $this;
    }

    public function getNameResponsible(): ?string
    {
        return $this->name_responsible;
    }

    public function setNameResponsible(?string $name_responsible): self
    {
        $this->name_responsible = $name_responsible;

        return $this;
    }

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(?string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getCostCenter(): ?int
    {
        return $this->cost_center;
    }

    public function setCostCenter(?int $cost_center): self
    {
        $this->cost_center = $cost_center;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(?string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getEmailDrugstore(): ?string
    {
        return $this->email_drugstore;
    }

    public function setEmailDrugstore(?string $email_drugstore): self
    {
        $this->email_drugstore = $email_drugstore;

        return $this;
    }

    public function getEmailSolicitant(): ?string
    {
        return $this->email_solicitant;
    }

    public function setEmailSolicitant(?string $email_solicitant): self
    {
        $this->email_solicitant = $email_solicitant;

        return $this;
    }
}
