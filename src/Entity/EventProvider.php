<?php

namespace App\Entity;

use App\Repository\EventProviderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=EventProviderRepository::class)
 */
class EventProvider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class, inversedBy="eventProvider")
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="eventProvider")
     */
    private $event;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zoom_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zoom_number;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity=ProviderUserEvent::class, mappedBy="event_provider")
     */
    private $providerUserEvents;

    public function __construct()
    {
        $this->providerUserEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getEvent(): ?event
    {
        return $this->event;
    }

    public function setEvent(?event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getZoomCode(): ?string
    {
        return $this->zoom_code;
    }

    public function setZoomCode(?string $zoom_code): self
    {
        $this->zoom_code = $zoom_code;

        return $this;
    }

    public function getZoomNumber(): ?string
    {
        return $this->zoom_number;
    }

    public function setZoomNumber(?string $zoom_number): self
    {
        $this->zoom_number = $zoom_number;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|ProviderUserEvent[]
     */
    public function getProviderUserEvents(): Collection
    {
        return $this->providerUserEvents;
    }

    public function addProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if (!$this->providerUserEvents->contains($providerUserEvent)) {
            $this->providerUserEvents[] = $providerUserEvent;
            $providerUserEvent->setEventProvider($this);
        }

        return $this;
    }

    public function removeProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if ($this->providerUserEvents->removeElement($providerUserEvent)) {
            // set the owning side to null (unless already changed)
            if ($providerUserEvent->getEventProvider() === $this) {
                $providerUserEvent->setEventProvider(null);
            }
        }

        return $this;
    }
}
