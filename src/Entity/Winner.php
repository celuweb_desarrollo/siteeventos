<?php

namespace App\Entity;

use App\Repository\WinnerRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=WinnerRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks
 */
class Winner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

      /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $logo;
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $created_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updated_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $delete_by_id;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider_id;

    /**
     * @ORM\ManyToOne(targetEntity=Premiaton::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $premiaton_id;

    /**
     * @ORM\ManyToOne(targetEntity=Associated::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $associated_id;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getCreatedById(): ?int
    {
        return $this->created_by_id;
    }

    public function setCreatedById(int $created_by_id): self
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    public function getUpdatedById(): ?int
    {
        return $this->updated_by_id;
    }

    public function setUpdatedById(?int $updated_by_id): self
    {
        $this->updated_by_id = $updated_by_id;

        return $this;
    }

    public function getDeleteById(): ?int
    {
        return $this->delete_by_id;
    }

    public function setDeleteById(?int $delete_by_id): self
    {
        $this->delete_by_id = $delete_by_id;

        return $this;
    }

    public function getProviderId(): ?Provider
    {
        return $this->provider_id;
    }

    public function setProviderId(?Provider $provider_id): self
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    public function getPremiatonId(): ?Premiaton
    {
        return $this->premiaton_id;
    }

    public function setPremiatonId(?Premiaton $premiaton_id): self
    {
        $this->premiaton_id = $premiaton_id;

        return $this;
    }

    public function getAssociatedId(): ?Associated
    {
        return $this->associated_id;
    }

    public function setAssociatedId(?Associated $associated_id): self
    {
        $this->associated_id = $associated_id;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        //$this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));

    }

}
