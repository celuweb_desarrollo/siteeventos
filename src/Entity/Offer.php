<?php

namespace App\Entity;

use App\Repository\OfferRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks
 */
class Offer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $apply;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(type="string", length=45,  nullable=true)
     */
    private $file_url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $created_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updated_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deleted_by_id;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApply(): ?string
    {
        return $this->apply;
    }

    public function setApply(?string $apply): self
    {
        $this->apply = $apply;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(?string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getFileUrl(): ?string
    {
        return $this->file_url;
    }

    public function setFile_Url(string $file_url): self
    {
        $this->file_url = $file_url;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getCreatedById(): ?int
    {
        return $this->created_by_id;
    }

    public function setCreatedById(int $created_by_id): self
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    public function getUpdatedById(): ?int
    {
        return $this->updated_by_id;
    }

    public function setUpdatedById(int $updated_by_id): self
    {
        $this->updated_by_id = $updated_by_id;

        return $this;
    }

    public function getDeletedById(): ?int
    {
        return $this->deleted_by_id;
    }

    public function setDeletedById(?int $deleted_by_id): self
    {
        $this->deleted_by_id = $deleted_by_id;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProviderId(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
