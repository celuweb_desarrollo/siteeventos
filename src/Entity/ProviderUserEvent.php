<?php

namespace App\Entity;

use App\Repository\ProviderUserEventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProviderUserEventRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class ProviderUserEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class, inversedBy="providerUserEvents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="providerUserEvents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="providerUserEvents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_at;


     /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finish_at;

    /**
     * @ORM\ManyToOne(targetEntity=EventProvider::class, inversedBy="providerUserEvents")
     */
    private $event_provider;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->create_at = $createAt;

        return $this;
    }

    public function getFinishAt(): ?\DateTimeInterface
    {
        return $this->finish_at;
    }

    public function setFinishAt(?\DateTimeInterface $finishAt): self
    {
        $this->finish_at = $finishAt;

        return $this;
    }


    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdateAt(new \DateTime('now'));
        if ($this->getCreateAt() === NULL)
            $this->setCreateAt(new \DateTime('now'));

    }

    public function getEventProvider(): ?EventProvider
    {
        return $this->event_provider;
    }

    public function setEventProvider(?EventProvider $event_provider): self
    {
        $this->event_provider = $event_provider;

        return $this;
    }

}
