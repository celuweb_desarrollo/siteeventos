<?php

namespace App\Entity;

use App\Entity\Provider;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $final_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $percent;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $created_by_id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updated_by_id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deleted_by_id;

    /**
     * @ORM\OneToMany(targetEntity=EventProvider::class, mappedBy="event")
     */
    private $eventProviders;

    /**
     * @ORM\OneToMany(targetEntity=EventProvider::class, mappedBy="event")
     */
    private $eventProvider;

    /**
     * @ORM\Column(type="integer")
     */
    private $active = 1;

    /**
     * @ORM\OneToOne(targetEntity=Banner::class, cascade={"persist", "remove"})
     */
    private $banner;

    /**
     * @ORM\OneToMany(targetEntity=Content::class, mappedBy="event")
     */
    private $contents;

    /**
     * @ORM\OneToMany(targetEntity=UserEvent::class, mappedBy="event")
     */
    private $userEvents;

    /**
     * @ORM\OneToMany(targetEntity=ProviderUserEvent::class, mappedBy="event")
     */
    private $providerUserEvents;

    /**
     * @ORM\OneToMany(targetEntity=UserContent::class, mappedBy="event")
     */
    private $userContents;


    public function __construct()
    {
        $this->eventProviders     = new ArrayCollection();
        $this->eventProvider      = new ArrayCollection();
        $this->contents           = new ArrayCollection();
        $this->userEvents         = new ArrayCollection();
        $this->providerUserEvents = new ArrayCollection();
        $this->userContents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getFinalDate(): ?\DateTimeInterface
    {
        return $this->final_date;
    }

    public function setFinalDate(\DateTimeInterface $final_date): self
    {
        $this->final_date = $final_date;

        return $this;
    }

    public function getPercent(): ?int
    {
        return $this->percent;
    }

    public function setPercent(int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCreatedById(): ?int
    {
        return $this->created_by_id;
    }

    public function setCreatedById(?int $created_by_id): self
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUpdatedById(): ?int
    {
        return $this->updated_by_id;
    }

    public function setUpdatedById(?int $updated_by_id): self
    {
        $this->updated_by_id = $updated_by_id;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getDeletedById(): ?int
    {
        return $this->deleted_by_id;
    }

    public function setDeletedById(?int $deleted_by_id): self
    {
        $this->deleted_by_id = $deleted_by_id;

        return $this;
    }

    /**
     * @return Collection|EventProvider[]
     */
    public function getEventProviders(): Collection
    {
        return $this->eventProviders;
    }

    public function addEventProvider(EventProvider $eventProvider): self
    {
        if (!$this->eventProviders->contains($eventProvider))
        {
            $this->eventProviders[] = $eventProvider;
            $eventProvider->setEvent($this);
        }

        return $this;
    }

    public function removeEventProvider(EventProvider $eventProvider): self
    {
        if ($this->eventProviders->removeElement($eventProvider))
        {
            // set the owning side to null (unless already changed)
            if ($eventProvider->getEvent() === $this)
            {
                $eventProvider->setEvent(NULL);
            }
        }

        return $this;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));

    }

    /**
     * @return Collection|EventProvider[]
     */
    public function getEventProvider(): Collection
    {
        return $this->eventProvider;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? (string) $this->getName() : '-';
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBanner(): ?Banner
    {
        return $this->banner;
    }

    public function setBanner(?Banner $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setEvent($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->removeElement($content)) {
            // set the owning side to null (unless already changed)
            if ($content->getEvent() === $this) {
                $content->setEvent(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|UserEvent[]
     */
    public function getUserEvents(): Collection
    {
        return $this->userEvents;
    }

    public function addUserEvent(UserEvent $userEvent): self
    {
        if (!$this->userEvents->contains($userEvent)) {
            $this->userEvents[] = $userEvent;
            $userEvent->setEvent($this);
        }

        return $this;
    }

    public function removeUserEvent(UserEvent $userEvent): self
    {
        if ($this->userEvents->removeElement($userEvent)) {
            // set the owning side to null (unless already changed)
            if ($userEvent->getEvent() === $this) {
                $userEvent->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProviderUserEvent[]
     */
    public function getProviderUserEvents(): Collection
    {
        return $this->providerUserEvents;
    }

    public function addProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if (!$this->providerUserEvents->contains($providerUserEvent)) {
            $this->providerUserEvents[] = $providerUserEvent;
            $providerUserEvent->setEvent($this);
        }

        return $this;
    }

    public function removeProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if ($this->providerUserEvents->removeElement($providerUserEvent)) {
            // set the owning side to null (unless already changed)
            if ($providerUserEvent->getEvent() === $this) {
                $providerUserEvent->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserContent[]
     */
    public function getUserContents(): Collection
    {
        return $this->userContents;
    }

    public function addUserContent(UserContent $userContent): self
    {
        if (!$this->userContents->contains($userContent)) {
            $this->userContents[] = $userContent;
            $userContent->setEvent($this);
        }

        return $this;
    }

    public function removeUserContent(UserContent $userContent): self
    {
        if ($this->userContents->removeElement($userContent)) {
            // set the owning side to null (unless already changed)
            if ($userContent->getEvent() === $this) {
                $userContent->setEvent(null);
            }
        }

        return $this;
    }


}
