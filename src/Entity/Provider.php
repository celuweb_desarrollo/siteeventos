<?php

namespace App\Entity;

use App\Repository\ProviderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass=ProviderRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks
 */
class Provider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $created_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updated_by_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deleted_by_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $representant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nit;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Transferrs::class, mappedBy="provider")
     */
    private $transferrs;

    /**
     * @ORM\OneToMany(targetEntity=EventProvider::class, mappedBy="provider")
     */
    private $eventProviders;

    /**
     * @ORM\OneToMany(targetEntity=EventProvider::class, mappedBy="provider")
     */
    private $eventProvider;

    /**
     * @ORM\OneToMany(targetEntity=Winner::class, mappedBy="provider_id")
     */
    private $premiaton_id;


    public function __construct()
    {
        $this->transferrs = new ArrayCollection();
        $this->eventProviders = new ArrayCollection();
        $this->eventProvider = new ArrayCollection();
        $this->premiaton_id = new ArrayCollection();
        $this->providerUserEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getCreatedById(): ?int
    {
        return $this->created_by_id;
    }

    public function setCreatedById(?int $created_by_id): self
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    public function getUpdatedById(): ?int
    {
        return $this->updated_by_id;
    }

    public function setUpdatedById(?int $updated_by_id): self
    {
        $this->updated_by_id = $updated_by_id;

        return $this;
    }

    public function getDeletedById(): ?int
    {
        return $this->deleted_by_id;
    }

    public function setDeletedById(?int $deleted_by_id): self
    {
        $this->deleted_by_id = $deleted_by_id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getRepresentant(): ?string
    {
        return $this->representant;
    }

    public function setRepresentant(?string $representant): self
    {
        $this->representant = $representant;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(?string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));

    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Transferrs[]
     */
    public function getTransferrs(): Collection
    {
        return $this->transferrs;
    }

    public function addTransferr(Transferrs $transferr): self
    {
        if (!$this->transferrs->contains($transferr)) {
            $this->transferrs[] = $transferr;
            $transferr->setProvider($this);
        }

        return $this;
    }

    public function removeTransferr(Transferrs $transferr): self
    {
        if ($this->transferrs->removeElement($transferr)) {
            // set the owning side to null (unless already changed)
            if ($transferr->getProvider() === $this) {
                $transferr->setProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventProvider[]
     */
    public function getEventProviders(): Collection
    {
        return $this->eventProviders;
    }

    public function addEventProvider(EventProvider $eventProvider): self
    {
        if (!$this->eventProviders->contains($eventProvider)) {
            $this->eventProviders[] = $eventProvider;
            $eventProvider->setProvider($this);
        }

        return $this;
    }

    public function removeEventProvider(EventProvider $eventProvider): self
    {
        if ($this->eventProviders->removeElement($eventProvider)) {
            // set the owning side to null (unless already changed)
            if ($eventProvider->getProvider() === $this) {
                $eventProvider->setProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventProvider[]
     */
    public function getEventProvider(): Collection
    {
        return $this->eventProvider;
    }

    /**
     * @return Collection|Winner[]
     */
    public function getPremiatonId(): Collection
    {
        return $this->premiaton_id;
    }

    public function addPremiatonId(Winner $premiatonId): self
    {
        if (!$this->premiaton_id->contains($premiatonId)) {
            $this->premiaton_id[] = $premiatonId;
            $premiatonId->setProviderId($this);
        }

        return $this;
    }

    public function removePremiatonId(Winner $premiatonId): self
    {
        if ($this->premiaton_id->removeElement($premiatonId)) {
            // set the owning side to null (unless already changed)
            if ($premiatonId->getProviderId() === $this) {
                $premiatonId->setProviderId(null);
            }
        }

        return $this;
    }

    // /**
    //  * @return Collection|ProviderUserEvent[]
    //  */
    // public function getProviderUserEvents(): Collection
    // {
    //     return $this->providerUserEvents;
    // }

    // public function addProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    // {
    //     if (!$this->providerUserEvents->contains($providerUserEvent)) {
    //         $this->providerUserEvents[] = $providerUserEvent;
    //         $providerUserEvent->setProvider($this);
    //     }

    //     return $this;
    // }

    // public function removeProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    // {
    //     if ($this->providerUserEvents->removeElement($providerUserEvent)) {
    //         // set the owning side to null (unless already changed)
    //         if ($providerUserEvent->getProvider() === $this) {
    //             $providerUserEvent->setProvider(null);
    //         }
    //     }

    //     return $this;
    // }
}
