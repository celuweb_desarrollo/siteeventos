<?php

namespace App\Entity;

use App\Entity\Event;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\HasLifecycleCallbacks
 *
 */
class User implements UserInterface
{

    const SUCCESSFUL_REGISTRATION = 'El usuario ha sido almacenado satisfactoriamente.';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(name="is_verified", type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(name="is_super_admin", type="boolean", nullable=true)
     */
    private $isSuperAdmin;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $session_token;

    /**
     * @ORM\OneToMany(targetEntity=UserEvent::class, mappedBy="user")
     */
    private $userEvents;

    /**
     * @ORM\OneToMany(targetEntity=ProviderUserEvent::class, mappedBy="user")
     */
    private $providerUserEvents;

    /**
     * @ORM\OneToMany(targetEntity=UserContent::class, mappedBy="user")
     */
    private $userContents;

    /**
     * @ORM\OneToOne(targetEntity=Profile::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $profile;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="user")
     */
    private $sessions;


    public function __construct()
    {
        $this->User               = new ArrayCollection();
        $this->userEvent          = new ArrayCollection();
        $this->providerUserEvents = new ArrayCollection();
        $this->termConditions = new ArrayCollection();
        $this->userContents = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
//        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return NULL;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function isSuperAdmin():bool{

        return $this->isSuperAdmin;
    }

    /**
     * @param mixed $isSuperAdmin
     */
    public function setIsSuperAdmin(bool $isSuperAdmin): self
    {
        $this->isSuperAdmin = $isSuperAdmin;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === NULL)
            $this->setCreatedAt(new \DateTime('now'));

    }

    public function getSessionToken(): ?string
    {
        return $this->session_token;
    }

    public function setSessionToken(?string $session_token): self
    {
        $this->session_token = $session_token;

        return $this;
    }

//    /**
//     * @return Collection|Event[]
//     */
//    public function getUser(): Collection
//    {
//        return $this->User;
//    }
//
//    public function addUser(Event $user): self
//    {
//        if (!$this->User->contains($user)) {
//            $this->User[] = $user;
//            $user->setUser($this);
//        }
//
//        return $this;
//    }
//
//    public function removeUser(Event $user): self
//    {
//        if ($this->User->removeElement($user)) {
//            // set the owning side to null (unless already changed)
//            if ($user->getUser() === $this) {
//                $user->setUser(null);
//            }
//        }
//
//        return $this;
//    }

    public function getEvent(): ?UserEvent
    {
        return $this->event;
    }

    public function setEvent(?UserEvent $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection|UserEvent[]
     */
    public function getUserevents(): Collection
    {
        return $this->userevents;
    }

    public function addUserevent(UserEvent $userevent): self
    {
        if (!$this->userevents->contains($userevent)) {
            $this->userevents[] = $userevent;
            $userevent->setUser($this);
        }

        return $this;
    }

    public function removeUserevent(UserEvent $userevent): self
    {
        if ($this->userevents->removeElement($userevent)) {
            // set the owning side to null (unless already changed)
            if ($userevent->getUser() === $this) {
                $userevent->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProviderUserEvent[]
     */
    public function getProviderUserEvents(): Collection
    {
        return $this->providerUserEvents;
    }

    public function addProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if (!$this->providerUserEvents->contains($providerUserEvent)) {
            $this->providerUserEvents[] = $providerUserEvent;
            $providerUserEvent->setUser($this);
        }

        return $this;
    }

    public function removeProviderUserEvent(ProviderUserEvent $providerUserEvent): self
    {
        if ($this->providerUserEvents->removeElement($providerUserEvent)) {
            // set the owning side to null (unless already changed)
            if ($providerUserEvent->getUser() === $this) {
                $providerUserEvent->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserContent[]
     */
    public function getUserContents(): Collection
    {
        return $this->userContents;
    }

    public function addUserContent(UserContent $userContent): self
    {
        if (!$this->userContents->contains($userContent)) {
            $this->userContents[] = $userContent;
            $userContent->setUser($this);
        }

        return $this;
    }

    public function removeUserContent(UserContent $userContent): self
    {
        if ($this->userContents->removeElement($userContent)) {
            // set the owning side to null (unless already changed)
            if ($userContent->getUser() === $this) {
                $userContent->setUser(null);
            }
        }

        return $this;
    }

//    public function getProfile(): ?self
//    {
//        return $this->profile;
//    }
//
//    public function setProfile(self $profile): self
//    {
//        $this->profile = $profile;
//
//        return $this;
//    }

public function getProfile(): ?Profile
{
    return $this->profile;
}

public function setProfile(Profile $profile): self
{
    $this->profile = $profile;

    return $this;
}

/**
 * @return Collection|Session[]
 */
public function getSessions(): Collection
{
    return $this->sessions;
}

public function addSession(Session $session): self
{
    if (!$this->sessions->contains($session)) {
        $this->sessions[] = $session;
        $session->setUser($this);
    }

    return $this;
}

public function removeSession(Session $session): self
{
    if ($this->sessions->removeElement($session)) {
        // set the owning side to null (unless already changed)
        if ($session->getUser() === $this) {
            $session->setUser(null);
        }
    }

    return $this;
}
}
