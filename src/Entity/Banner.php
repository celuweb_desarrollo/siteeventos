<?php

namespace App\Entity;

use App\Repository\BannerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BannerRepository::class)
 */
class Banner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=BannerImage::class, mappedBy="banner")
     */
    private $bannerImages;

    public function __construct()
    {
        $this->bannerImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|BannerImage[]
     */
    public function getBannerImages(): Collection
    {
        return $this->bannerImages;
    }

    public function addBannerImage(BannerImage $bannerImage): self
    {
        if (!$this->bannerImages->contains($bannerImage)) {
            $this->bannerImages[] = $bannerImage;
            $bannerImage->setBanner($this);
        }

        return $this;
    }

    public function removeBannerImage(BannerImage $bannerImage): self
    {
        if ($this->bannerImages->removeElement($bannerImage)) {
            // set the owning side to null (unless already changed)
            if ($bannerImage->getBanner() === $this) {
                $bannerImage->setBanner(null);
            }
        }

        return $this;
    }
}
