<?php

namespace App\Controller;


use App\Entity\Event;
use App\Entity\Provider;
use App\Entity\Transferrs;
use App\Repository\TransferrsRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


class TransferrsController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/admin/transferrs", name="transferrs")
     */
    public function index(Request $request): Response

    {
        $table = $this->createDataTable()
            ->add('grm4', TextColumn::class, ['field' => 'p.grm4', 'label' => 'Código'])
            ->add('provider', TextColumn::class, ['field' => 'p.provider', 'label' => 'Proveedor'])
            ->add('divis', TextColumn::class, ['field' => 'p.divis', 'label' => 'Divis'])
            ->add('transfer', TextColumn::class, ['field' => 'p.transfer', 'label' => 'Transferencista'])
            ->add('transfer_phone', TextColumn::class, ['field' => 'p.transfer_phone', 'label' => 'Teléfono'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($transferr_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('transferrs_edit', ['id' => $transferr_id]).'">Editar</a><a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$transferr_id.')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Transferrs::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('p')->from(Transferrs::class, 'p');
                }
            ])
            ->handleRequest($request);
        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/transferrs/index.html.twig', [
            'controller_name' => 'TransferrsController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/admin/transferrs/delete/{id?}", name="transferrs_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $transferrs    = $this->getDoctrine()->getRepository(Transferrs::class)->find($id);
        //Registro quien borra y actualiza
        $transferrs->setUpdatedById($this->getUser()->getId());
        $transferrs->setDeletedById($this->getUser()->getId());
        $entityManager->persist($transferrs);
        $entityManager->flush();


        $entityManager->remove($transferrs);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/admin/transferrs/edit/{id?}/{provider_id?}", name="transferrs_edit")
     */
    public function edit($id, $provider_id): Response
    {
        $message   = '';
        $transferr = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        if ($id > 0)
            $transferr = $this->getDoctrine()->getRepository(Transferrs::class)->find($id);

        return $this->render('admin/transferrs/edit.html.twig', compact('transferr', 'provider_id', 'message'));
    }

    /**
     * @Route("/admin/transferrs/save/{id}", name="transferrs_save")
     */
    public function updateOrCreate(Request $request, $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        if ($id > 0)
            $transferrs = $this->getDoctrine()->getRepository(Transferrs::class)->find($id);
        else
            $transferrs = new Transferrs();

        $provider = $this->getDoctrine()->getRepository(Provider::class)->find($dat['provider_id']);

        $transferrs->setGrm4($dat['grm4']);
        $transferrs->setNit(0);
        $transferrs->setProvider($provider);
        $transferrs->setDivis($dat['divis']);
        $transferrs->setTransfer($dat['transfer']);
        $transferrs->setPhone($dat['phone']);
        $transferrs->setCreatedById($this->getUser()->getId());
        $transferrs->setUpdatedById($this->getUser()->getId());

        $entityManager->persist($transferrs);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', 'La información del transferencista ha sido almacenada satisfactoriamente');

        return $this->redirectToRoute('provider_edit', [
            'id' => $transferrs->getProvider()->getId()
        ]);
    }

    /**
     * @Route("/admin/transferrs/export_transfer_sample", name="export_transfer_sample")
     *
     */
    public function export_transfer_sample(Request $request)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Carga Transferencistas");
        $sheet->setCellValue('A1', 'Código Proveedor');
        $sheet->setCellValue('B1', 'Nit Proveedor');
        $sheet->setCellValue('C1', 'Nombre Transferencista');
        $sheet->setCellValue('D1', 'Teléfono');
        $sheet->setCellValue('E1', 'División');

        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT code, nit FROM provider WHERE deleted_at IS NULL");
        $statement->execute();
        $avalaible_providers = $statement->fetchAll();
        $count               = 0;
        for ($i = 2; $i < count($avalaible_providers) + 2; $i++)
        {
            $value = $avalaible_providers[$count]["code"];
            $nit   = $avalaible_providers[$count]["nit"];
            $sheet->getCell('A'.$i)->setValue($value);
            $sheet->getCell('B'.$i)->setValue($nit);
            $sheet->getCell('C'.$i)->setValue(' ');
            $sheet->getCell('D'.$i)->setValue(' ');
            $sheet->getCell('E'.$i)->setValue(' ');
            $count++;
        }
        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'transfer_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/transferrs/import", name="transferrs_import")
     *
     */
    public function loadxsls(Request $request)
    {
        if ($request->getMethod() == 'POST')
            if ($_FILES["file"]["size"] < 10000000) //5Mb
                return $this->xslx($request->files->get('file'));

        return $this->render(
            'admin/transferrs/import.html.twig'
        );
    }

    /**
     * @Route("/admin/transferrs/upload-excel", name="transferXlsx")
     *
     * @throws \Exception
     */
    public function xslx($inputFileName)
    {
        //$inputFileName = './uploads/test.xlsx';
        $spreadsheet = IOFactory::load($inputFileName); // Here we are able to read from the excel file
        $row         = $spreadsheet->getActiveSheet()->removeRow(1); // I added this to be able to remove the first file line
        $sheetData   = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE); // here, the read data is turned into an array
        //dd($sheetData);

        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row)
        {
            if ($Row['A'] != NULL && $Row['B'] != NULL)
            {
                $prov_temp = $this->loadProvider($Row['A']);

                if ($prov_temp)
                {
                    $provider = $this->getDoctrine()->getRepository(Provider::class)->find($prov_temp);

                    $grm4     = $Row['A']; // store the grm4 on each iteration             
                    $nit      = $Row['B']; // store the nit on each iteration 
                    $transfer = $Row['C'] != NULL ? $Row['C'] : '';
                    $phone    = $Row['D'] != NULL ? $Row['D'] : '';
                    $divis    = $Row['E'] != NULL ? $Row['E'] : '';

                    $transferrs_existant = $entityManager->getRepository(Transferrs::class)->findOneBy(array('transfer' => $transfer, 'provider' => $provider));

                    // make sure that the user does not already exists in your db
                    if (!$transferrs_existant)
                    {
                        $transfers = new Transferrs();
                        $transfers->setProvider($provider);
                        $transfers->setGrm4(strval($grm4));
                        $transfers->setNit(strval($nit));
                        $transfers->setDivis(strval($divis));
                        $transfers->setTransfer(strval($transfer));
                        $transfers->setPhone(strval($phone));
                        $entityManager->persist($transfers);
                        $entityManager->flush();
                    }
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('provider_list');
    }

    public function loadProvider($prov_code)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM provider WHERE code LIKE '%$prov_code%'");
        $statement->execute();
        $provider_temp = $statement->fetchAll();

        if (!empty($provider_temp))
            return $provider_temp[0]['id'];
        else
            return NULL;
    }

    /**
     * @Route("admin/transferrs/deleted_transferrs", name="show_transferrs_deleted")
     */
    public function showOffersDeleted(Request $request): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $table = $this->createDataTable()
            ->add('transfer', TextColumn::class, ['field' => 't.transfer', 'label' => 'Nombre'])
            ->add('transfer_phone', TextColumn::class, ['field' => 't.phone', 'label' => 'Teléfono'])
            ->add('grm4', TextColumn::class, ['field' => 't.grm4', 'label' => 'Código'])
            ->add('divis', TextColumn::class, ['field' => 't.divis', 'label' => 'División'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 't.id', 'render' => function ($transferr_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('restore_transferrs', ['id' => $transferr_id]).'">Restaurar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Transferrs::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('t')->from(Transferrs::class, 't')->where('t.deleted_at IS NOT NULL')
                        ->orderBy('t.id', 'ASC');
                }
            ])
            ->handleRequest($request);
        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/transferrs/deleted.html.twig', [
            'controller_name' => 'offerController',
            'datatable'       => $table,
            'message'         => $message
        ]);
        $filters->enable('soft_deleteable');
    }

    /**
     * @Route("admin/transferrs/restore_transffers/{id?}", name="restore_transferrs")
     */
    public function restoreTranssfersDeleted($id): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $entityManager = $this->getDoctrine()->getManager();
        $transfer      = $this->getDoctrine()->getRepository(Transferrs::class)->find($id);

        $transfer->setDeletedAt(NULL);
        $transfer->setUpdatedById($this->getUser()->getId());
        $transfer->setDeletedById(NULL);

        $entityManager->persist($transfer);
        $entityManager->flush();


        $entityManager->flush();
        $filters->enable('soft_deleteable');
        $this->session->getFlashBag()->add('success', 'Transferencista activado satisfactoriamente');

        return $this->redirectToRoute('show_transferrs_deleted');
        //return new Response(Response::HTTP_ACCEPTED);
    }
}
