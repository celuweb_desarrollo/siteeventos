<?php

namespace App\Controller;

use App\Entity\BannerImage;
use App\Entity\Event;
use App\Entity\Pqrs;
use App\Form\PqrsType;
use App\Repository\PqrsRepository;
use Doctrine\ORM\QueryBuilder;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;


class PqrsController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/admin/pqrs", name="pqrs_index")
     */
    public function index(Request $request): Response
    {
        $table = $this->createDataTable()
            ->add('code', TextColumn::class, ['field' => 'p.code_associate', 'label' => 'Código Asociado'])
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre'])
            ->add('email', TextColumn::class, ['field' => 'p.email', 'label' => 'Email'])
            ->add('phone', TextColumn::class, ['field' => 'p.phone', 'label' => 'Teléfono'])
            ->add('message', TextColumn::class, ['field' => 'p.message', 'label' => 'Mensaje'])
            ->add('type', TextColumn::class, [
                'field' => 'p.type', 'label' => 'Tipo Pqr', 'render' => function ($type) {
                    switch ($type) {
                        case "1":
                            $showTipe = "Petición";
                            break;
                        case "2":
                            $showTipe = "Queja";
                            break;
                        case "3":
                            $showTipe = "Reclamo";
                            break;
                        case "4":
                            $showTipe = "Solicitud";
                            break;
                        default:
                            $showTipe = "Felicitación";
                    }

                    return sprintf('<span>' . $showTipe . '</span>');
                }
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Pqrs::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('p')->from(Pqrs::class, 'p');
                }
            ])
            ->handleRequest($request);
        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/pqrs/index.html.twig', [
            'controller_name' => 'ProviderController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/pqrs/new", name="pqrs_new", methods={"GET","POST"})
     */
    public function new(Request $request, MailerInterface $mailer)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser()->getEmail();

        $pqrs = new Pqrs();
        $pqrs->setName($dat['name']);
        $pqrs->setEmail($dat['email']);
        $pqrs->setPhone($dat['phone']);
        $pqrs->setMessage($dat['message']);
        $pqrs->setType($dat['type']);
        $pqrs->setCodeAssociate($user);
        $entityManager->persist($pqrs);
        $entityManager->flush();

        $types = ['1' => 'Petición', '2' => 'Queja', '3' => 'Reclamo', '4' => 'Solicitud', '5' => 'Felicitación'];
        $dat['type'] = $types[$dat['type']];
        $dat['user'] = $user;
        $email = (new TemplatedEmail())
            ->from($_ENV['EMAIL_SENDER'])
            ->to(new Address($_ENV['EMAIL_PQRS'], 'Administrador PQRS'))
            ->subject('Nuevo PQRS almacenado en el sistema.')
            ->htmlTemplate('admin/pqrs/email.html.twig')
            ->context([
                'pqrsData' => $dat,
            ]);

        $mailer->send($email);

        $this->addFlash('success', 'Su PQRS ha sido creada satisfactoriamente!');

        return $this->redirectToRoute('associate_pqrs');
    }

    /**
     * @Route("/admin/pqrs/{id}", name="pqrs_show", methods={"GET"})
     */
    public function show(Pqrs $pqr): Response
    {
        return $this->render('admin/pqrs/show.html.twig', [
            'pqr' => $pqr,
        ]);
    }

    public function new_pqrs(Request $request, MailerInterface $mailer): Response
    {
        $pqr = new Pqrs();
        $form = $this->createForm(PqrsType::class, $pqr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pqr);
            $entityManager->flush();


            return $this->redirectToRoute('associate_pqrs');
        }

        return $this->render('admin/pqrs/new.html.twig', [
            'pqr'  => $pqr,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/pqrs/edit/{id}", name="pqrs_edit")
     */
    public function edit(Request $request, $id): Response
    {

        $reg = [];

        if ($id > 0)
            $reg = $this->getDoctrine()->getRepository(Pqrs::class)->find($id);

        $datatable = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre'])
            ->add('email', TextColumn::class, ['field' => 'p.email', 'label' => 'Email'])
            ->add('phone', TextColumn::class, ['field' => 'p.phone', 'label' => 'Teléfono'])
            ->add('city', TextColumn::class, ['field' => 'p.city', 'label' => 'Ciudad'])
            ->add('message', TextColumn::class, ['field' => 'p.message', 'label' => 'Mensaje'])
            ->add('type', TextColumn::class, ['field' => 'p.type', 'label' => 'Tipo Pqr'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 't.id', 'render' => function ($pqrs_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('pqrs_edit', ['id' => $pqrs_id]) . '">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $pqrs_id . ')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Pqrs::class,
                'query'  => function (QueryBuilder $builder) use ($id) {
                    $builder->select('t')->from(Pqrs::class, 't')->setParameter('pqrs_id', $id);
                }
            ])
            ->handleRequest($request);

        if ($datatable->isCallback())
            return $datatable->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        //return $this->render('admin/pqrs/edit.html.twig', compact('reg', 'datatable', 'message'));
    }

    /**
     * @Route("/admin/pqrs/delete/{id}", name="pqrs_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $pqrs          = $this->getDoctrine()->getRepository(Pqrs::class)->find($id);
        $entityManager->remove($pqrs);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("asociado/pqrs", name="associate_pqrs")
     */
    public function showPqrs(Request $request){

        $event_id = $request->getSession()->get('event_id');

        if(isset($event_id)){
            $event   = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
            $banner  = $event->getBanner();
            $banners = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);
        }else{
            $banners  = $this->getDoctrine()->getRepository(BannerImage::class)->findByEventStatus();
        }

        return $this->render('site/pqrs.html.twig', [
            'banners'  => $banners,
            'event_id' => $event_id,
        ]);
    }
}
