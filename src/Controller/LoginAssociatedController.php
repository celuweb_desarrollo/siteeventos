<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LoginAssociatedController extends AbstractController
{
    private $session;

    private const AUTHENTICATION_LOGIN = '_security.route_login';

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="associate_login")
     */
    public function loginAssociate(AuthenticationUtils $authenticationUtils): Response
    {
        // Obtener el error de inicio de sesión si hay uno
        $error = $authenticationUtils->getLastAuthenticationError();

        // Recupera el último correo electrónico ingresado por el usuario.
        $lastUsername = $authenticationUtils->getLastUsername();

        $sessionEmail = $this->session->get(Security::LAST_USERNAME);
        $user         = $this->getUser();

        if(isset($sessionEmail) && isset($user)){

            if($this->session->get(self::AUTHENTICATION_LOGIN) == 'associated'){
                return new RedirectResponse($this->generateUrl('associate_events'));

            }else if($this->session->get(self::AUTHENTICATION_LOGIN) == 'admin'){
                return new RedirectResponse($this->generateUrl('event_index'));
            }
        }

        return $this->render('site/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error
        ]);
    }
}
