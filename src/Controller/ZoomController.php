<?php

namespace App\Controller;

use App\Entity\Associated;
use App\Entity\Drugstore;
use App\Entity\EventProvider;
use App\Entity\Zoom;
use App\Form\ZoomType;
use App\Repository\ZoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ZoomController extends AbstractController
{
    /**
     * @Route("asociado/zoom", name="zoom_index")
     */
    public function index(ZoomRepository $zoomRepository): Response
    {
        return $this->render('admin/zoom/zoom.html.twig');
    }

    /**
     * @Route("/zoom/new", name="zoom_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $zoom = new Zoom();
        $form = $this->createForm(ZoomType::class, $zoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($zoom);
            $entityManager->flush();

            return $this->redirectToRoute('zoom_index');
        }

        return $this->render('admin/zoom/new.html.twig', [
            'zoom' => $zoom,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/zoom/{id}", name="zoom_show", methods={"GET"})
     */
    public function show(Zoom $zoom): Response
    {
        return $this->render('admin/zoom/show.html.twig', [
            'data_zoom' => $zoom,
        ]);
    }

    /**
     * @Route("/zoom/join/{ev_provider_id}", name="join_zoom")
     */
    public function joinZoom(Request $request, $ev_provider_id)
    {
        $info_provider = $this->getDoctrine()->getRepository(EventProvider::class)->findBy(['id' => $ev_provider_id]);

        if (isset($info_provider[0]))
        {
            $user           = $this->getUser();
            $user_info      = $this->getDoctrine()->getRepository(Associated::class)->findById($user->getId());
            $ass_code       = $user_info[0]->getCode();
            $drugstore_info = $this->getDoctrine()->getRepository(Drugstore::class)->findByCode($ass_code);

            $ass_email = $drugstore_info[0]->getEmailDrugstore();
            $ass_name  = $drugstore_info[0]->getNameDrugstore();
            $zoom_code = $info_provider[0]->getZoomCode();
            $zoom_psw  = $info_provider[0]->getZoomNumber();

            return $this->render('site/zoom.html.twig', [
                'user_id'        => $user->getId(),
                'provider_id'    => $info_provider[0]->getProvider()->getId(),
                'ev_provider_id' => $ev_provider_id,
                'zoom_code'      => $zoom_code,
                'zoom_psw'       => $zoom_psw,
                'email'          => $ass_email,
                'code'           => $ass_code,
                'name'           => $ass_name
            ]);
        }

        return $this->redirectToRoute('associate_providers');
    }

    /**
     * @Route("/zoom/join/{provider_id}/meeting", name="meeting_zoom")
     */
    public function meetingZoom(Request $request, $provider_id)
    {
        return $this->render('site/meeting_zoom.html.twig');
    }

    /**
     * @Route("/zoom/{id}/edit", name="zoom_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Zoom $zoom): Response
    {
        $form = $this->createForm(ZoomType::class, $zoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('zoom_index');
        }

        return $this->render('admin/zoom/edit.html.twig', [
            'zoom' => $zoom,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/zoom/{id}", name="zoom_delete", methods={"POST"})
     */
    public function delete(Request $request, Zoom $zoom): Response
    {
        if ($this->isCsrfTokenValid('delete'.$zoom->getId(), $request->request->get('_token')))
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($zoom);
            $entityManager->flush();
        }

        return $this->redirectToRoute('zoom_index');
    }
}
