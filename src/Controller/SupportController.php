<?php

namespace App\Controller;

use App\Entity\BannerImage;
use App\Entity\Event;
use App\Entity\Support;
use App\Form\SupportType;
use Omines\DataTablesBundle\Column\BoolColumn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\DataTableFactory;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Column\TextColumn;
use Symfony\Component\Routing\RouterInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;


class SupportController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("asociado/contacto", name="associate_contact")
     */
    public function showContact(Request $request)
    {
        //$agents = $this->getDoctrine()->getRepository(Support::class)->findAll();
        $event_id = $request->getSession()->get('event_id');

        if(isset($event_id)){
            $event       = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
            $banner      = $event->getBanner();
            $banners     = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);
        }else{
            $banners  = $this->getDoctrine()->getRepository(BannerImage::class)->findByEventStatus();
        }

        $agents   = $this->getDoctrine()->getRepository(Support::class)->findBy(['status' => 1]);

        foreach ($agents as $out){
            $daysSelect = $out->getDays();
            if(isset($daysSelect)){
                $daysSelect = explode('-', $daysSelect);
                $auxDays = '';
                foreach ($daysSelect as $key => $day){
                    $auxDays .= substr($day,0,3);
                    if(($key+1) < count($daysSelect)){
                        $auxDays .=', ';
                    }
                }
            }
            $days[$out->getId()] = ($auxDays??'').' de '.$out->getStartTime().' a '.$out->getEndTime();
        }

        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('site/contact.html.twig', [
            'agents'   => $agents,
            'days'     => $days??[],
            'message'  => $message,
            'event_id' => $event_id,
            'banners'  => $banners
        ]);

        /*         return $this->render('site/contact.html.twig', [
            'controller_name' => 'HomeController'
        ]); */
    }


    /**
     * @Route("/admin/support", name="support_list")
     */
    public function index(Request $request): Response
    {
        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 's.name', 'label' => 'Nombre Completo'])
            ->add('email', TextColumn::class, ['field' => 's.email', 'label' => 'Email'])
            ->add('phone', TextColumn::class, ['field' => 's.phone', 'label' => 'Teléfono'])
            ->add('ext', TextColumn::class, ['field' => 's.ext', 'label' => 'Extensión'])
            ->add('mobile', TextColumn::class, ['field' => 's.mobile', 'label' => 'Celular'])
//            ->add('atention_hour', TextColumn::class, ['field' => 's.atention_hour', 'label' => 'Horario Atención'])
            ->add('status',BoolColumn::class,[
                'field'      => 's.status' ,
                'label'      => 'Estado',
                'trueValue'  => '<span class="badge badge-success">Activo</span>',
                'falseValue' => '<span class="badge badge-warning">Inactivo</span>',
            ])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 's.id', 'render' => function ($support_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('support_edit', ['id' => $support_id]).'">Editar</a>
                                <a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$support_id. ')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Support::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('s')->from(Support::class, 's');
                }])
            ->handleRequest($request);

        if ($table->isCallback()){
            return $table->getResponse();
        }

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/support/index.html.twig', [
            'controller_name' => 'SupportController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/admin/support/edit/{id?}", name="support_edit")
     */
    public function edit(Request $request, $id): Response
    {
        $reg['status']  = 1;
        $reg['new']     = true;
        $reg['date']    = new \DateTime('7:00 A');
        $reg['days']    = [
            'Lunes',
            'Martes',
            'Miercoles',
            'Jueves',
            'Viernes',
            'Sabado',
            'Domingo'
        ];

         if ($id > 0) {

             $reg['new']        = false;
             $reg['support']    = $this->getDoctrine()->getRepository(Support::class)->find($id);
             $reg['name']       = $reg['support']->getName();
             $reg['email']      = $reg['support']->getEmail();
             $reg['phone']      = $reg['support']->getPhone();
             $reg['ext']        = $reg['support']->getExt();
             $reg['phone']      = $reg['support']->getPhone();
             $reg['mobile']     = $reg['support']->getMobile();
             $reg['status']     = $reg['support']->getStatus();
             $reg['start_time'] = $reg['support']->getStartTime();
             $reg['end_time']   = $reg['support']->getEndTime();

             $daysSelect = $reg['support']->getDays();
             if(isset($daysSelect)){
                 $daysSelect = explode('-', $daysSelect);
                 foreach ($daysSelect as $out){
                     $reg['daysSelect'][$out] = 'checked';
                 }
             }
        }

        $reg['id']        = $id;

        return $this->render('admin/support/edit.html.twig', compact('reg'));
    }

    /**
     * @Route("/admin/support/save/{id}", name="support_save")
     */
    public function updateOrCreate(Request $request,  $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();
        $support       = $id > 0 ? $this->getDoctrine()->getRepository(Support::class)->find($id) : new Support();
        $days          = $request->request->get('days')??[];

        $auxDays = null;
        foreach ($days as $key => $day){
            $auxDays .= $day;
            if(($key+1) < count($days)){
                $auxDays .='-';
            }
        }

        $support->setName($dat['name']);
        $support->setEmail($dat['email']);
        $support->setPhone($dat['phone']);
        $support->setExt($dat['ext']);
        $support->setPhone($dat['phone']);
        $support->setMobile($dat['mobile']);
        $support->setStatus($dat['status']);
        $support->setDays($auxDays);
        $support->setStartTime($dat['start_time']);
        $support->setEndTime($dat['end_time']);

        $entityManager->persist($support);
        $entityManager->flush();
        $this->session->getFlashBag()->add('success', Support::SUCCESSFUL_REGISTRATION);

        return $this->redirectToRoute('support');
    }

    /**
     * @Route("/admin/support/delete/{id?}", name="support_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $support       = $this->getDoctrine()->getRepository(Support::class)->find($id);

        $entityManager->remove($support);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }
}
