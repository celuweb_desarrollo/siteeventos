<?php

namespace App\Controller;


use App\Entity\BannerImage;
use App\Entity\Event;
use App\Entity\IncentiveProvider;
use App\Entity\Premiaton;
use App\Entity\Provider;
use App\Entity\EventProvider;
use App\Entity\ProviderUserEvent;
use App\Entity\Transferrs;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\DataTable;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ProviderController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/admin/providers", name="provider_list")
     */
    public function index(Request $request): Response
    {
        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre'])
            ->add('nit', TextColumn::class, ['field' => 'p.nit', 'label' => 'NIT'])
            ->add('representant', TextColumn::class, ['field' => 'p.representant', 'label' => 'Representante'])
            ->add('phone', TextColumn::class, ['field' => 'p.phone', 'label' => 'Teléfono'])
            ->add('email', TextColumn::class, ['field' => 'p.email', 'label' => 'Email'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($provider_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('provider_edit', ['id' => $provider_id]).'">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$provider_id.')">Inactivar</a>');
            }])
            ->addOrderBy(0, DataTable::SORT_ASCENDING)
            ->createAdapter(ORMAdapter::class, [
                'entity' => Provider::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('p')->from(Provider::class, 'p');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        $providers = $this->getDoctrine()->getRepository(Provider::class)->findAll();

        return $this->render('admin/provider/index.html.twig', [
            'controller_name' => 'ProviderController',
            'datatable'       => $table,
            'providers'       => $providers,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/admin/inactive_providers", name="inactive_provider_list")
     */
    public function inactive_providers(Request $request): Response
    {
        // $em = $this->getDoctrine()->getManager();
        // $em->getFilters()->disable('softdeleteable');
        // $object = $em->find('AppBundle:Object', 1);
        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre'])
            ->add('nit', TextColumn::class, ['field' => 'p.nit', 'label' => 'NIT'])
            ->add('representant', TextColumn::class, ['field' => 'p.representant', 'label' => 'Representante'])
            ->add('phone', TextColumn::class, ['field' => 'p.phone', 'label' => 'Teléfono'])
            ->add('email', TextColumn::class, ['field' => 'p.email', 'label' => 'Email'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($provider_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('provider_restore', ['id' => $provider_id]).'">Activar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Provider::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('p')->from(Provider::class, 'p')->where('p.deleted_at IS NOT NULL');
                }
            ])
            ->handleRequest($request);

        // $em->getFilters()->enable('softdeleteable');

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/provider/index_inactive.html.twig', [
            'controller_name' => 'ProviderController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/admin/provider/edit/{id?}", name="provider_edit")
     */
    public function edit(Request $request, $id): Response
    {
        $reg = [];

        if ($id > 0)
            $reg = $this->getDoctrine()->getRepository(Provider::class)->find($id);

        $datatable = $this->createDataTable()
            ->add('transfer', TextColumn::class, ['field' => 't.transfer', 'label' => 'Nombre'])
            ->add('transfer_phone', TextColumn::class, ['field' => 't.phone', 'label' => 'Teléfono'])
            ->add('grm4', TextColumn::class, ['field' => 't.grm4', 'label' => 'Código'])
            ->add('divis', TextColumn::class, ['field' => 't.divis', 'label' => 'División'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 't.id', 'render' => function ($transferr_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('transferrs_edit', ['id' => $transferr_id]).'">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$transferr_id.')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Transferrs::class,
                'query'  => function (QueryBuilder $builder) use ($id) {
                    $builder->select('t')->from(Transferrs::class, 't')->where('t.provider = :provider_id')->setParameter('provider_id', $id);
                }
            ])
            ->handleRequest($request);

        if ($datatable->isCallback())
            return $datatable->getResponse();

        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/provider/edit.html.twig', compact('reg', 'datatable', 'message'));
    }

    /**
     * @Route("/provider/save/{id}", name="provider_save")
     */
    public function updateOrCreate(Request $request, MaelInterventionImageManager $imageManager, $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        if ($id > 0)
            $provider = $this->getDoctrine()->getRepository(Provider::class)->find($id);
        else
            $provider = new Provider();

        if ($request->files->get('file'))
        {
            $filename = date('Ymdhis').rand(0, 9);
            $img      = $imageManager->make($request->files->get('file'));
            $mimes    = [
                'image/jpeg' => 'jpg',
                'image/png'  => 'png',
                'image/gif'  => 'gif'
            ];

            $ext = $mimes[$img->mime];

            $img->fit(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('upload/logos/'.$filename.'.'.$ext);
            $provider->setImage($filename.'.'.$ext);
        }

        $provider->setName($dat['name']);
        $provider->setCode($dat['code']);
        $provider->setName($dat['name']);
        $provider->setNit($dat['nit']);
        $provider->setEmail($request->request->get('email'));
        $provider->setPhone($dat['phone']);
        $provider->setRepresentant($dat['representant']);
        $provider->setCreatedById($this->getUser()->getId());
        $provider->setUpdatedById($this->getUser()->getId());

        $entityManager->persist($provider);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', 'La información del proveedor ha sido almacenada satisfactoriamente');

        return $this->redirectToRoute('provider_edit', [
            'id' => $provider->getId()
        ]);
    }

    /**
     * @Route("/provider/delete/{id?}", name="provider_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $provider      = $this->getDoctrine()->getRepository(Provider::class)->find($id);
        //Registro quien borra y actualiza
        $provider->setUpdatedById($this->getUser()->getId());
        $provider->setDeletedById($this->getUser()->getId());
        $entityManager->persist($provider);
        $entityManager->flush();

        $entityManager->remove($provider);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/provider/restore/{id?}", name="provider_restore")
     */
    public function restore($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $provider      = $this->getDoctrine()->getRepository(Provider::class)->find($id);
        $entityManager->resetManager($provider);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/admin/provider/export_provider_sample", name="export_provider_sample")
     *
     */
    public function export_provider_sample(Request $request)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Carga Proveedores");
        $sheet->setCellValue('A1', 'Nit');
        $sheet->setCellValue('B1', 'Nombre');
        $sheet->setCellValue('C1', 'Código');
        $sheet->setCellValue('D1', 'Telefono');
        $sheet->setCellValue('E1', 'Email');
        $sheet->setCellValue('F1', 'Representante');
        $sheet->setCellValue('G1', 'Logo Proveedor');

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'provider_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }


    /**
     * @Route("/admin/provider/import", name="provider_import")
     *
     */
    public function import(Request $request)
    {
        if ($request->getMethod() == 'POST')
            if ($_FILES["file"]["size"] < 10000000) //5Mb
                return $this->xslx($request->files->get('file'));

        return $this->render('admin/provider/import.html.twig');
    }

    /**
     * @Route("/providers/upload-excel", name="xlsx")
     *
     * @throws \Exception
     */
    public function xslx($inputFileName)
    {
        $spreadsheet   = IOFactory::load($inputFileName);
        $row           = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData     = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row)
        {
            if ($Row['A'] != NULL && $Row['B'] != NULL)
            {
                $nit             = $Row['A'];
                $name            = $Row['B'];
                $code            = $Row['C'] != NULL ? $Row['C'] : '';
                $phone           = $Row['D'] != NULL ? $Row['D'] : '';
                $email           = $Row['E'] != NULL ? $Row['E'] : '';
                $representant    = $Row['F'] != NULL ? $Row['F'] : '';
                $image           = $Row['G'] != NULL ? $Row['G'] : '';
                $exists_provider = $entityManager->getRepository(Provider::class)->findOneBy(array('name' => $name, 'nit' => $nit));

                if (!$exists_provider)
                {
                    $provider = new Provider();
                    $provider->setName($name);
                    $provider->setImage($image);
                    $provider->setNit($nit);
                    $provider->setCode($code);
                    $provider->setEmail($email);
                    $provider->setPhone($phone);
                    $provider->setRepresentant($representant);
                    $entityManager->persist($provider);
                    $entityManager->flush();
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('provider_list');
    }



    // ****************** ASOCIADO ***********************

    /**
     * @Route("asociado/proveedores/{ev_provider_id?}", name="associate_providers")
     */
    public function listEventProvider(Request $request, $ev_provider_id): Response
    {
        $zoom_user        = $request->query->get('us');
        $zoom_event       = $request->query->get('ev');
        $zoom_provider    = $request->query->get('pr');
        $zoom_ev_provider = $request->query->get('ev_pr');
        $zoom_date        = $request->query->get('dt');

        if ($zoom_user && $zoom_event && $zoom_provider && $zoom_ev_provider)
        {
            $entityManager       = $this->getDoctrine()->getManager();
            $event_provider_user = $this->getDoctrine()->getRepository(ProviderUserEvent::class)->findOneBy(['event'          => $zoom_event,
                                                                                                             'provider'       => $zoom_provider,
                                                                                                             'user'           => $zoom_user,
                                                                                                             'event_provider' => $zoom_ev_provider,
                                                                                                             'finish_at'      => NULL]);

            if ($event_provider_user)
            {
                $event_provider_user->setFinishAt(new \DateTime('now'));
                $entityManager->persist($event_provider_user);
                $entityManager->flush();
            }
        }

        $user        = $this->getUser();
        $id          = $user->getId();
        $search      = $request->query->get('search');
        $event_id    = $this->session->get('event_id');
        $event       = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
        $content_url = $event->getContents()[0]->getFileUrl();
        $banner      = $event->getBanner();
        $banners     = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);

        // LISTA ANLITICA
        $search_query = "";

        if ($search)
            $search_query = "AND p.name LIKE '%$search%' ";

        // LISTA DE PROVEEDORES
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("
            SELECT 
            p.id, 
            p.name, 
            p.phone, 
            p.image, 
            e.id as event_provider_id,
            (
                SELECT pr.id
                FROM provider_user_event pr
                WHERE pr.provider_id = p.id AND pr.user_id = $id AND pr.event_id = $event_id
                LIMIT 1
            ) AS visited, 
            ( 
                  SELECT o.id
                  FROM offer o
                  WHERE o.provider_id = p.id AND o.event_id = $event_id
                  LIMIT 1 
            ) AS offer, 
            e.zoom_code, 
            e.zoom_number,
            e.region
            FROM event_provider e       
            LEFT JOIN provider p ON e.provider_id = p.id 
            WHERE e.event_id = $event_id                
            AND e.zoom_number IS NOT NULL AND e.zoom_number != ''
            AND e.zoom_code IS NOT NULL AND e.zoom_code !=''   
            AND p.deleted_at is NULL            
            $search_query 
            ORDER BY p.name ASC");

        $statement->execute();
        $providers         = $statement->fetchAll();
        $statement_visiter = $connection->prepare("SELECT pr.provider_id
                                                    FROM provider_user_event pr
                                                    WHERE pr.event_provider_id IS NOT NULL AND pr.user_id = $id AND pr.event_id = $event_id
                                                    GROUP BY pr.provider_id");

        $statement_visiter->execute();
        $visited = $statement_visiter->fetchAll();

        // Percent validation
        $sizeHistory = count($visited);
        $sizPercent  = $event->getPercent();
        $percent     = ($sizeHistory * 100) / $sizPercent;

        //Call Premiaton & Incentive Ligthbox
        $this->getDoctrine()->getRepository(Premiaton::class)->validate_banner();
        $this->getDoctrine()->getRepository(IncentiveProvider::class)->validate_banner();

        return $this->render('site/provider.html.twig', [
            'banners'        => $banners,
            'search'         => $search,
            'event_id'       => $event_id,
            'percent'        => $percent,
            'history'        => $sizeHistory,
            'size'           => $sizPercent,
            'providers'      => $providers,
            'banners'        => $banners,
            'content_url'    => $content_url,
            'ev_provider_id' => $ev_provider_id
        ]);
    }


    /**
     * @Route("api/provider_zoom/{event_id}/{provider_id}/{ev_provider_id}", name="api_provider_zoom")
     */
    public function apiProviderZoom(Request $request, $event_id, $provider_id, $ev_provider_id): Response
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $provederUser  = new ProviderUserEvent();
        $provider      = $this->getDoctrine()->getRepository(Provider::class)->findOneBy(['id' => $provider_id]);
        $event         = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
        $ev_provider   = $this->getDoctrine()->getRepository(EventProvider::class)->find($ev_provider_id);

        $provederUser->setUser($user);
        $provederUser->setProvider($provider);
        $provederUser->setEvent($event);
        $provederUser->setEventProvider($ev_provider);

        $entityManager->persist($provederUser);
        $entityManager->flush();

        return $this->json(['message' => 'OK']);
    }

    /**
     * @Route("zoom/update_finish_hour", name="update_finish_hour_zoom", methods={"POST"})
     */
    public function update_finish_hour_zoom(Request $request)
    {
        $hour             = $request->request->get('finish_hour');
        $zoom_event       = $request->request->get('zoom_event');
        $zoom_provider    = $request->request->get('zoom_provider');
        $zoom_ev_provider = $request->request->get('zoom_ev_provider');

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager       = $this->getDoctrine()->getManager();
        $event_provider_user = $this->getDoctrine()->getRepository(ProviderUserEvent::class)->findOneBy(['event'          => $zoom_event,
                                                                                                         'provider'       => $zoom_provider,
                                                                                                         'user'           => $this->getUser()->getId(),
                                                                                                         'event_provider' => $zoom_ev_provider], ['id' => 'desc']);

        if ($event_provider_user)
        {
            $finish_date = \DateTime::createFromFormat('Y-m-d H:i:s', $hour);
            $event_provider_user->setFinishAt($finish_date);
            $entityManager->persist($event_provider_user);
            $entityManager->flush();
        }

        $entityManager->flush();

        return $this->json([]);
    }

    /**
     * @Route("api/provider/{event_id}/{id}", name="api_provider_id")
     */
    public function apiProviderId(Request $request, $event_id, $id): Response
    {
        $ev_provider = $this->getDoctrine()->getRepository(EventProvider::class)->find($id);
        $provider    = $this->getDoctrine()->getRepository(EventProvider::class)->findProviderByEventProvider($id);
        $transferrs  = $this->getDoctrine()->getRepository(Transferrs::class)->findByProvider($ev_provider->getProvider()->getId());
        $event       = $this->getDoctrine()->getRepository(Event::class)->findEvent($event_id);

        return $this->json(['provider' => $provider, 'trasnfer' => $transferrs, 'event' => $event]);
    }

    /**
     * @return object|void
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * @Route("/providers/get/{event_id?}", name="provider_get_all")
     */
    public function get_json($event_id)
    {
        $excepts_id = $this->getDoctrine()->getRepository(EventProvider::class)->getEventProviders($event_id);
        $providers  = $this->getDoctrine()->getRepository(Provider::class)->getByExcept($excepts_id);

        return new JsonResponse($providers);
    }

    /**
     * @Route("admin/provider/show_provider_deleted", name="show_provider_deleted")
     */
    public function showProviderDeleted(Request $request): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre'])
            ->add('nit', TextColumn::class, ['field' => 'p.nit', 'label' => 'NIT'])
            ->add('representant', TextColumn::class, ['field' => 'p.representant', 'label' => 'Representante'])
            ->add('phone', TextColumn::class, ['field' => 'p.phone', 'label' => 'Teléfono'])
            ->add('email', TextColumn::class, ['field' => 'p.email', 'label' => 'Email'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($provider_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('restore_provider', ['id' => $provider_id]).'">Restaurar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Provider::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('p')->from(Provider::class, 'p')
                        ->where('p.deleted_at IS NOT NULL')
                        ->orderBy('p.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/provider/deleted.html.twig', [
            'controller_name' => 'ProviderController',
            'datatable'       => $table,
            'message'         => $message
        ]);

        $filters->enable('soft_deleteable');
    }

    /**
     * @Route("admin/provider/restore_provider/{id}", name="restore_provider")
     */
    public function restoreProviderDeleted($id): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $entityManager = $this->getDoctrine()->getManager();
        $provider      = $this->getDoctrine()->getRepository(Provider::class)->find($id);
        $provider->setDeletedAt(NULL);

        $provider->setUpdatedById($this->getUser()->getId());
        $provider->setDeletedById(NULL);

        $entityManager->persist($provider);


        $entityManager->flush();
        $filters->enable('soft_deleteable');
        $this->session->getFlashBag()->add('success', 'Proveedor activado satisfactoriamente');

        return $this->redirectToRoute('show_provider_deleted');
        //return new Response(Response::HTTP_ACCEPTED);
    }


    /**
     */
    public function verifyEmail($id = 0, Request $request)
    {

        $users = $this->getDoctrine()
            ->getRepository(Provider::class)
            ->findByEmail($request->request->get('email'), $id);

        return new JsonResponse(count($users) > 0 ? FALSE : TRUE);
    }

    /**
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * @Route("provider/get_names", name="get_providers_name")
     */
    public function get_providers_name()
    {
        $event_id  = $this->session->get('event_id');
        $providers = $this->getDoctrine()->getRepository(EventProvider::class)->getProvidersByName($event_id);

        return $this->json($providers);
    }
}
