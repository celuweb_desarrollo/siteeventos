<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\User;
use Omines\DataTablesBundle\Column\BoolColumn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Column\TextColumn;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/users", name="user_list")
     */
    public function index(Request $request): Response
    {
        $table = $this->createDataTable()
                ->add('identification', TextColumn::class, ['field' => 'p.identification', 'label' => 'Identificación'])
                ->add('full_name', TextColumn::class, ['field' => 'p.full_name', 'label' => 'Nombre Completo'])
                ->add('email', TextColumn::class, ['field' => 'u.email', 'label' => 'Email'])
                ->add('is_active',BoolColumn::class,[
                    'field' => 'u.is_active' ,
                    'label' => 'Estado',
                    'trueValue'  => '<span class="badge badge-success">Activo</span>',
                    'falseValue' => '<span class="badge badge-warning">Inactivo</span>',
                ])
                ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'u.id', 'render' => function ($user_id) {
                    return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('user_edit', ['id' => $user_id]).'">Editar</a>');
                }])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => User::class,
                    'query'  => function (QueryBuilder $builder) {
                        $builder->select('u')
                            ->from(User::class, 'u')
                            ->innerJoin('u.profile', 'p')
                            ->andWhere('u.roles LIKE :role')
                            ->setParameter('role', '%"ROLE_ADMIN"%');
                    }])
                ->handleRequest($request);

        if ($table->isCallback()){
            return $table->getResponse();
        }

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/user/index.html.twig', [
            'controller_name' => 'UserController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/user/edit/{id?}", name="user_edit")
     */
    public function edit(Request $request, $id): Response
    {

        $reg['new']          = true;
        $reg['is_active']    = 1;
        $reg['view_profile'] = $request->get('view')??null;

        if(isset($reg['view_profile']) && $reg['view_profile'] == 'profile'){
            $reg['view_profile'] = true;
        }

        if ($id > 0) {

            $reg['new']            = false;
            $user                  = $this->getDoctrine()->getRepository(User::class)->find($id);
            $profile               = $user->getProfile();
            $reg['identification'] = $profile->getIdentification();
            $reg['full_name']      = $profile->getFullName();
            $reg['phone']          = $profile->getPhone();
            $reg['is_active']      = $user->getisActive();
            $reg['email']          = $user->getEmail();
        }

        $reg['id']        = $id;

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg){
            $message .= $msg;
        }
        
        return $this->render('admin/user/edit.html.twig', compact('reg','message'));
    }

    /**
     * @Route("/user/save/{id}", name="user_save")
     */
    public function updateOrCreate(Request $request, UserPasswordEncoderInterface $encoder,  $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        if($id > 0){
            $user    = $this->getDoctrine()->getRepository(User::class)->find($id);
            $profile = $user->getProfile();
        }else{
            $user    = new User();
            $profile = new Profile();
            $user->setIsSuperAdmin(0);
        }

        $user->setEmail($request->get('email'));

        if($request->get('password') != ''){
            $encoded = $encoder->encodePassword($user, $request->get('password'));
            $user->setPassword($encoded);
        }

        $profile->setIdentification($dat['identification']);
        $profile->setFullName($dat['full_name']);
        $profile->setPhone($dat['phone']);

        $user->setIsActive($dat['is_active']);
        $user->setIsVerified(0);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setProfile($profile);
        
//        $user->setCreatedById($this->getUser()->getId());
//        $user->setUpdatedById($this->getUser()->getId());

        $entityManager->persist($profile);
        $entityManager->persist($user);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', User::SUCCESSFUL_REGISTRATION);

        return $this->redirectToRoute('user_edit', [
            'id'      => $profile->getId()
        ]);
    }

    /**
     * @Route("/user/verify-email/{id?}", name="url_verify_email")
     */
    public  function verifyEmail($id = 0, Request $request){

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByEmail($request->request->get('email') , $id);

        return new JsonResponse(count($users) > 0 ? false : true);
    }
}
