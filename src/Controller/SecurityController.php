<?php

namespace App\Controller;

use App\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SecurityController extends AbstractController
{
    private const AUTHENTICATION_LOGIN = '_security.route_login';

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/admin/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $message      = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        $sessionEmail = $this->session->get(Security::LAST_USERNAME);
        $user         = $this->getUser();

        if(isset($sessionEmail) && isset($user)){

            if($this->session->get(self::AUTHENTICATION_LOGIN) == 'associated'){
                return new RedirectResponse($this->generateUrl('associate_events'));

            }else if($this->session->get(self::AUTHENTICATION_LOGIN) == 'admin'){
                return new RedirectResponse($this->generateUrl('event_index'));
            }
        }

        return $this->render('admin/security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error, 'message' => $message
        ]);
    }

    /**
     * @Route("/admin/logout", name="admin_logout")
     */
    public function logoutAdmin()
    {
        $em      = $this->getDoctrine()->getManager();
        $user    = $this->getUser();
        $token   = $this->session->get('token_session');
        $session = $this->getDoctrine()->getRepository(Session::class)->findSessionActive($user->getId(), $token);

        if(isset($session)){
            $session->setEnd(new \DateTime('now'));
            $session->setInSession(0);
            $em->persist($session);
            $em->flush();
        }

        $this->get('security.token_storage')->setToken(null);

        $this->session->set(self::AUTHENTICATION_LOGIN, null);

        $this->get('session')->invalidate();

        return new RedirectResponse($this->generateUrl('app_login'));
    }

    /**
     * @Route("/asociado/logout", name="associated_logout")
     */
    public function logoutAssociated()
    {
        $this->get('security.token_storage')->setToken(null);

        $this->session->set(self::AUTHENTICATION_LOGIN, null);

        $this->get('session')->invalidate();

        return new RedirectResponse($this->generateUrl('associate_login'));
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(Request $request)
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
