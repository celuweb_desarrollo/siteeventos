<?php

namespace App\Controller;


use App\Entity\IncentiveProvider;
use App\Entity\Provider;
use App\Entity\Associated;
use App\Entity\WinnersProvider;
use App\Form\WinnerType;
use App\Repository\WinnersProviderRepository;
use Doctrine\ORM\QueryBuilder;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class WinnerProvController extends AbstractController
{

    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/winners_provider", name="winners_provider_index")
     */
    public function index(Request $request): Response
    {
        return $this->render('admin/incentive/index.html.twig');
    }

    /**
     * @Route("/WinnersProv/edit/{id}", name="winners_provider_edit")
     */
    public function edit(Request $request, $id): Response
    {
        if (isset($_GET['incentive_id'])) {
            $incentive_id = $_GET['incentive_id'];
        }else{
            $incentive_id = '';
        }

        $reg = [];
        $date_now = date('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT ip.id, e.title, ip.start_date FROM incentive_provider ip INNER JOIN event e ON (ip.event_id = e.id) WHERE e.final_date >= '$date_now' AND e.deleted_at IS NULL");
        $statement->execute();
        $incentives = $statement->fetchAll();
        $providers = $this->getDoctrine()->getRepository(Provider::class)->findAll();
        $associates = $this->getDoctrine()->getRepository(Associated::class)->findAll();

        if ($id > 0) {
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT wp.id, a.code, a.name, p.name s, wp.logo, ip.start_date FROM winners_provider wp INNER JOIN incentive_provider ip ON(wp.incentive_id = ip.id) INNER JOIN provider p ON (wp.provider_id = p.id) INNER JOIN associated a ON (wp.associated_id = a.id) WHERE wp.id = $id");
            $statement->execute();
            $reg = $statement->fetchAll();
        }


        //$reg['message'] = $this->session->get('message');

        $datatable = $this->createDataTable()
            ->add('code', TextColumn::class, ['field' => 'a.code', 'label' => 'Código Asociado', 'render' => function ($winner_id) {
                return sprintf('<span>' . $winner_id . '</span>');
            }])
            ->add('provider', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre Proveedor', 'render' => function ($winner_id) {
                return sprintf('<span>' . $winner_id . '</span>');
            }])
            ->add('start_date', DateTimeColumn::class, ['field' => 'ip.start_date', 'label' => 'Fecha de Inicio', 'render' => function ($winner_id) {
                return sprintf(date('g i a - j F Y', strtotime($winner_id)));
            }])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'pr.id', 'render' => function ($winner_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('winners_provider_edit', ['id' => $winner_id]) . '">Editar</a>
                              <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $winner_id . ')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => WinnersProvider::class,
                'query'  => function (QueryBuilder $builder) use ($id) {
                    $builder->select('wp')
                        ->from(WinnersProvider::class, 'wp')
                        ->join('wp.associated', 'a')
                        ->join('wp.incentive', 'ip')
                        ->join('ip.provider', 'p')
                        ->join('ip.event', 'e')
                        ->where('wp.incentive = :id')
                        ->setParameter('id', $id);
                }
            ])
            ->handleRequest($request);

        if ($datatable->isCallback())
            return $datatable->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/WinnersProvider/edit.html.twig', compact('reg', 'datatable', 'message', 'incentives', 'providers', 'associates', 'incentive_id'));
    }


    /**
     * @Route("/WinnersProvider/save/{id}", name="winners_provider_save")
     */
    public function updateOrCreate(Request $request, MaelInterventionImageManager $imageManager, $id)
    {
        $incentive_id = $_GET['incentive_id'];
        $dat = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();


        if ($id > 0)
            $winner = $this->getDoctrine()->getRepository(WinnersProvider::class)->find($id);
        else
            $winner = new WinnersProvider();

        if ($request->files->get('file')) {
            $filename = date('Ymdhis') . rand(0, 9);
            $img      = $imageManager->make($request->files->get('file'));
            $mimes    = [
                'image/jpeg' => 'jpg',
                'image/png'  => 'png',
                'image/gif'  => 'gif'
            ];

            $ext = $mimes[$img->mime];

            $img->save('upload/logos/' . $filename . '.' . $ext);

            $img->fit(400, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('upload/logos/b' . $filename . '.' . $ext);

            $winner->setLogo($filename . '.' . $ext);
        }

        $associate = $this->getDoctrine()->getRepository(Associated::class)->find(intval($dat['associate_id']));
        $provider = $this->getDoctrine()->getRepository(Provider::class)->find($dat['provider_id']);
        $incentive = $this->getDoctrine()->getRepository(IncentiveProvider::class)->find($incentive_id);
        $winner->setProvider($provider);
        $winner->setIncentive($incentive);
        $winner->setAssociated($associate);
        $entityManager->persist($winner);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', 'La información ha sido almacenada satisfactoriamente');

        return $this->redirectToRoute('incentive_edit', ['id' => $incentive_id]);
    }

    /**
     * @Route("/WinnersProvider/delete/{id?}", name="winners_provider_delete")
     */
    public function delete(Request $request, Winner $winner): Response
    {
        if ($this->isCsrfTokenValid('delete' . $winner->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($winner);
            $entityManager->flush();
        }

        return $this->redirectToRoute('winner_index');
    }


    /**
     * @Route("/WinnersProvider/export_sample_winners_provider", name="export_sample_winners_provider")
     *
     */
    public function export_sample_winner(Request $request)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Carga Ganadores");
        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'Incentivos Disponibles');
        $sheet->setCellValue('C1', 'Imagen Ganador');
        $sheet->setCellValue('D1', 'Proveedor');
        $sheet->setCellValue('E1', 'Código Asociado');

        $date_now = date('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT ip.id, ip.start_date, e.title, p.name FROM incentive_provider ip INNER JOIN provider p ON (ip.provider_id = p.id) INNER JOIN event e ON (ip.event_id = e.id) WHERE e.deleted_at IS NULL AND e.final_date >= '$date_now' GROUP BY ip.id");
        $statement->execute();
        $avalaible_events = $statement->fetchAll();
        $date = new \Datetime();
        $count = 0;
        for ($i = 2; $i < count($avalaible_events) + 2; $i++) {
            $event_date = $avalaible_events[$count]["start_date"];
            $value = $avalaible_events[$count]["title"];
            $id = $avalaible_events[$count]["id"];
            $provider = $avalaible_events[$count]["name"];
            $sheet->getCell('A' . $i)->setValue($id);
            $sheet->getCell('B' . $i)->setValue($event_date . ' - ' . $value);
            $sheet->getCell('C' . $i)->setValue('ej: incentivo' . $count . '.jpg');
            $sheet->getCell('D' . $i)->setValue($provider);
            $sheet->getCell('E' . $i)->setValue('ej:36782');
            $count++;
        }
        $writer = new Xlsx($spreadsheet);
        $fileName = 'winners_incentive_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/IncentiveProvider/import_winner", name="import_winners_provider")
     *
     */
    public function import_winnner(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            if ($_FILES["file"]["size"] < 10000000) //5Mb
            {
                if ($_FILES["file"]["error"] > 0) {
                    //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (empty($_POST) && empty($_FILES) && isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
                        //echo "The file is bigger than post_max_size in php.ini.";
                    } else {
                        if (file_exists("uploads/" . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"])) {
                            // echo $_FILES["file"]["name"] . " already exists. ";
                        } else {
                            move_uploaded_file(
                                $_FILES["file"]["tmp_name"],
                                "uploads/" . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"]
                            );
                            //echo "Stored in: " . "uploads/" . $_FILES["file"]["name"];
                            $fileUpload = './uploads/' . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"];

                            return $this->xslx_winner($fileUpload);
                        }
                    }
                }
            } else {
                /*echo "Archivo invalido";*/
            }
        }

        return $this->render('admin/IncentiveProvider/import_winner.html.twig');
    }

    /**
     * @Route("/WinnersProvider/upload-excel-winners", name="xslx_winner")
     *
     * @throws \Exception
     */
    public function xslx_winner($inputFileName)
    {
        $spreadsheet   = IOFactory::load($inputFileName);
        $row           = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData     = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row) {
            if ($Row['A'] != null && $Row['B'] != null && $Row['C'] != null && $Row['D'] != null && $Row['E'] != null) {
                $incentive = $this->getDoctrine()->getRepository(IncentiveProvider::class)->find($Row['A']);
                $provider = $this->getDoctrine()->getRepository(Provider::class)->find(intval($Row['D']));
                $asso_temp = $this->loadAssociate($Row['E']);
                $associate = $this->getDoctrine()->getRepository(Associated::class)->find(intval($asso_temp));
                if (!empty($prov_temp) && !empty($asso_temp)) {
                    $logo         = $Row['C'];
                    $winner = new WinnersProvider();
                    $winner->setProvider($provider);
                    $winner->setIncentive($incentive);
                    $winner->setLogo($logo);
                    $winner->setAssociated($associate);
                    $entityManager->persist($winner);
                    $entityManager->flush();
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('incentive_index');
    }

    public function loadAssociate($prov_name)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM associated WHERE code LIKE '%$prov_name%'");
        $statement->execute();
        $provider_temp = $statement->fetchAll();
        return $provider_temp[0]['id'];
    }
}
