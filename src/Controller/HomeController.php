<?php

namespace App\Controller;

use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\BannerImage;
use App\Entity\Event;


class HomeController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }
    
    /**
     * @Route("asociado/pqrs", name="associate_pqrs")
     */
    public function showPqrs(Request $request){

        $event_id = $request->getSession()->get('event_id');

        if(isset($event_id)){
            $event   = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
            $banner  = $event->getBanner();
            $banners = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);
        }else{
            $banners  = $this->getDoctrine()->getRepository(BannerImage::class)->findByEventStatus();
        }

        return $this->render('site/pqrs.html.twig', [
            'banners'  => $banners,
            'event_id' => $event_id,
        ]);
    }
}
