<?php

namespace App\Controller;

use App\Entity\EventProvider;
use App\Form\EventProviderType;
use App\Repository\EventProviderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventProviderController extends AbstractController
{
    /**
     * @Route("/admin/eventProvider", name="event_provider_index", methods={"GET"})
     */
    public function index(EventProviderRepository $eventProviderRepository): Response
    {
        return $this->render('admin/event_provider/index.html.twig', [
            'event_providers' => $eventProviderRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/eventProvider/new", name="event_provider_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $eventProvider = new EventProvider();
        $form = $this->createForm(EventProviderType::class, $eventProvider);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eventProvider);
            $entityManager->flush();

            return $this->redirectToRoute('event_provider_index');
        }

        return $this->render('admin/event_provider/new.html.twig', [
            'event_provider' => $eventProvider,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/eventProvider/{id}", name="event_provider_show", methods={"GET"})
     */
    public function show(EventProvider $eventProvider): Response
    {
        return $this->render('admin/event_provider/show.html.twig', [
            'event_provider' => $eventProvider,
        ]);
    }

    /**
     * @Route("/admin/eventProvider/{id}/edit", name="event_provider_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EventProvider $eventProvider): Response
    {
        $form = $this->createForm(EventProviderType::class, $eventProvider);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_provider_index');
        }

        return $this->render('admin/event_provider/edit.html.twig', [
            'event_provider' => $eventProvider,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/eventProvider/{id}", name="event_provider_delete", methods={"POST"})
     */
    public function delete(Request $request, EventProvider $eventProvider): Response
    {
        if ($this->isCsrfTokenValid('delete'.$eventProvider->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($eventProvider);
            $entityManager->flush();
        }

        return $this->redirectToRoute('event_provider_index');
    }
}
