<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\EventProvider;
use App\Entity\Provider;
use App\Entity\Associated;
use App\Entity\Drugstore;
use App\Entity\UserContent;
use App\Entity\User;
use App\Entity\ProviderUserEvent;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\DataTable;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class ReportsController extends AbstractController
{
    private $router;
    private $session;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/admin/reports", name="reports")
     */
    public function index(): Response
    {
        return $this->render('admin/reports/index.html.twig');
    }

    /**
     * @Route("/admin/reports/general", name="general_report")
     */
    public function general_report(Request $request, $event_id = 0): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/general_report.html.twig', [
            'title'  => 'Reporte General',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/general/{id?}", name="search_general_report")
     */
    public function search_general_report(Request $request, $id): Response
    {
        $table = $this->createDataTable()
            ->add('id', TextColumn::class, ['field' => 'p.id', 'label' => '#', 'visible' => TRUE])
            ->add('created_at', DateTimeColumn::class, ['field' => 'p.create_at', 'label' => 'Fecha y Hora', 'searchable' => FALSE, 'render' => function ($date) {
                return sprintf(date('F j, Y', strtotime($date)));
            }])
            ->add('code_drugstore', TextColumn::class, ['field' => 'd.code_drugstore', 'className' => 'text-center', 'label' => 'Código Drogueria'])
            ->add('name_drugstore', TextColumn::class, ['field' => 'd.name_drugstore', 'label' => 'Nombre Drogueria'])
            ->add('code_responsible', TextColumn::class, ['field' => 'd.code_responsible', 'className' => 'text-center', 'label' => 'Código Responsable'])
            ->add('name_responsible', TextColumn::class, ['field' => 'd.name_responsible', 'label' => 'Nombre Responsable'])
            ->add('nit', TextColumn::class, ['field' => 'd.nit', 'label' => 'Nit Asociado'])
            ->add('centro', TextColumn::class, ['field' => 'd.cost_center', 'label' => 'Centro'])
            ->add('provider', TextColumn::class, ['field' => 'pr.name', 'label' => 'Proveedor'])
            ->add('time_start', DateTimeColumn::class, ['field' => 'p.create_at', 'label' => 'Hora Inicial', 'searchable' => FALSE, 'className' => 'text-center', 'render' => function ($date) {
                if ($date != '')
                    return sprintf(date('H:i A', strtotime($date)));
                else
                    return 'N/A';
            }])
            ->add('time_end', DateTimeColumn::class, ['field' => 'p.finish_at', 'label' => 'Hora Final', 'searchable' => FALSE, 'className' => 'text-center', 'render' => function ($date) {
                if ($date != '')
                    return sprintf(date('H:i A', strtotime($date)));
                else
                    return 'N/A';
            }])
            ->add('total', TextColumn::class, ['field' => 'p.finish_at', 'label' => 'Hora Total', 'searchable' => FALSE, 'className' => 'text-center', 'data' => function ($data) {

                $start_hour = $data["create_at"];
                $final_hour = $data["finish_at"];

                if ($final_hour)
                {
                    $interval = $start_hour->diff($final_hour);

                    return sprintf($interval->format('%H:%I:%S'));
                } else
                    return 'N/A';
            }])
            ->addOrderBy(0, DataTable::SORT_DESCENDING)
            ->createAdapter(ORMAdapter::class, [
                'hydrate' => \Doctrine\ORM\Query::HYDRATE_ARRAY,
                'entity'  => Associated::class,
                'query'   => function (QueryBuilder $builder) use ($id) {
                    $builder
                        ->addSelect(
                            'p.id',
                            'p.create_at',
                            'p.finish_at'
                        )
                        ->addSelect(
                            'pr.name'
                        )
                        ->addSelect(
                            'd.code_drugstore',
                            'd.name_drugstore',
                            'd.code_responsible',
                            'd.name_responsible',
                            'd.nit',
                            'd.cost_center'
                        )
                        ->from(Associated::class, 'a')
                        ->innerJoin(Drugstore::class, 'd', 'WITH', 'd.code_drugstore = a.code')
                        ->innerJoin(ProviderUserEvent::class, 'p', 'WITH', 'p.user = a.user')
                        ->innerJoin(EventProvider::class, 'ep', 'WITH', 'ep = p.event_provider')
                        ->innerJoin(Provider::class, 'pr', 'WITH', 'pr = ep.provider')
                        ->where('p.event = :event_id')
                        ->setParameter('event_id', $id);
                }
            ])
            ->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
            return $table->getResponse();

        return $this->render('admin/reports/general_report.html.twig', [
            'title'     => 'Reporte General',
            'senal'     => 1,
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/consolidated_x_drugstore", name="consolidated_x_drugstore")
     */
    public function consolidated_x_drugstore(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/consolidated_x_drugstore.html.twig', [
            'title'  => 'Consolidado por Drogueria',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/consolidated_x_drugstore/{id}", name="search_consolidated_x_drugstore")
     */
    public function search_consolidated_x_drugstore(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            ass.code as code,
                                            COUNT(DISTINCT log.provider_id) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, code
                                            ORDER BY pro.name");

        $statement->execute();
        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement_days->execute();
        $event_temp = $statement->fetchAll();
        $event_days = $statement_days->fetchAll();

        $table = $this->createDataTable()
            ->add('code_drugstore', TextColumn::class, ['label' => 'Código Drogueria'])
            ->add('name_drugstore', TextColumn::class, ['label' => 'Nombre Drogueria'])
            ->add('code_responsible', TextColumn::class, ['label' => 'Código Responsable'])
            ->add('nit', TextColumn::class, ['label' => 'Nit Asociado'])
            ->add('cost_center', TextColumn::class, ['label' => 'Centro']);

        for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        {
            $table->add('Dia'.$i, TextColumn::class, ['label' => 'Dia '.$i, 'data' => function ($data) use ($event_temp, $i) {
                foreach ($event_temp as $key => $value)
                {
                    if ($value['code'] == $data['code_drugstore'] && $i == $value['day'])
                        return $value['total'];
                }

                return 0;
            }]);
        }

        $table->add('total', NumberColumn::class, ['label' => 'Total', 'data' => function ($data) {
            return $data['total'];
        }]);

        $primary = $connection->prepare("SELECT
                                          DATE(log.create_at) fecha,
                                          d.code_drugstore,
                                          d.name_drugstore,
                                          d.code_responsible,
                                          d.nit,
                                          d.cost_center,
                                        COUNT(DISTINCT log.provider_id, DATE(log.create_at)) as total
                                        FROM provider_user_event log
                                        INNER JOIN event ev ON (ev.id = log.event_id)
                                        INNER JOIN provider pro ON (pro.id = log.provider_id)
                                        INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                        INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                        WHERE log.event_id = $id
                                        GROUP BY d.name_drugstore");

        $primary->execute();
        $table->createAdapter(ArrayAdapter::class, $primary->fetchAll())->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
        {
            return $table->getResponse();
        }

        return $this->render('admin/reports/consolidated_x_drugstore.html.twig', [
            'title'     => 'Consolidado por Drogueria',
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/consolidated_x_asociated", name="consolidated_x_asociated")
     */
    public function consolidated_x_asociated(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/consolidated_x_associated.html.twig', [
            'title'  => 'Consolidado por Asociado',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/consolidated_x_asociated/{id}", name="search_consolidated_x_asociated")
     */
    public function search_consolidated_x_asociated(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT 
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            drug.nit as code,
                                            COUNT(DISTINCT log.provider_id, DATE(log.create_at)) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore drug ON (drug.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, code  
                                            ORDER BY code  DESC");

        $statement->execute();
        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement_days->execute();
        $event_temp = $statement->fetchAll();
        $event_days = $statement_days->fetchAll();

        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['label' => 'Nombre Asociado'])
            ->add('nit', TextColumn::class, ['label' => 'Nit Asociado'])
            ->add('cost_center', TextColumn::class, ['label' => 'Centro']);

        for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        {
            $table->add('Dia'.$i, TextColumn::class, ['label' => 'Dia '.$i, 'data' => function ($data) use ($event_temp, $i) {
                foreach ($event_temp as $key => $value)
                {
                    if ($value['code'] == $data['nit'] && $i == $value['day'])
                        return $value['total'];
                }

                return 0;
            }]);
        }

        $table->add('total', NumberColumn::class, ['label' => 'Total', 'data' => function ($data) {
            return $data['total'];
        }]);

        $primary = $connection->prepare("SELECT
              DATE(log.create_at) fecha,
              ass.name,
              d.code_drugstore,
              d.name_drugstore,
              d.code_responsible,
              d.nit,
              d.cost_center,
            COUNT(DISTINCT log.provider_id, DATE(log.create_at)) as total
            FROM provider_user_event log
            INNER JOIN event ev ON (ev.id = log.event_id)
            INNER JOIN provider pro ON (pro.id = log.provider_id)
            INNER JOIN associated ass ON (ass.user_id = log.user_id)
            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
            INNER JOIN drugstore drug ON (drug.code_drugstore = ass.code)
            WHERE log.event_id = $id
            GROUP BY ass.name");

        $primary->execute();
        $table->createAdapter(ArrayAdapter::class, $primary->fetchAll())->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
        {
            return $table->getResponse();
        }

        return $this->render('admin/reports/consolidated_x_associated.html.twig', [
            'title'     => 'Consolidado por Asociado',
            'senal'     => 1,
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/register_provider_x_drugstore", name="register_provider_x_drugstore")
     */
    public function register_provider_x_drugstore(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/register_provider_x_drugstore.html.twig', [
            'title'  => 'Registro Proveedor por Droguería',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/register_provider_x_drugstore/{id}", name="search_register_provider_x_drugstore")
     */
    public function search_register_provider_x_drugstore(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("
                                            SELECT 
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            pro.id provider,
                                            pro.name as name,
                                            COUNT(DISTINCT log.user_id, DATE(log.create_at)) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, provider, name
                                            ORDER BY pro.name;
        ");
        $statement->execute();
        $statement2 = $connection->prepare("SELECT 
                                            DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement2->execute();
        $event_temp = $statement->fetchAll();
        $event_days = $statement2->fetchAll();

        $count = 0;

        $table = $this->createDataTable()
            ->add('name', TextColumn::class, ['label' => 'Proveedor']);

        for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        {
            $table->add('Dia'.$i, TextColumn::class, ['label' => 'Dia '.$i, 'data' => function ($data) use ($event_temp, $i) {
                foreach ($event_temp as $key => $value)
                {
                    if ($value["provider"] == $data["id"] && $i == $value["day"])
                    {
                        return $value["total"];
                    }
                }

                return 0;
            }]);
        }

        $table->add('total', NumberColumn::class, ['label' => 'Total']);
        $primary = $connection->prepare("
                                        SELECT 
                                        pro.id id,
                                        pro.name as name,
                                        COUNT(DISTINCT log.user_id, DATE(log.create_at)) total
                                        FROM provider_user_event log
                                        INNER JOIN event ev ON (ev.id = log.event_id)
                                        INNER JOIN provider pro ON (pro.id = log.provider_id)
                                        WHERE log.event_id = $id
                                        GROUP BY pro.id, pro.name
                                        ORDER BY pro.name;
        ");

        $primary->execute();
        $table->createAdapter(ArrayAdapter::class, $primary->fetchAll())->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
            return $table->getResponse();

        return $this->render('admin/reports/register_provider_x_drugstore.html.twig', [
            'title'     => 'Registro Proveedor por Droguería',
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/register_provider_x_asociated", name="register_provider_x_asociated")
     */
    public function register_provider_x_asociated(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/register_provider_x_associated.html.twig', [
            'title'  => 'Registro Proveedor por Asociado',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/register_provider_x_asociated/{id}", name="search_register_provider_x_asociated")
     */
    public function search_register_provider_x_asociated(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT (DATEDIFF(log.create_at, ev.start_date) + 1) day, 
                                            DATE(log.create_at) fecha, 
                                            pro.id provider,
                                            pro.name AS name, 
                                            COUNT(DISTINCT d.nit, 
                                            DATE(log.create_at)) as total 
                                            FROM provider_user_event log 
                                            INNER JOIN event ev ON (ev.id = log.event_id) 
                                            INNER JOIN provider pro ON (pro.id = log.provider_id) 
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id) 
                                            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY fecha, pro.id, pro.name 
                                            ORDER BY pro.name ASC;");
        $statement->execute();
        $statement_day = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement_day->execute();
        $event_temp = $statement->fetchAll();
        $event_days = $statement_day->fetchAll();
        $count      = 0;

        $table = $this->createDataTable()->add('name', TextColumn::class, ['label' => 'Proveedor']);

        for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        {
            $table->add('day_'.$i, TextColumn::class, ['label' => 'Día '.$i, 'data' => function ($data) use ($event_temp, $i) {

                foreach ($event_temp as $key => $value)
                    if ($value["provider"] == $data["id"] && $i == $value["day"])
                        return $value["total"];

                return 0;
            }]);
        }

        $table->add('total', NumberColumn::class, ['label' => 'Total']);
        $primary = $connection->prepare("SELECT                                        
                                            pro.id as id,
                                            pro.name AS name, 
                                            COUNT(DISTINCT d.nit, 
                                            DATE(log.create_at)) as total 
                                            FROM provider_user_event log 
                                            INNER JOIN event ev ON (ev.id = log.event_id) 
                                            INNER JOIN provider pro ON (pro.id = log.provider_id) 
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id) 
                                            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                            WHERE log.event_id = $id 
                                            GROUP BY pro.id, pro.name 
                                            ORDER BY pro.name ASC;");

        $primary->execute();
        $table->createAdapter(ArrayAdapter::class, $primary->fetchAll())->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
            return $table->getResponse();

        return $this->render('admin/reports/register_provider_x_associated.html.twig', [
            'title'     => 'Registro Proveedor por Asociado',
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/register_drugstore_x_time", name="register_drugstore_x_time")
     */
    public function register_drugstore_x_time(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/register_drugstore_x_hour.html.twig', [
            'title'  => 'Registro Droguería por Hora',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/register_drugstore_x_time/{id}", name="search_register_drugstore_x_time")
     */
    public function search_register_drugstore_x_time(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("SELECT 
                                            (DATEDIFF(pre.create_at, ev.start_date)) day,
                                            DATE_FORMAT(pre.create_at, '%H:%i:%s') time                                        
                                            FROM provider_user_event pre
                                            INNER JOIN associated ass ON (ass.user_id = pre.user_id)
                                            INNER JOIN event ev ON (ev.id = pre.event_id)
                                            WHERE pre.event_id = $id");

        $statement->execute();
        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as event_days FROM event WHERE id LIKE $id");

        $statement_days->execute();

        $event_temp = $statement->fetchAll();
        $event_days = $statement_days->fetchAll();

        $table         = $this->createDataTable()->add('label', TextColumn::class, ['label' => 'Rango Horas']);
        $total_ev_days = $event_days[0]["event_days"];
        $result        = [];
        $hours         = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18'];

        for ($i = 0; $i < $total_ev_days; $i++)
            foreach ($hours as $hour)
            {
                $hour_count = 0;

                foreach ($event_temp as $item)
                    if ((strtotime($item["time"]) >= strtotime($hour.':00:00')) and (strtotime($item["time"]) <= strtotime($hour.':59:59')) and $item['day'] == $i)
                        $hour_count++;

                array_push($result, ['day' => $i, 'total' => $hour_count, 'hour' => $hour]);
            }


        for ($i = 0; $i < $total_ev_days; $i++)
        {
            $table->add('day_'.$i, TextColumn::class, ['label' => 'Día '.($i + 1), 'className' => 'text-center', 'data' => function ($data) use ($result, $i) {

                $total = 0;

                foreach ($result as $item)
                    if ((intval($i) == intval($item["day"])) and $data['hour'] == $item['hour'])
                        $total = $item["total"];

                return $total;
            }]);
        }

        $table->add('total', TextColumn::class, ['label' => 'Total ', 'className' => 'text-center', 'data' => function ($data) use ($result, $total_ev_days) {

            $total = 0;

            for ($i = 0; $i < $total_ev_days; $i++)
                foreach ($result as $item)
                    if ((intval($i) == intval($item["day"])) and $data['hour'] == $item['hour'])
                        $total += $item["total"];

            return $total;
        }]);

        $table->createAdapter(ArrayAdapter::class, [
            ['label' => '6:00 AM - 6:59 AM', 'hour' => '06'],
            ['label' => '7:00 AM - 7:59 AM', 'hour' => '07'],
            ['label' => '8:00 AM - 8:59 AM', 'hour' => '08'],
            ['label' => '9:00 AM - 9:59 AM', 'hour' => '09'],
            ['label' => '10:00 AM - 10:59 AM', 'hour' => '10'],
            ['label' => '11:00 AM - 11:59 PM', 'hour' => '11'],
            ['label' => '12:00 PM - 12:59 PM', 'hour' => '12'],
            ['label' => '1:00 PM - 1:59 PM', 'hour' => '13'],
            ['label' => '2:00 PM - 2:59 PM', 'hour' => '14'],
            ['label' => '3:00 PM - 3:59 PM', 'hour' => '15'],
            ['label' => '4:00 PM - 4:59 PM', 'hour' => '16'],
            ['label' => '5:00 PM - 5:59 PM', 'hour' => '17'],
            ['label' => '6:00 PM - 7:00 PM', 'hour' => '18']
        ])->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
            return $table->getResponse();

        return $this->render('admin/reports/register_drugstore_x_hour.html.twig', [
            'title'     => 'Registro Droguería por Hora',
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/total_general", name="total_general")
     */
    public function total_general(Request $request): Response
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();

        return $this->render('admin/reports/total_report.html.twig', [
            'title'  => 'Total General',
            'events' => $events
        ]);
    }

    /**
     * @Route("/admin/reports/total_general/{id}", name="search_total_general")
     */
    public function search_total_general(Request $request, $id): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("
                                            SELECT 
                                            '0' as type,
                                            COUNT(DISTINCT log.user_id) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            WHERE log.event_id = $id
                                            UNION ALL
                                            SELECT 
                                            '1' as type,
                                            COUNT(DISTINCT dru.nit) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore dru ON (dru.code_drugstore = ass.code)
                                            WHERE log.event_id = $id;");

        $statement->execute();
        $statement2 = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement2->execute();
        $event_temp = $statement->fetchAll();

        $event_days = $statement2->fetchAll();
        $count      = 0;
        $table      = $this->createDataTable()->add('label', TextColumn::class, ['label' => '']);

        // for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        // {
        //     $table->add('Dia'.$i, TextColumn::class, ['label' => 'Dia '.$i, 'data' => function ($data) use ($event_temp, $i) {
        //         foreach ($event_temp as $key => $value)
        //             if ($value["type"] == $data["type"] && $i == $value["day"])
        //                 return $value["total"];

        //         return 0;
        //     }]);
        // }

        $table->add('Total', TextColumn::class, ['label' => 'Total', 'data' => function ($data) use ($event_temp, $event_days) {

            // $total = 0;

            // for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
            //     foreach ($event_temp as $key => $value)
            //         if ($value["type"] == $data["type"] && $i == $value["day"])
            //         {
            //             $total += $value["total"];
            //             break;
            //         }

            return $event_temp[$data["type"]]["total"];
        }]);

        $table->createAdapter(ArrayAdapter::class, [
            ['label' => 'Droguerías', 'type' => 0],
            ['label' => 'Asociados', 'type' => 1]
        ])
            ->handleRequest($request);

        $events     = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        $eventsName = $this->getDoctrine()->getManager()->getRepository(Event::class)->find($id);

        if ($table->isCallback())
            return $table->getResponse();

        return $this->render('admin/reports/total_report.html.twig', [
            'title'     => 'Total General',
            'datatable' => $table,
            'events'    => $events,
            'event_id'  => $id,
            'eventName' => $eventsName
        ]);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_general_report/{id}", name="export_prov_ev_sample_general_report")
     *
     */
    public function export_prov_ev_sample_general_report(Request $request, $id)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Reporte general por evento")
            ->setCellValue('A1', 'Fecha y Hora')
            ->setCellValue('B1', 'Código Drogueria')
            ->setCellValue('C1', 'Nombre Drogueria')
            ->setCellValue('D1', 'Código Responsable')
            ->setCellValue('E1', 'Nombre Responsable')
            ->setCellValue('F1', 'Nit Asociado')
            ->setCellValue('G1', 'Centro')
            ->setCellValue('H1', 'Proveedor')
            ->setCellValue('I1', 'Hora Inicial')
            ->setCellValue('J1', 'Hora Final')
            ->setCellValue('K1', 'Hora Total');

        $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];

        foreach ($columns as $column)
        {
            $sheet->getColumnDimension($column)->setAutoSize(TRUE);
            $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:K1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:K1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT 
                                                a.created_at,
                                                d.code_drugstore,
                                                d.name_drugstore,
                                                d.code_responsible,
                                                d.name_responsible,
                                                d.nit,
                                                d.cost_center,
                                                pr.name,
                                                p.create_at,
                                                p.finish_at
                                            FROM
                                                associated AS a 
                                                INNER JOIN drugstore AS d ON d.code_drugstore = a.code 
                                                INNER JOIN provider_user_event AS p ON p.user_id = a.user_id 
                                                INNER JOIN event_provider AS ep ON ep.id = p.event_provider_id
                                                INNER JOIN provider AS pr ON pr.id = ep.provider_id
                                            WHERE
                                                p.event_id = $id");
        $statement->execute();
        $avalaible_events = $statement->fetchAll();
        $count            = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++)
        {
            $prov_create_at = $avalaible_events[$count]["create_at"] ? sprintf(date('H:i A', strtotime($avalaible_events[$count]["create_at"]))) : 'N/A';
            $prov_finish_at = $avalaible_events[$count]["finish_at"] ? sprintf(date('H:i A', strtotime($avalaible_events[$count]["finish_at"]))) : 'N/A';

            $start_date = $avalaible_events[$count]["create_at"] ? new \DateTime($avalaible_events[$count]["create_at"]) : '';
            $final_date = $avalaible_events[$count]["finish_at"] ? new \DateTime($avalaible_events[$count]["finish_at"]) : '';

            if ($start_date && $final_date)
            {
                $interval      = $start_date->diff($final_date);
                $prov_total_at = sprintf($interval->format('%H:%I:%S'));
            } else
                $prov_total_at = 'N/A';

            $sheet->getCell('A'.$i)->setValue(sprintf(date('F j, Y', strtotime($avalaible_events[$count]["create_at"]))));
            $sheet->getCell('B'.$i)->setValue($avalaible_events[$count]["code_drugstore"]);
            $sheet->getCell('C'.$i)->setValue($avalaible_events[$count]["name_drugstore"]);
            $sheet->getCell('D'.$i)->setValue($avalaible_events[$count]["code_responsible"]);
            $sheet->getCell('E'.$i)->setValue($avalaible_events[$count]["name_responsible"]);
            $sheet->getCell('F'.$i)->setValue($avalaible_events[$count]['nit']);
            $sheet->getCell('G'.$i)->setValue($avalaible_events[$count]['cost_center']);
            $sheet->getCell('H'.$i)->setValue($avalaible_events[$count]['name']);
            $sheet->getCell('I'.$i)->setValue($prov_create_at);
            $sheet->getCell('J'.$i)->setValue($prov_finish_at);
            $sheet->getCell('K'.$i)->setValue($prov_total_at);

            $count++;
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'reporte_general_'.date('Ymdhis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_consolidated_x_drugstore/{id}", name="export_prov_ev_sample_consolidated_x_drugstore")
     *
     */
    public function export_prov_ev_sample_consolidated_x_drugstore(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("SELECT
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            ass.code as code,
                                            COUNT(DISTINCT log.provider_id) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, code
                                            ORDER BY pro.name");
        $statement->execute();
        $event_temp = $statement->fetchAll();
        $event_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as days FROM event WHERE id LIKE $id");
        $event_days->execute();
        $event_days = $event_days->fetchAll()[0]['days'];

        $results = $connection->prepare("SELECT
                                              DATE(log.create_at) fecha,
                                              d.code_drugstore,
                                              d.name_drugstore,
                                              d.code_responsible,
                                              d.nit,
                                              d.cost_center,
                                            COUNT(DISTINCT log.provider_id, DATE(log.create_at)) as total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY d.name_drugstore");
        $results->execute();
        $results = $results->fetchAll();

        $spreadsheet = new Spreadsheet();
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Proveedores Eventos ");
        $sheet->setCellValue('A1', 'Código Drogueria');
        $sheet->setCellValue('B1', 'Nombre Drogueria');
        $sheet->setCellValue('C1', 'Código Responsable');
        $sheet->setCellValue('D1', 'Nit Asociado');
        $sheet->setCellValue('E1', 'Centro');

        $columns = [];
        foreach (range('a', 'z') as $column)
            array_push($columns, strtoupper($column));

        $index         = 5;
        $number_column = 1;
        for ($i = 1; $i <= $event_days; $i++)
            $sheet->setCellValue($columns[$index++].$number_column, 'Día '.$i);

        $sheet->setCellValue($columns[$index].'1', 'Total');

        foreach ($columns as $key => $column)
        {
            if ($key <= $event_days)
            {
                $sheet->getColumnDimension($column)->setAutoSize(TRUE);
                $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            }
        }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:'.$columns[$event_days + 5].'1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:'.$columns[$event_days + 5].'1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $cont = 0;
        for ($i = 2; $i < count($results) + 2; $i++)
        {
            $total          = $results[$cont]['total'];
            $code_drugstore = $results[$cont]['code_drugstore'];
            $sheet->getCell('A'.$i)->setValue($results[$cont]['code_drugstore']);
            $sheet->getCell('B'.$i)->setValue($results[$cont]['name_drugstore']);
            $sheet->getCell('C'.$i)->setValue($results[$cont]['code_responsible']);
            $sheet->getCell('D'.$i)->setValue($results[$cont]['nit']);
            $sheet->getCell('E'.$i)->setValue($results[$cont++]['cost_center']);

            $index_column = 5;
            for ($j = 1; $j <= $event_days; $j++)
            {
                foreach ($event_temp as $key => $value)
                {
                    if ($value['code'] == $code_drugstore && $j == $value['day'])
                    {
                        $sheet->getCell($columns[$index_column].$i)->setValue($value['total']);
                        break;
                    } else
                        $sheet->getCell($columns[$index_column].$i)->setValue('0');

                }
                $index_column++;
            }
            $sheet->getCell($columns[$index_column].$i)->setValue($total);
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'reporte_consolidado_drogueria_'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_consolidated_x_asociated/{id}", name="export_prov_ev_sample_consolidated_x_asociated")
     *
     */
    public function export_prov_ev_sample_consolidated_x_asociated(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("SELECT 
                                            ass.name as name,
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            drug.nit as code,
                                            COUNT(DISTINCT log.provider_id, DATE(log.create_at)) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore drug ON (drug.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, code  
                                            ORDER BY code  DESC");

        $statement->execute();
        $event_temp = $statement->fetchAll();

        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as days FROM event WHERE id LIKE $id");
        $statement_days->execute();
        $event_days = $statement_days->fetchAll()[0]['days'];

        $statement_total = $connection->prepare("SELECT 
                                                DATE(log.create_at) fecha,
                                                  ass.name as name,
                                                  d.code_drugstore,
                                                  d.name_drugstore,
                                                  d.code_responsible,
                                                  d.nit,
                                                  d.cost_center as cost_center,
                                                COUNT(DISTINCT log.provider_id, DATE(log.create_at)) as total
                                                FROM provider_user_event log
                                                INNER JOIN event ev ON (ev.id = log.event_id)
                                                INNER JOIN provider pro ON (pro.id = log.provider_id)
                                                INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                                INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                                INNER JOIN drugstore drug ON (drug.code_drugstore = ass.code)
                                                WHERE log.event_id = $id
                                                GROUP BY ass.name");

        $statement_total->execute();
        $avalaible_events = $statement_total->fetchAll();
        $count            = 0;
        $spreadsheet      = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Consolidado por Asociado");
        $sheet->setCellValue('A1', 'Asociado');
        $sheet->setCellValue('B1', 'Nit');
        $sheet->setCellValue('C1', 'Centro');

        $abc = array();

        foreach (range('a', 'z') as $letter)
            array_push($abc, strtoupper($letter));

        $index_letter  = 3;
        $number_column = 1;

        for ($i = 1; $i <= $event_days; $i++)
        {
            $sheet->setCellValue($abc[$index_letter].$number_column, 'Día '.$i);
            $index_letter++;
        }

        $sheet->setCellValue($abc[$index_letter]."1", 'Total');

        foreach ($abc as $key => $column)
        {
            if ($key <= $event_days)
            {
                $sheet->getColumnDimension($column)->setAutoSize(TRUE);
                $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            }
        }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:'.$abc[$event_days + 3].'1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:'.$abc[$event_days + 3].'1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $total_cont = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++)
        {
            $associated_name = $avalaible_events[$count]["name"];
            $associated_nit  = $avalaible_events[$count]["nit"];
            $associated_cost = $avalaible_events[$count]["cost_center"];

            $prov_total = $avalaible_events[$count]["total"];
            $sheet->getCell('A'.$i)->setValue($associated_name);
            $sheet->getCell('B'.$i)->setValue($associated_nit);
            $sheet->getCell('C'.$i)->setValue($associated_cost);

            $index_letter = 3;

            for ($j = 1; $j <= $event_days; $j++)
            {
                foreach ($event_temp as $key => $value)
                    if ($value["name"] == $associated_name && $j == $value["day"])
                    {
                        $sheet->getCell($abc[$index_letter].$i)->setValue($value["total"]);
                        break;
                    } else
                        $sheet->getCell($abc[$index_letter].$i)->setValue(0);

                $index_letter++;
            }

            $final_letter = $abc[$index_letter];
            $total_cont   += $prov_total;

            $sheet->getCell($abc[$index_letter].$i)->setValue($prov_total);

            $count++;
        }

        $position = $count + 2;
        $sheet->setCellValue($final_letter.$position, '');
        $sheet->getCell($final_letter.$position)->setValue($total_cont);

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'consolidado_por_asociado_'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_register_provider_x_drugstore/{id}", name="export_prov_ev_sample_register_provider_x_drugstore")
     *
     */
    public function export_register_provider_x_drugstore(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT 
                                                (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                                DATE(log.create_at) fecha,
                                                pro.id provider,
                                                pro.name name,
                                                COUNT(DISTINCT log.user_id, DATE(log.create_at)) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, provider, name
                                            ORDER BY pro.name;");
        $statement->execute();
        $event_temp     = $statement->fetchAll();
        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement_days->execute();

        $total_event_day = $statement_days->fetchAll();
        $event_days      = $total_event_day[0]['dias'];

        $statement_total = $connection->prepare("SELECT p.id, a.id, a.name, d.division, COUNT(DISTINCT p.user_id, DATE(p.create_at)) as total FROM provider as a INNER JOIN provider_user_event AS p ON p.provider_id = a.id INNER JOIN associated AS c ON c.user_id = p.user_id INNER JOIN drugstore AS d ON d.code_drugstore = c.code WHERE p.event_id = $id GROUP BY a.id, p.provider_id ORDER BY a.name");
        $statement_total->execute();

        $avalaible_events = $statement_total->fetchAll();
        $count            = 0;

        $spreadsheet = new Spreadsheet();
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Reg Proveedor por Drogueria");
        $sheet->setCellValue('A1', 'Proveedor');
        $abc = array();

        foreach (range('a', 'z') as $letra)
            array_push($abc, strtoupper($letra));

        $index_letter  = 1;
        $column_number = 1;

        for ($i = 1; $i <= $event_days; $i++)
        {
            $sheet->setCellValue($abc[$index_letter].$column_number, 'Día '.$i);
            $index_letter++;
        }

        $sheet->setCellValue($abc[$index_letter]."1", 'Total');
        $total_cont = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++)
        {
            $event_id   = $avalaible_events[$count]["id"];
            $event_name = $avalaible_events[$count]["name"];
            $prov_total = $avalaible_events[$count]["total"];
            $sheet->getCell('A'.$i)->setValue($event_name);
            $index_letter = 1;
            for ($j = 1; $j <= $event_days; $j++)
            {
                foreach ($event_temp as $key => $value)
                    if ($value["provider"] == $event_id && $j == $value["day"])
                    {
                        $sheet->getCell($abc[$index_letter].$i)->setValue($value["total"]);
                        break;
                    } else
                        $sheet->getCell($abc[$index_letter].$i)->setValue(0);

                $index_letter++;
            }

            $total_row  = $abc[$index_letter];
            $total_cont += $prov_total;
            $sheet->getCell($abc[$index_letter].$i)->setValue($prov_total);
            $count++;
        }

        $position = $count + 2;
        $sheet->setCellValue($total_row.$position, '');
        $sheet->getCell($total_row.$position)->setValue($total_cont);
        $index_letter  = 1;
        $column_number = 1;

        foreach ($abc as $key => $column)
            if ($key <= $event_days)
            {
                $sheet->getColumnDimension($column)->setAutoSize(TRUE);
                $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:'.$abc[$event_days + 1].'1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:'.$abc[$event_days + 1].'1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'reporte_registro_proveedor_por_drogueria_'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_register_provider_x_asociated/{id}", name="export_prov_ev_sample_register_provider_x_asociated")
     *
     */
    public function export_prov_ev_sample_register_provider_x_asociated(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("SELECT 
                                            (DATEDIFF(log.create_at, ev.start_date) + 1) day,
                                            DATE(log.create_at) fecha,
                                            pro.id AS provider,
                                            pro.name AS name,
                                            COUNT(DISTINCT d.nit,  DATE(log.create_at)) as total
                                            FROM provider_user_event log
                                            INNER JOIN 	event ev ON (ev.id = log.event_id)
                                            INNER JOIN provider pro ON (pro.id = log.provider_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY day, fecha, pro.id, pro.name
                                            ORDER BY pro.name ASC");

        $statement->execute();
        $event_temp = $statement->fetchAll();

        $statement_day = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id LIKE $id");
        $statement_day->execute();
        $event_days = $statement_day->fetchAll();

        $statement_available = $connection->prepare("SELECT 
                                            pro.id as id,
                                            pro.name AS name, 
                                            COUNT(DISTINCT d.nit, 
                                            DATE(log.create_at)) as total 
                                            FROM provider_user_event log 
                                            INNER JOIN event ev ON (ev.id = log.event_id) 
                                            INNER JOIN provider pro ON (pro.id = log.provider_id) 
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id) 
                                            INNER JOIN drugstore d ON (d.code_drugstore = ass.code)
                                            WHERE log.event_id = $id
                                            GROUP BY pro.id, pro.name 
                                            ORDER BY pro.name ASC;");

        $statement_available->execute();
        $avalaible_events = $statement_available->fetchAll();

        $count = 0;

        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Proveedores Eventos ");
        $sheet->setCellValue('A1', 'Asociado');


        $abc = array();

        foreach (range('a', 'z') as $letra)
            array_push($abc, strtoupper($letra));

        $indiceAlfabeto = 1;
        $numeroColumna  = 1;

        for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        {
            $sheet->setCellValue($abc[$indiceAlfabeto].$numeroColumna, 'Dia '.$i);

            $indiceAlfabeto++;
        }

        $sheet->setCellValue($abc[$indiceAlfabeto]."1", 'Total');

        $contadorTotal = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++)
        {
            $event_id   = $avalaible_events[$count]["id"];
            $event_name = $avalaible_events[$count]["name"];
            $prov_total = $avalaible_events[$count]["total"];
            $sheet->getCell('A'.$i)->setValue($event_name);

            $indiceAlfabeto = 1;

            for ($j = 1; $j <= $event_days[0]["dias"]; $j++)
            {

                foreach ($event_temp as $key => $value)
                {

                    if ($value["provider"] == $event_id && $j == $value["day"])
                    {
                        $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue($value["total"]);
                        break;
                    } else
                    {
                        $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue(0);
                    }

                }
                $indiceAlfabeto++;
            }

            $letraFinal = $abc[$indiceAlfabeto];

            $contadorTotal += $prov_total;

            $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue($prov_total);

            $count++;
        }

        $posicion = $count + 2;
        $sheet->setCellValue($letraFinal.$posicion, '');
        $sheet->getCell($letraFinal.$posicion)->setValue($contadorTotal);

        foreach ($abc as $key => $column)
            if ($key <= $event_days)
            {
                $sheet->getColumnDimension($column)->setAutoSize(TRUE);
                $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:'.$abc[$event_days[0]['dias'] + 1].'1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:'.$abc[$event_days[0]['dias'] + 1].'1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'registro_proveedores_por_asociado_'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_register_drugstore_x_time/{id}", name="export_prov_ev_sample_register_drugstore_x_time")
     *
     */
    public function export_prov_ev_sample_register_drugstore_x_time(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("SELECT 
                                            (DATEDIFF(pre.create_at, ev.start_date)) day,
                                            DATE_FORMAT(pre.create_at, '%H:%i:%s') time                                        
                                            FROM provider_user_event pre
                                            INNER JOIN associated ass ON (ass.user_id = pre.user_id)
                                            INNER JOIN event ev ON (ev.id = pre.event_id)
                                            WHERE pre.event_id = $id");

        $statement->execute();
        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as event_days FROM event WHERE id LIKE $id");

        $statement_days->execute();

        $event_temp = $statement->fetchAll();
        $event_days = $statement_days->fetchAll();

        $table         = $this->createDataTable()->add('label', TextColumn::class, ['label' => 'Rango Horas']);
        $total_ev_days = $event_days[0]["event_days"];
        $result        = [];
        $totals        = [];
        $hours         = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18'];

        for ($i = 0; $i < $total_ev_days; $i++)
        {
            foreach ($hours as $hour)
            {
                $hour_count = 0;

                foreach ($event_temp as $item)
                    if ((strtotime($item["time"]) >= strtotime($hour.':00:00')) and (strtotime($item["time"]) <= strtotime($hour.':59:59')) and $item['day'] == $i)
                        $hour_count++;

                array_push($result, ['day' => $i, 'total' => $hour_count, 'hour' => $hour]);
            }
        }

        $spreadsheet = new Spreadsheet();

        $data_hours = [['label' => '6:00 AM - 6:59 AM', 'hour' => '06'],
                       ['label' => '7:00 AM - 7:59 AM', 'hour' => '07'],
                       ['label' => '8:00 AM - 8:59 AM', 'hour' => '08'],
                       ['label' => '9:00 AM - 9:59 AM', 'hour' => '09'],
                       ['label' => '10:00 AM - 10:59 AM', 'hour' => '10'],
                       ['label' => '11:00 AM - 11:59 PM', 'hour' => '11'],
                       ['label' => '12:00 PM - 12:59 PM', 'hour' => '12'],
                       ['label' => '1:00 PM - 1:59 PM', 'hour' => '13'],
                       ['label' => '2:00 PM - 2:59 PM', 'hour' => '14'],
                       ['label' => '3:00 PM - 3:59 PM', 'hour' => '15'],
                       ['label' => '4:00 PM - 4:59 PM', 'hour' => '16'],
                       ['label' => '5:00 PM - 5:59 PM', 'hour' => '17'],
                       ['label' => '6:00 PM - 7:00 PM', 'hour' => '18']];

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Reporte droguerías por hora");
        $sheet->setCellValue('A1', 'Rango de Horas');

        $label = 2;
        foreach ($data_hours as $hour)
        {
            $sheet->setCellValue('A'.$label, $hour['label']);
            $label++;
        }

        $abc = array();

        foreach (range('A', 'Z') as $letter)
            array_push($abc, strtoupper($letter));

        $index_letter  = 1;
        $column_number = 1;

        foreach ($abc as $key => $column)
        {
            if ($key <= $total_ev_days)
            {
                $sheet->getColumnDimension($column)->setAutoSize(TRUE);
                $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            }
        }

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:'.$abc[$total_ev_days + 1].'1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:'.$abc[$total_ev_days + 1].'1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        for ($i = 1; $i <= $total_ev_days; $i++)
        {
            $sheet->setCellValue($abc[$index_letter].$column_number, 'Día '.$i);
            $index_letter++;
        }

        $sheet->setCellValue($abc[$index_letter]."1", 'Total');
        $index_letter = 1;

        for ($i = 0; $i < $total_ev_days; $i++)
        {
            $total  = 0;
            $column = 2;

            foreach ($result as $item)
                if ((intval($i) == intval($item["day"])))
                {
                    $sheet->setCellValue($abc[$index_letter].$column, $item["total"]);
                    $column++;
                }

            $index_letter++;
        }

        $totals = [];

        foreach ($data_hours as $hour)
        {
            $total = 0;
            foreach ($result as $item)
                if ($hour['hour'] == $item['hour'])
                    $total += $item["total"];

            array_push($totals, $total);
        }

        $column = 2;

        foreach ($totals as $item)
        {
            $sheet->setCellValue($abc[$total_ev_days + 1].$column, $item);
            $column++;
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'registro_drogueria_por_hora'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/reports/export_prov_ev_sample_total_general/{id}", name="export_prov_ev_sample_total_general")
     *
     */
    public function export_prov_ev_sample_total_general(Request $request, $id)
    {
        $em                = $this->getDoctrine()->getManager();
        $connection        = $em->getConnection();
        $statement_general = $connection->prepare("
                                            SELECT 
                                            '0' as type,
                                            COUNT(DISTINCT log.user_id) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            WHERE log.event_id = $id
                                            UNION ALL
                                            SELECT 
                                            '1' as type,
                                            COUNT(DISTINCT dru.nit) total
                                            FROM provider_user_event log
                                            INNER JOIN event ev ON (ev.id = log.event_id)
                                            INNER JOIN associated ass ON (ass.user_id = log.user_id)
                                            INNER JOIN drugstore dru ON (dru.code_drugstore = ass.code)
                                            WHERE log.event_id = $id;");

        $statement_general->execute();
        $event_temp = $statement_general->fetchAll();

        $statement_days = $connection->prepare("SELECT DATEDIFF(final_date, start_date) + 1 as dias FROM event WHERE id = $id");
        $statement_days->execute();
        $event_days = $statement_days->fetchAll();

        $spreadsheet = new Spreadsheet();
        $data        = array(
            ['label' => 'Droguerías', 'type' => 0],
            ['label' => 'Asociados', 'type' => 1]
        );

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Reporte General");
        $sheet->setCellValue('A1', 'Tipo Usuario');
        $sheet->setCellValue('A2', 'Droguerías');
        $sheet->setCellValue('A3', 'Asociados');

        // $abc = array();
        // foreach (range('a', 'z') as $letra)
        // {
        //     array_push($abc, strtoupper($letra));
        // }

        // $indiceAlfabeto = 1;
        // $numeroColumna  = 1;
        // for ($i = 1; $i <= $event_days[0]["dias"]; $i++)
        // {
        //     $sheet->setCellValue($abc[$indiceAlfabeto].$numeroColumna, 'Dia '.$i);
        //     $indiceAlfabeto++;
        // }

        $sheet->setCellValue("B1", 'Total');
        $sheet->getCell("B2")->setValue($event_temp[0]["total"]);
        $sheet->getCell("B3")->setValue($event_temp[1]["total"]);
        // $count = 0;
        // for ($i = 2; $i < count($data) + 2; $i++)
        // {
        //     $type           = $data[$count]["type"];
        //     $indiceAlfabeto = 1;
        //     $total          = 0;

        //     for ($j = 1; $j <= $event_days[0]["dias"]; $j++)
        //     {
        //         foreach ($event_temp as $key => $value)
        //         {
        //             if ($value["type"] == $type && $j == $value["day"])
        //             {
        //                 $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue($value["total"]);
        //                 $total += $value["total"];
        //                 break;
        //             } else
        //             {
        //                 $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue(0);
        //                 $total = $total;
        //             }
        //         }
        //         $indiceAlfabeto++;
        //     }

        //     $sheet->getCell($abc[$indiceAlfabeto].$i)->setValue($total);
        //     $count++;
        // }

        // $index_letter  = 1;
        // $column_number = 1;
        // foreach ($abc as $key => $column)
        // {
        //     if ($key <= $event_days[0]["dias"])
        //     {
        //         $sheet->getColumnDimension($column)->setAutoSize(TRUE);
        //         $sheet->getStyle($column)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        //     }
        // }

        $sheet->getColumnDimension("A")->setAutoSize(TRUE);
        $sheet->getColumnDimension("B")->setAutoSize(TRUE);

        $styleArray = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color'       => ['argb' => '0000000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:B1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('85C1E9');

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'total_general_'.date('YmdHis').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
