<?php

namespace App\Controller;

use App\Entity\Associated;
use App\Form\AssociatedType;
use App\Repository\AssociatedRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AssociatedController extends AbstractController
{
    /**
     * @Route("/admin/associated", name="associated_index", methods={"GET"})
     */
    public function index(AssociatedRepository $associatedRepository): Response
    {
        return $this->render('admin/associated/index.html.twig', [
            'associateds' => $associatedRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/associated/new", name="associated_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $associated = new Associated();
        $form = $this->createForm(AssociatedType::class, $associated);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($associated);
            $entityManager->flush();

            return $this->redirectToRoute('associated_index');
        }

        return $this->render('admin/associated/new.html.twig', [
            'associated' => $associated,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/associated/{id}", name="associated_show", methods={"GET"})
     */
    public function show(Associated $associated): Response
    {
        return $this->render('admin/associated/show.html.twig', [
            'associated' => $associated,
        ]);
    }

    /**
     * @Route("/admin/associated/{id}/edit", name="associated_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Associated $associated): Response
    {
        $form = $this->createForm(AssociatedType::class, $associated);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('associated_index');
        }

        return $this->render('admin/associated/edit.html.twig', [
            'associated' => $associated,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/associated/{id}", name="associated_delete", methods={"POST"})
     */
    public function delete(Request $request, Associated $associated): Response
    {
        if ($this->isCsrfTokenValid('delete'.$associated->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($associated);
            $entityManager->flush();
        }

        return $this->redirectToRoute('associated_index');
    }
}
