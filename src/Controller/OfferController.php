<?php

namespace App\Controller;

use App\Entity\BannerImage;
use App\Entity\EventProvider;
use App\Entity\Offer;
use App\Entity\Provider;
use App\Entity\Event;

use Doctrine\ORM\QueryBuilder;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\BoolColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


class OfferController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/admin/offer/offer_list", name="offer_list")
     */
    public function index(Request $request): Response
    {
        $actualDate = date('Y-m-d H:i:s');
        $table      = $this->createDataTable()
            ->add('event_id', TextColumn::class, ['field' => 'e.title', 'label' => 'Evento'])
            ->add('provider_id', TextColumn::class, ['field' => 'p.name', 'label' => 'Proveedor'])
            ->add('apply', TextColumn::class, ['field' => 'o.apply', 'label' => 'Aplica'])
            ->add('discount', TextColumn::class, ['field' => 'o.discount', 'label' => 'Descuento'])
            ->add('status', BoolColumn::class, [
                'field'      => 'o.status',
                'label'      => 'Estado',
                'trueValue'  => '<span class="badge badge-success">Visible</span>',
                'falseValue' => '<span class="badge badge-warning">No Visible</span>',
            ])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'o.id', 'render' => function ($offer_id) {

                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('offer_edit', ['id' => $offer_id]).'">Editar</a>&nbsp;<a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$offer_id.')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Offer::class,
                'query'  => function (QueryBuilder $builder) use ($actualDate) {
                    $builder->select('o')->from(Offer::class, 'o')
                        ->join('o.event', 'e')
                        ->join('o.provider', 'p')
                        ->where('e.final_date >= :actualDate')
                        ->andWhere('e.deleted_at IS NULL')
                        ->setParameter('actualDate', $actualDate)
                        ->orderBy('e.start_date', 'ASC')
                        ->orderBy('e.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/offer/index.html.twig', [
            'controller_name' => 'offerController',
            'datatable'       => $table,
            'message'         => $message
        ]);
    }

    /**
     * @Route("/admin/offer/new", name="offer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $offer = new Offer();
        $form  = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('offer_list');
        }

        return $this->render('admin/offer/new.html.twig', [
            'offers' => $offer,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/offer/providers", name="offer_provider", methods={"GET","POST"})
     */
    public function providers(Request $request): Response
    {
        $event_id   = $request->request->get('id');
        $actualDate = date('Y-m-d H:i:s');
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT p.id, p.nit, p.name FROM event e INNER JOIN event_provider ep ON (e.id = ep.event_id) INNER JOIN provider p ON (p.id = ep.provider_id) WHERE e.id = $event_id AND e.final_date >= '$actualDate' AND e.deleted_at IS NULL AND p.deleted_at IS NULL");
        $statement->execute();
        $providers = $statement->fetchAll();

        return $this->json($providers);
    }


    /**
     * @Route("/admin/offer/edit/{id}", name="offer_edit")
     */
    public function edit(Request $request, $id): Response
    {
        $actualDate    = date('Y-m-d H:i:s');
        $reg           = [];
        $reg['status'] = 1;

        if ($id > 0)
            $reg = $this->getDoctrine()->getRepository(Offer::class)->find($id);
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT p.id, p.nit, p.name FROM event e INNER JOIN event_provider ep ON (e.id = ep.event_id) INNER JOIN provider p ON (p.id = ep.provider_id) WHERE e.final_date >= '$actualDate' AND e.deleted_at IS NULL AND p.deleted_at IS NULL");
        $statement->execute();
        $providers   = $statement->fetchAll();
        $statement_e = $connection->prepare("SELECT id, title FROM event WHERE final_date >= '$actualDate' AND deleted_at IS NULL");
        $statement_e->execute();
        $events = $statement_e->fetchAll();

        $datatable = $this->createDataTable()
            ->add('event_id', TextColumn::class, ['field' => 'e.title', 'label' => 'Evento', 'render' => function ($offer_id) {
                return sprintf('<span>'.$offer_id.'</span>');
            }])
            ->add('provider_id', TextColumn::class, ['field' => 'p.name', 'label' => 'Proveedor', 'render' => function ($offer_id) {
                return sprintf('<span>'.$offer_id.'</span>');
            }])
            ->add('apply', TextColumn::class, ['field' => 'o.apply', 'label' => 'Aplica'])
            ->add('discount', TextColumn::class, ['field' => 'o.discount', 'label' => 'Descuento'])
            ->add('status', BoolColumn::class, [
                'field'      => 'o.status',
                'label'      => 'Estado',
                'trueValue'  => '<span class="badge badge-success">Visible</span>',
                'falseValue' => '<span class="badge badge-warning">No Visible</span>',
            ])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($offer_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('offer_edit', ['id' => $offer_id]).'">Editar</a><a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$offer_id.')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Offer::class,
                'query'  => function (QueryBuilder $builder) use ($actualDate) {
                    $builder->select('o')->from(Offer::class, 'o')
                        ->join('o.event', 'e')
                        ->join('o.provider_id', 'p')
                        ->where('e.final_date >= :actualDate')
                        ->andWhere('e.deleted_at IS NULL')
                        ->setParameter('actualDate', $actualDate)
                        ->orderBy('e.start_date', 'ASC')
                        ->orderBy('e.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($datatable->isCallback())
            return $datatable->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        $actualData = [];
        if ($id != 0)
        {
            $em         = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement  = $connection->prepare("SELECT p.id, p.name FROM provider p INNER JOIN offer o ON (p.id = o.provider_id) WHERE o.id = $id");
            $statement->execute();
            $providerid_temp = $statement->fetchAll();

            $event      = $this->getDoctrine()->getManager();
            $connection = $event->getConnection();
            $statement  = $connection->prepare("SELECT e.id, e.title FROM event e INNER JOIN offer o ON (e.id = o.event_id) WHERE o.id = $id");
            $statement->execute();
            $event_temp = $statement->fetchAll();

            $actualData['id']       = intval($providerid_temp[0]['id']) ? intval($providerid_temp[0]['id']) : 0;
            $actualData['name']     = $providerid_temp[0]['name'] ? $providerid_temp[0]['name'] : '';
            $actualData['event_id'] = $event_temp[0]['id'] ? $event_temp[0]['id'] : '';
            $actualData['title']    = $event_temp[0]['title'] ? $event_temp[0]['title'] : '';
            //$actualData['status'] = $event_temp[0]['status'] ? $event_temp[0]['status']  : '';
        } else
        {
            $actualData['id']       = 0;
            $actualData['name']     = '';
            $actualData['event_id'] = 0;
            $actualData['title']    = '';
        }

        return $this->render('admin/offer/edit.html.twig', compact('reg', 'datatable', 'message', 'providers', 'actualData', 'events'));
    }

    /**
     * @Route("/admin/offer/save/{id}", name="offer_save")
     */
    public function updateOrCreate(Request $request, MaelInterventionImageManager $imageManager, $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        if ($id > 0)
            $offer = $this->getDoctrine()->getRepository(Offer::class)->find($id);
        else
            $offer = new Offer();

        if ($request->files->get('file'))
        {
            $filename = date('Ymdhis').rand(0, 9);
            $img      = $imageManager->make($request->files->get('file'));
            $mimes    = [
                'image/jpeg' => 'jpg',
                'image/png'  => 'png',
                'image/gif'  => 'gif'
            ];

            $ext = $mimes[$img->mime];

            $img->save('upload/offer/'.$filename.'.'.$ext);

            $img->fit(400, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('upload/offer/b'.$filename.'.'.$ext);

            $offer->setFile_Url($filename.'.'.$ext);
        }

        $provider = $this->getDoctrine()->getRepository(Provider::class)->find(intval($dat['provider_id']));
        $event    = $this->getDoctrine()->getRepository(Event::class)->find(intval($dat['event_id']));


        $offer->setProviderId($provider);
        $offer->setApply($dat['apply']);
        $offer->setDiscount($dat['discount']);
        $offer->setEvent($event);
        $offer->setStatus($dat['status']);
        //$offer->setFileUrl($dat['file_url']);
        $offer->setCreatedById($this->getUser()->getId());
        $offer->setUpdatedById($this->getUser()->getId());

        $entityManager->persist($offer);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', 'La información de la oferta ha sido almacenada satisfactoriamente');

        return $this->redirectToRoute('offer_list');
    }


    /**
     * @Route("/admin/offer/delete/{id?}", name="offer_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $offer         = $this->getDoctrine()->getRepository(Offer::class)->find($id);
        //Registro quien borra y actualiza
        $offer->setUpdatedById($this->getUser()->getId());
        $offer->setDeletedById($this->getUser()->getId());
        $entityManager->persist($offer);
        $entityManager->flush();


        $entityManager->remove($offer);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/admin/offer/export_sample_offer", name="export_sample_offer")
     *
     */
    public function export_sample_offer(Request $request)
    {
        $spreadsheet = new Spreadsheet();
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Ofertas");
        $sheet->setCellValue('A1', 'ID Evento:');
        $sheet->setCellValue('B1', 'Nombre Evento:');
        $sheet->setCellValue('C1', 'Nit Proveedor:');
        $sheet->setCellValue('D1', 'Nombre Proveedor:');
        $sheet->setCellValue('E1', 'Aplica a:');
        $sheet->setCellValue('F1', 'Descuento:');

        $date_now   = date('Y-m-d H:i:s');
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT e.id, p.nit, e.title, p.name FROM event e INNER JOIN provider p WHERE e.deleted_at IS NULL AND e.final_date >= '$date_now' AND e.active = 1 AND e.deleted_at IS NULL AND p.deleted_at IS NULL");
        $statement->execute();
        $avalaible_events = $statement->fetchAll();
        $count            = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++)
        {
            $event_name = $avalaible_events[$count]["title"];
            $event_id   = $avalaible_events[$count]["id"];
            $prov_nit   = $avalaible_events[$count]["nit"];
            $prov_name  = $avalaible_events[$count]["name"];
            $sheet->getCell('A'.$i)->setValue($event_id);
            $sheet->getCell('B'.$i)->setValue($event_name);
            $sheet->getCell('C'.$i)->setValue($prov_nit);
            $sheet->getCell('D'.$i)->setValue($prov_name);
            $sheet->getCell('E'.$i)->setValue(' ');
            $sheet->getCell('F'.$i)->setValue(' ');
            $count++;
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'offers_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/offer/offerimport", name="offerimport")
     *
     */

    public function importExcel(Request $request)
    {
        if ($request->getMethod() == 'POST')
            if ($_FILES["file"]["size"] < 10000000) //5Mb
                return $this->xslx($request->files->get('file'));

        return $this->render('admin/offer/uploadExcel.html.twig');
    }

    /**
     * @Route("/admin/transferrs/offerexcel", name="offerexcel")
     *
     * @throws \Exception
     */
    public function xslx($inputFileName)
    {
        $spreadsheet   = IOFactory::load($inputFileName);
        $row           = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData     = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row)
        {
            if ($Row['A'] != NULL && $Row['C'] != NULL)
            {
                $prov_temp  = $this->loadProvider($Row['C']);
                $event_temp = $this->loadEvent(intval($Row['A']));
                if (!empty($prov_temp) && !empty($event_temp))
                {
                    $apply    = $Row['E'] != NULL ? $Row['E'] : '';
                    $discount = $Row['F'] != NULL ? $Row['F'] : '';

                    $provider = $this->getDoctrine()->getRepository(Provider::class)->find($prov_temp);
                    $event    = $this->getDoctrine()->getRepository(Event::class)->find($event_temp);
                    $offer    = new Offer();
                    $offer->setEvent($event);
                    $offer->setProviderId($provider);
                    $offer->setApply($apply);
                    $offer->setDiscount($discount);
                    //$offer->setFile_Url($image);
                    $offer->setStatus(1);
                    $entityManager->persist($offer);
                    $entityManager->flush();
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('offer_list');
    }

    public function loadProvider($prov_nit)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM provider WHERE nit LIKE '%$prov_nit%'");
        $statement->execute();
        $provider_temp = $statement->fetchAll();

        return $provider_temp[0]['id'];
    }

    public function loadEvent($event_id)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM event WHERE id = $event_id");
        $statement->execute();
        $event_temp = $statement->fetchAll();

        return $event_temp[0]['id'];
    }

    /**
     * @Route("asociado/ofertas", name="associate_offers")
     */
    public function showOffers()
    {
        $event_id   = $this->session->get('event_id');
        $event      = $this->getDoctrine()->getRepository(Event::class)->find($event_id);
        $banner     = $event->getBanner();
        $banners    = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);
        $associate  = $this->getUser();

        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT o.id, o.apply, o.discount, o.provider_id FROM offer o WHERE o.status = 1 and o.event_id = '$event_id' AND o.deleted_at IS NULL");

        $statement->execute();
        $offers       = $statement->fetchAll();
        $offer_result = [];
        $message      = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        foreach ($offers as $offer)
        {
            $event_provider  = $this->getDoctrine()->getRepository(EventProvider::class)->findBy(['event' =>$event_id, 'provider'=>$offer['provider_id']])[0];
            $provider = $this->getDoctrine()->getRepository(Provider::class)->find($offer['provider_id']);

            $offer['event_provider_id'] = $event_provider->getId();
            $offer['image']             = $provider->getImage();

            array_push($offer_result, $offer);
        }

        return $this->render('site/offers.html.twig', [
            'offers'    => $offer_result,
            'associate' => $associate,
            'message'   => $message,
            'banners'   => $banners,
        ]);
    }

    /**
     * @Route("admin/offer/deleted_offers", name="show_offers_deleted")
     */
    public function showOffersDeleted(Request $request): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $table = $this->createDataTable()
            ->add('event_id', TextColumn::class, ['field' => 'e.title', 'label' => 'Evento'])
            ->add('provider_id', TextColumn::class, ['field' => 'p.name', 'label' => 'Proveedor'])
            ->add('apply', TextColumn::class, ['field' => 'o.apply', 'label' => 'Aplica'])
            ->add('discount', TextColumn::class, ['field' => 'o.discount', 'label' => 'Descuento'])
            ->add('status', BoolColumn::class, [
                'field'      => 'o.status',
                'label'      => 'Estado',
                'trueValue'  => '<span class="badge badge-success">Activa</span>',
                'falseValue' => '<span class="badge badge-warning">Inactiva</span>',
            ])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'o.id', 'render' => function ($offer_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="'.$this->generateUrl('restore_offers', ['id' => $offer_id]).'">Restaurar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Offer::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('o')->from(Offer::class, 'o')
                        ->join('o.event', 'e')
                        ->join('o.provider', 'p')
                        ->where('o.deleted_at IS NOT NULL')
                        ->orderBy('e.start_date', 'ASC')
                        ->orderBy('e.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/offer/deleted.html.twig', [
            'controller_name' => 'offerController',
            'datatable'       => $table,
            'message'         => $message
        ]);
        $filters->enable('soft_deleteable');
    }

    /**
     * @Route("admin/offer/restore_offers/{id?}", name="restore_offers")
     */
    public function restoreOffersDeleted($id): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $entityManager = $this->getDoctrine()->getManager();
        $offer         = $this->getDoctrine()->getRepository(Offer::class)->find($id);

        $offer->setDeletedAt(NULL);
        $offer->setUpdatedById($this->getUser()->getId());
        $offer->setDeletedById(NULL);

        $entityManager->persist($offer);
        $entityManager->flush();


        $entityManager->flush();
        $filters->enable('soft_deleteable');
        $this->session->getFlashBag()->add('success', 'Oferta activada satisfactoriamente');

        return $this->redirectToRoute('show_offers_deleted');
        //return new Response(Response::HTTP_ACCEPTED);
    }
}
