<?php

namespace App\Controller;

use App\Entity\BannerImage;
use App\Entity\Premiaton;
use App\Entity\Provider;
use App\Entity\Winner;
use App\Entity\Associated;
use App\Entity\Event;
use App\Form\PremiatonType;
use App\Repository\PremiatonRepository;

use Doctrine\ORM\QueryBuilder;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class PremiatonController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("/premiaton", name="premiaton_index")
     */
    public function index(Request $request)
    {
        // dd($_SERVER);
        $date_now   = date('Y-m-d H:i:s');
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM event e WHERE e.deleted_at IS NULL AND final_date >= '$date_now'");

        $statement->execute();
        $events = $statement->fetchAll();

        $table = $this->createDataTable()
            ->add('event', TextColumn::class, ['field' => 'e.title', 'label' => 'Evento', 'render' => function ($premiation_id) {
                return sprintf('<span>'.$premiation_id.'</span>');
            }])
            ->add('start_date', DateTimeColumn::class, ['field' => 'pr.created_at', 'label' =>' Fecha', 'searchable' => false,  'render' => function ($date) {
                return sprintf(date('g i a - j F Y', strtotime($date)));
            }])
            ->add('logo', TextColumn::class, ['field' => 'pr.logo', 'label' =>'Imagen', 'searchable' => false, 'render' => function ($image) {
                return sprintf('<a class="popup-gallery" title="" href="upload/premiaton/'.$image.'"/>Ver Imagen</a>');
            }])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-20', 'field' => 'pr.id', 'render' => function ($premiation_id) {
                return sprintf('<a href="#" class="btn btn-sm btn-danger" onclick="delete_row('.$premiation_id.')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Premiaton::class,
                'query'  => function (QueryBuilder $builder) use ($date_now) {
                    $builder->select('pr')
                        ->from(Premiaton::class, 'pr')
                        ->join('pr.event_id', 'e')
                        ->where('e.final_date >= :date_now')
                        ->andWhere('e.deleted_at IS NULL')
                        ->setParameter('date_now', $date_now)
                        ->orderBy('pr.created_at', 'DESC')
                        ->orderBy('e.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/premiaton/create.html.twig', [
            'controller_name' => 'PremiatonController',
            'datatable'       => $table,
            'events'          => $events,
            'message'         => $message
        ]);
    }

    // /**
    //  * @Route("/premiaton", name="premiaton_index")
    //  */
    // public function index(Request $request)
    // {
    //     $date_now = date('Y-m-d H:i:s');
    //     $table = $this->createDataTable()
    //         ->add('event', TextColumn::class, ['field' => 'e.title', 'label' => 'Evento', 'render' => function ($premiation_id) {
    //             return sprintf('<span>'.$premiation_id.'</span>');
    //         }])
    //         ->add('start_date', DateTimeColumn::class, ['field' => 'pr.start_date', 'label' => ' Fecha Inicio', 'render' => function ($date) {
    //             return sprintf(date('g i a - j F Y', strtotime($date)));
    //         }])
    //         ->add('logo', TextColumn::class, ['field' => 'pr.logo', 'label' => 'Logo'])
    //         ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'pr.id', 'render' => function ($premiation_id) {
    //             return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('premiaton_edit', ['id' => $premiation_id]) . '">Editar</a>
    //                             <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $premiation_id . ')">Inactivar</a>');
    //         }])
    //         ->createAdapter(ORMAdapter::class, [
    //             'entity' => Premiaton::class,
    //             'query'  => function (QueryBuilder $builder) use ($date_now) {
    //                 $builder->select('pr')
    //                     ->from(Premiaton::class, 'pr')
    //                     ->join('pr.event_id', 'e')
    //                     ->where('e.final_date >= :date_now')
    //                     ->andWhere('e.deleted_at IS NULL')
    //                     ->setParameter('date_now', $date_now)
    //                     ->orderBy('e.start_date', 'ASC')
    //                     ->orderBy('e.id', 'ASC');
    //             }
    //         ])
    //         ->handleRequest($request);

    //     if ($table->isCallback())
    //         return $table->getResponse();

    //     /*Show flash message*/
    //     $message = '';

    //     foreach ($this->session->getFlashBag()->get('success', []) as $msg)
    //         $message .= $msg;

    //     return $this->render('admin/premiaton/index.html.twig', [
    //         'controller_name' => 'PremiatonController',
    //         'datatable'       => $table,
    //         'message'         => $message
    //     ]);
    // }


    // /**
    //  * @Route("/premiaton/edit/{id}", name="premiaton_edit")
    //  */
    // public function edit(Request $request, $id): Response
    // {
    //     $reg = [];
    //     $date_now = date('Y-m-d H:i:s');
    //     $em = $this->getDoctrine()->getManager();
    //     $connection = $em->getConnection();
    //     $statement = $connection->prepare("SELECT * FROM event e WHERE e.deleted_at IS NULL AND final_date >= '$date_now'");
    //     $statement->execute();
    //     $events = $statement->fetchAll();

    //     if ($id > 0) {
    //         $connection = $em->getConnection();
    //         $statement = $connection->prepare("SELECT pr.id, e.title, pr.logo, pr.start_date FROM premiaton pr INNER JOIN event e ON(pr.event_id_id = e.id) WHERE pr.id = $id");
    //         $statement->execute();
    //         $reg = $statement->fetchAll();

    //     } else {
    //         //$reg['message'] = $this->session->get('message');
    //     }

    //     $datatable = $this->createDataTable()
    //         ->add('code', TextColumn::class, ['field' => 'a.code', 'label' => 'Código Asociado', 'render' => function ($winner_id) {
    //             return sprintf('<span>' . $winner_id . '</span>');
    //         }])
    //         ->add('provider', TextColumn::class, ['field' => 'p.name', 'label' => 'Nombre Proveedor', 'render' => function ($winner_id) {
    //              return sprintf('<span>' . $winner_id . '</span>');
    //         }])
    //         ->add('start_date', DateTimeColumn::class, ['field' => 'pr.start_date', 'label' => 'Fecha de Inicio', 'render' => function ($date) {
    //             return sprintf(date('g i a - j F Y', strtotime($date)));
    //         }])
    //         ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'pr.id', 'render' => function ($winner_id) {
    //             return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('winner_edit', ['id' => $winner_id]) . '">Editar</a>
    //                                 <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $winner_id . ')">Inactivar</a>');
    //         }])
    //         ->createAdapter(ORMAdapter::class, [
    //             'entity' => Winner::class,
    //             'query'  => function (QueryBuilder $builder) use ($id) {
    //                 $builder->select('w')
    //                     ->from(Winner::class, 'w')
    //                     ->join('w.provider_id', 'p')
    //                     ->join('w.associated_id', 'a')
    //                     ->join('w.premiaton_id', 'pr')
    //                     ->join('pr.event_id', 'e')
    //                     ->where('w.premiaton_id = :id')
    //                     ->setParameter('id', $id);
    //             }
    //         ])
    //         ->handleRequest($request);
    //     if ($datatable->isCallback())
    //         return $datatable->getResponse();

    //     /*Show flash message*/
    //     $message = '';

    //     foreach ($this->session->getFlashBag()->get('success', []) as $msg)
    //         $message .= $msg;
    //     /*Show flash message*/
    //     $message = '';

    //     foreach ($this->session->getFlashBag()->get('success', []) as $msg)
    //         $message .= $msg;

    //     return $this->render('admin/premiaton/edit.html.twig', compact('reg', 'datatable', 'message', 'events'));
    // }


    /**
     * @Route("/admin/premiaton/save/{id}", name="premiaton_save")
     */
    public function updateOrCreate(Request $request, MaelInterventionImageManager $imageManager, $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        if ($id > 0)
        {
            $connection = $entityManager->getConnection();
            $statement  = $connection->prepare("SELECT pr.id, e.title, pr.logo, pr.start_date FROM premiaton pr INNER JOIN event e ON(pr.event_id_id = e.id) WHERE pr.id = $id");
            $statement->execute();
            $premiaton = $statement->fetchAll();
        } else
        {

            $premiaton = new Premiaton();
        }

        if ($request->files->get('file'))
        {
            $filename = date('Ymdhis').rand(0, 9);
            $img      = $imageManager->make($request->files->get('file'));
            $mimes    = [
                'image/jpeg' => 'jpg',
                'image/png'  => 'png',
                'image/gif'  => 'gif'
            ];

            $ext = $mimes[$img->mime];

            $img->save('upload/premiaton/'.$filename.'.'.$ext);

            $premiaton->setLogo($filename.'.'.$ext);
        }

        $event = $this->getDoctrine()->getRepository(Event::class)->find(intval($dat['event_id']));
        $premiaton->setEventId($event);
        // $premiaton->setStartDate(new \Datetime($dat['start_date']));
        $entityManager->persist($premiaton);
        $entityManager->flush();

        $this->session->getFlashBag()->add('success', 'La información ha sido almacenada satisfactoriamente');

        // return $this->redirectToRoute('premiaton_edit', [
        //     'id' => $premiaton->getId()
        // ]);

        return $this->redirectToRoute('premiaton_index');
    }

    // /**
    //  * @Route("/premiaton/export_sample", name="export_sample")
    //  *
    //  */
    // public function export_sample(Request $request)
    // {
    //     $spreadsheet = new Spreadsheet();

    //     /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->setTitle("Formato Carga Premiaton");
    //     $sheet->setCellValue('A1', 'Eventos Disponibles');
    //     $sheet->setCellValue('B1', 'Imagen Premiaton');
    //     $sheet->setCellValue('C1', 'Fecha Inicio');

    //     $date_now = date('Y-m-d H:i:s');
    //     $em = $this->getDoctrine()->getManager();
    //     $connection = $em->getConnection();
    //     $statement = $connection->prepare("SELECT id, title FROM event WHERE deleted_at IS NULL AND final_date >= '$date_now' GROUP BY id");
    //     $statement->execute();
    //     $avalaible_events = $statement->fetchAll();
    //     $date = new \Datetime();
    //     $count = 0;
    //     for ($i = 2; $i < count($avalaible_events) + 2; $i++) {
    //         $value = $avalaible_events[$count]["title"];
    //         $sheet->getCell('A' . $i)->setValue($value);
    //         $sheet->getCell('B' . $i)->setValue($value.'.jpg');
    //         $sheet->getCell('C' . $i)->setValue($date);
    //         $count++;
    //     }
    //     $writer = new Xlsx($spreadsheet);
    //     $fileName = 'premiaton_sample.xlsx';
    //     $temp_file = tempnam(sys_get_temp_dir(), $fileName);
    //     $writer->save($temp_file);
    //     return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    // }

    // /**
    //  * @Route("/premiaton/import", name="premiaton_import")
    //  *
    //  */
    // public function import(Request $request)
    // {
    //     if ($request->getMethod() == 'POST') {
    //         if ($_FILES["file"]["size"] < 10000000) //5Mb
    //         {
    //             if ($_FILES["file"]["error"] > 0) {
    //                 //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    //             } else {
    //                 if (empty($_POST) && empty($_FILES) && isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    //                     //echo "The file is bigger than post_max_size in php.ini.";
    //                 } else {
    //                     if (file_exists("uploads/" . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"])) {
    //                         // echo $_FILES["file"]["name"] . " already exists. ";
    //                     } else {
    //                         move_uploaded_file(
    //                             $_FILES["file"]["tmp_name"],
    //                             "uploads/" . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"]
    //                         );
    //                         //echo "Stored in: " . "uploads/" . $_FILES["file"]["name"];
    //                         $fileUpload = './uploads/' . date('m-d-Y_hia') . '_' . $_FILES["file"]["name"];

    //                         return $this->xslx($fileUpload);
    //                     }
    //                 }
    //             }
    //         } else {
    //             /*echo "Archivo invalido";*/
    //         }
    //     }

    //     return $this->render('admin/premiaton/import.html.twig');
    // }

    // /**
    //  * @Route("/providers/upload-excel", name="xlsx")
    //  *
    //  * @throws \Exception
    //  */
    // public function xslx($inputFileName)
    // {
    //     $spreadsheet   = IOFactory::load($inputFileName);
    //     $row           = $spreadsheet->getActiveSheet()->removeRow(1);
    //     $sheetData     = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
    //     $entityManager = $this->getDoctrine()->getManager();

    //     foreach ($sheetData as $Row) {
    //         if ($Row['A'] != null && $Row['B'] != null && $Row['C'] != null) {
    //             $event_temp = $this->loadEvent($Row['A']);
    //             if (!empty($event_temp)) {
    //                 $event = $this->getDoctrine()->getRepository(Event::class)->find($event_temp);
    //                 $logo         = $Row['B'];
    //                 $start_date   = $Row['C'];

    //                 $premiaton = new Premiaton();
    //                 $premiaton->setEventId($event);
    //                 $premiaton->setLogo($logo);
    //                 $premiaton->setStartDate(new \Datetime($start_date));
    //                 $entityManager->persist($premiaton);
    //                 $entityManager->flush();
    //             }
    //         }
    //     }

    //     $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

    //     return $this->redirectToRoute('premiaton_index');
    // }

    // public function loadEvent($prov_name)
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     $connection = $em->getConnection();
    //     $statement = $connection->prepare("SELECT * FROM event WHERE title LIKE '%$prov_name%'");
    //     $statement->execute();
    //     $provider_temp = $statement->fetchAll();
    //     return $provider_temp[0]['id'];
    // }


    /**
     * @Route("/premiaton/delete/{id?}", name="premiaton_delete")
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $premiaton     = $this->getDoctrine()->getRepository(Premiaton::class)->find($id);
        //Registro quien borra y actualiza
        $premiaton->setUpdatedById($this->getUser()->getId());
        $premiaton->setDeletedById($this->getUser()->getId());
        $entityManager->persist($premiaton);
        $entityManager->flush();

        $entityManager->remove($premiaton);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("asociado/premiaton", name="premiaton_incentive")
     */
    public function showPremiaton()
    {
        $event_id    = $this->session->get('event_id');
        $event       = $this->getDoctrine()->getRepository(Event::class)->find($event_id);
        $content_url = $event->getContents()[0]->getFileUrl();
        $banner      = $event->getBanner();
        $banners     = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);
        $date_now    = date('Y-m-d H:i:s');
        $em          = $this->getDoctrine()->getManager();
        $connection  = $em->getConnection();
        $statement   = $connection->prepare("SELECT pr.logo, pr.created_at FROM premiaton pr INNER JOIN event e ON (pr.event_id_id = e.id) WHERE e.id = $event_id AND e.active = 1 AND e.deleted_at IS NULL AND e.final_date >= '$date_now' ORDER BY pr.created_at DESC LIMIT 1");
        $statement->execute();
        $premiaton = $statement->fetchAll();

        return $this->render('site/premiaton_img.html.twig', [
            'controller_name' => 'PremiatonController',
            'banners'         => $banners,
            'premiaton'       => $premiaton,
            'content_url'     => $content_url,
        ]);
    }
}
