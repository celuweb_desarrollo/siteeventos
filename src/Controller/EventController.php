<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\BannerImage;
use App\Entity\Content;
use App\Entity\Event;
use App\Entity\EventProvider;
use App\Entity\IncentiveProvider;
use App\Entity\Premiaton;
use App\Entity\Provider;
use App\Entity\UserContent;
use App\Entity\UserEvent;
use App\Repository\EventRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\DataTable;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\BoolColumn;
use Omines\DataTablesBundle\DataTableFactory;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class EventController extends AbstractController
{
    private $router;
    private $session;
    private $factory;

    public function __construct(RouterInterface $router, DataTableFactory $factory)
    {
        $this->router  = $router;
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
        $this->factory = $factory;
    }

    /**
     * @Route("/admin/event", name="event_index",methods={"GET","POST"})
     */
    public function index(Request $request): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->enable('soft_deleteable');
        $table = $this->createDataTable()
            ->add('id', TextColumn::class, ['field' => 'e.id', 'label' => '#', 'className' => 'w-20 text-center'])
            ->add('name', TextColumn::class, ['field' => 'e.title', 'label' => 'Nombre evento'])
            ->add('start_date', DateTimeColumn::class, ['field' => 'e.start_date', 'label' => 'Fecha inicio','searchable' => false, 'render' => function ($date) {
                return sprintf(date('F j, Y', strtotime($date)));
            }])
            ->add('final_date', DateTimeColumn::class, ['field' => 'e.final_date', 'label' => 'Fecha finalización','searchable' => false,'render' => function ($date) {
                return sprintf(date('F j, Y', strtotime($date)));
            }])
            ->add('provider_goal', TextColumn::class, ['field' => 'e.percent', 'label' => 'Meta Proveedores', 'className' => 'text-center'])
            ->add('description', TextColumn::class, ['field' => 'e.description', 'label' => 'Descripción'])
            ->add('status', BoolColumn::class, [
                'field'      => 'e.active',
                'label'      => 'Estado',
                'trueValue'  => '<span class="badge badge-success">Visible</span>',
                'falseValue' => '<span class="badge badge-warning">No Visible</span>',
            ])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'e.id', 'render' => function ($event_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('event_edit', ['id' => $event_id]) . '">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $event_id . ')">Inactivar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Event::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('e')->from(Event::class, 'e');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/event/index.html.twig', [
            'datatable' => $table,
            'message'   => $message
        ]);
    }

    /**
     * @Route("/admin/event/edit/{id?}", name="event_edit")
     */
    public function edit(Request $request, $id): Response
    {
        $reg                = [];
        $images             = [];
        $content            = [];
        $selected_providers = [];

        if ($id > 0) {
            $reg       = $this->getDoctrine()->getRepository(Event::class)->find($id);
            $images    = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => $reg->getBanner()]);
            $content   = $this->getDoctrine()->getRepository(Content::class)->findOneBy(['event' => $reg->getId()]);
            $evt_provs = $this->getDoctrine()->getRepository(EventProvider::class)->findAll(['event' => $reg]);

            foreach ($evt_provs as $evt_prov)
                array_push($selected_providers, $evt_prov->getProvider()->getId());
        }

        $datatable = $this->createDataTable()
            ->add('name', TextColumn::class, ['field' => 'p.name', 'label' => 'Proveedor'])
            ->add('zoom_code', TextColumn::class, ['field' => 'ep.zoom_code', 'label' => 'ID reunión Zoom'])
            ->add('zoom_number', TextColumn::class, ['field' => 'ep.zoom_number', 'label' => 'Número Zoom'])
            ->add('region', TextColumn::class, ['field' => 'ep.region', 'label' => 'Región'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'p.id', 'render' => function ($evt_provider_id) {
                return sprintf('<a href="#"  class="btn btn-sm btn-success" onclick="edit_provider(' . $evt_provider_id . ')">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger" onclick="delete_row(' . $evt_provider_id . ')">Inactivar</a>');
            }])
            ->addOrderBy(0, DataTable::SORT_ASCENDING)
            ->createAdapter(ORMAdapter::class, [
                'hydrate' => Query::HYDRATE_ARRAY,
                'entity'  => EventProvider::class,
                'query'   => function (QueryBuilder $builder) use ($id) {
                    $builder->select('ep.id, ep.zoom_code, ep.zoom_number, p.name','ep.region')
                        ->from(EventProvider::class, 'ep')
                        ->innerJoin(Provider::class, 'p', 'WITH', 'p.id = ep.provider')
                        ->where('ep.event = :event_id')
                        ->setParameter('event_id', $id);
                }
            ])
            ->handleRequest($request);

        if ($datatable->isCallback())
            return $datatable->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        $providers = $this->getDoctrine()->getRepository(Provider::class)->findAll();

        return $this->render('admin/event/edit.html.twig', compact('reg', 'message', 'providers', 'images', 'content', 'selected_providers', 'datatable'));
    }

    /**
     * @Route("/admin/event/save/{id}", name="event_save")
     */
    public function updateOrCreate(Request $request, $id)
    {
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();
        $providers     = $request->request->get('providers');

        if ($id > 0) {
            $event   = $this->getDoctrine()->getRepository(Event::class)->find($id);
            $content = $this->getDoctrine()->getRepository(Content::class)->findOneBy(['event' => $event]);
            $content = $content ? $content : new Content();
        } else {
            $event   = new Event();
            $content = new Content();
        }

        $event->setTitle($dat['title']);
        $event->setPercent($dat['percent']);
        $event->setDescription($dat['description']);
        $event->setActive($dat['active']);
        $event->setCreatedById($this->getUser()->getId());
        $event->setUpdatedById($this->getUser()->getId());

        if ($dat['banner_id']) {
            $banner = $this->getDoctrine()->getRepository(Banner::class)->findOneBy(['id' => $dat['banner_id']]);
            $event->setBanner($banner);
        }

        $event->setStartDate(new \DateTime($dat['start_date'] . 'T00:00:00'));
        $event->setFinalDate(new \DateTime($dat['final_date'] . 'T23:59:59'));

        $entityManager->persist($event);
        $entityManager->flush();

        if ($request->files->get('file')) {
            /** @var UploadedFile $file */
            $file     = $request->files->get('file');
            $filename = date('Ymdhis') . rand(0, 9) . '.pdf';
            $dest     = 'upload/terms';

            $file->move($dest, $filename);

            $content->setFileUrl($filename);
        }

        if (isset($dat['terms_content']) and $dat['terms_content'] != '')
            $content->setDescription($dat['terms_content']);

        $content->setTitle($dat['title']);
        $content->setEvent($event);
        $entityManager->persist($content);
        $entityManager->flush();

        if (!empty($providers))
            foreach ($providers as $prov_id) {
                $provider = $this->getDoctrine()->getRepository(Provider::class)->find($prov_id);

                $evt_prv = new EventProvider();
                $evt_prv->setProvider($provider);
                $evt_prv->setEvent($event);

                $entityManager->persist($evt_prv);
                $entityManager->flush();
            }

        $this->session->getFlashBag()->add('success', 'La información del evento ha sido almacenada satisfactoriamente');

        return $this->redirectToRoute('event_edit', [
            'id' => $event->getId()
        ]);
    }


    /**
     * @Route("/admin/event/event_delete/{id?}", name="event_delete", methods={"DELETE"})
     */
    public function delete($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event         = $this->getDoctrine()->getRepository(Event::class)->find($id);
        //Registro quien borra y actualiza
        $event->setUpdatedById($this->getUser()->getId());
        $event->setDeletedById($this->getUser()->getId());
        $entityManager->persist($event);
        $entityManager->flush();

        $entityManager->remove($event);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }

    /**
     * @Route("asociado/eventos", name="associate_events")
     */
    public function showeEvents(Request $request)
    {
        $request->getSession()->set('event_id', NULL);
        $contImg    = 0;
        $events     = $this->getDoctrine()->getRepository(Event::class)->allEventsActive();

        $allBanners = null;

        foreach ($events as $key => $event) {
            $banner = $event->getBanner();
            if (isset($banner)) {
                $banners = $banner->getBannerImages();
                if (isset($banners)) {
                    $eventId = $event->getId();
                    foreach ($banners as $keyBanner => $banner) {
                        $allBanners[$contImg]['link']          = 'javascript:checkTermsAndConditions(' . $eventId . ')';
                        $allBanners[$contImg]['filename']      = $banner->getFilename();
                        $allBanners[$contImg++]['description'] = $banner->getDescription();
                    }
                }
            }
        }

        $path = $this->getParameter('kernel.project_dir') . '/public/upload/terms';

        return $this->render('site/event.html.twig', compact('allBanners', 'path'));
    }

    /**
     * @param EventRepository $eventRepository
     */
    public function getEvents(EventRepository $eventRepository)
    {
        $events = $eventRepository->all();
        $colors = ['bg-primary' => 'bg-primary', 'bg-success' => 'bg-success', 'bg-warning' => 'bg-warning', 'bg-danger' => 'bg-danger', 'bg-info' => 'bg-info'];
        $array  = [];

        foreach ($events as $out) {

            $dateStart = $out['start_date'];
            $dateFinal = $out['final_date'];
            $dateNow   = new \DateTime('now');

            if ($dateFinal->format('Y-m-d') < $dateNow->format('Y-m-d')) {
                continue;
            }

            $array[] = [
                'id'         => $out['id'],
                'title'      => ucwords($out['title']),
                'start'      => $dateStart->format('Y-m-d') . 'T00:00:00',
                'end'        => $dateFinal->format('Y-m-d') . 'T23:59:59',
                'allDay'     => FALSE,
                'classNames' => [array_rand($colors, 1), 'font-weight-bold', 'text-white', ' btn-outline-secondary'],
            ];
        }

        return $this->json($array);
    }

    /**
     * @param Request $request
     * @param $event_id
     */
    public function eventMenu()
    {
        $event_id    = $this->session->get('event_id');
        $event       = $this->getDoctrine()->getRepository(Event::class)->find($event_id);
        $content_url = $event->getContents()[0]->getFileUrl();
        $banner      = $event->getBanner();
        $banners     = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => (isset($banner) ? $banner->getId() : '')]);

        //Call Premiaton & Incentive Ligthbox
        $this->getDoctrine()->getRepository(Premiaton::class)->validate_banner();
        $this->getDoctrine()->getRepository(IncentiveProvider::class)->validate_banner();

        return $this->render('site/grid.html.twig', compact('banners', 'content_url'));
    }

    /**
     * Almacena el id de un evento en Sessión
     * @param Request $request
     * @param $event_id
     */
    public function getEventData(Request $request)
    {

        $event_id = $request->get('event_id'); //Evento el cual fue seleccionado pero aún no esta en el storage
        setlocale(LC_TIME, 'es_ES.UTF-8');

        $event               = $this->getDoctrine()->getRepository(Event::class)->findOneBy(['id' => $event_id]);
        $user                = $this->getUser();
        $userContent         = $this->getDoctrine()->getRepository(UserContent::class)->findByEventAndUser($event_id, $user->getId());
        $data['status']      = (!empty($userContent) ? FALSE : TRUE);
        $start_date          = date("d-m-Y", strtotime($event->getStartDate()->format('Y-m-d')));
        $final_date          = date("d-m-Y", strtotime($event->getFinalDate()->format('Y-m-d')));
        $data['id']          = $event->getId();
        $data['title']       = mb_strtoupper($event->getTitle());
        $data['start_date']  = strftime("%d de %B de %Y", strtotime($start_date));
        $data['final_date']  = strftime("%d de %B de %Y", strtotime($final_date));
        $data['percent']     = $event->getPercent();
        $content             = $event->getContents()[0];
        $data['fileurl']     = $content->getFileUrl();
        $data['description'] = $content->getDescription();

        return $this->json($data);
    }

    /**
     * Almacena el usuario y la fecha en que acepto los término y condiciones.
     * @param Request $request
     */
    public function saveTerms(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event         = $this->getDoctrine()->getRepository(Event::class)->find($request->get('event_id'));

        $userContent = new UserContent();
        $userContent->setUser($this->getUser());
        $userContent->setContent($event->getContents()[0]);
        $userContent->setEvent($event);
        $userContent->setCreatetAt(new \DateTime('now'));
        $entityManager->persist($userContent);
        $entityManager->flush();

        return $this->json($event->getId() ?? 0);
    }

    /**
     * Permite almacenar el log de usuarios y eventos
     * @param Request $request
     */
    public function logUserEvent(Request $request)
    {
        $user     = $this->getUser();
        $event_id = $request->get('event_id');
        $request->getSession()->set('event_id', $event_id);
        $event         = $this->getDoctrine()->getRepository(Event::class)->find($event_id);
        $userEvent     = $this->getDoctrine()->getRepository(UserEvent::class)->findByUserAndEvent($event_id, $user->getId());
        $entityManager = $this->getDoctrine()->getManager();
        $counter       = 1;

        if (empty($userEvent)) {
            $userEvent = new UserEvent();
            $userEvent->setUser($user);
            $userEvent->setEvent($event);
        } else {
            $userEvent = $this->getDoctrine()->getRepository(UserEvent::class)->findOneBy(['id' => $userEvent['id']]);
            $counter   = (int)$userEvent->getCounter() + $counter;
        }

        $userEvent->setCounter($counter);
        $entityManager->persist($userEvent);
        $entityManager->flush();

        return $this->json($userEvent->getId() ?? 0);
    }


    /**
     * @Route("/admin/event/export_provGen_ev_sample", name="export_provGen_ev_sample")
     *
     */
    public function export_provGen_ev_sample(Request $request)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Proveedores Eventos ");
        $sheet->setCellValue('A1', 'ID Evento');
        $sheet->setCellValue('B1', 'Nombre Evento');
        $sheet->setCellValue('C1', 'Nit Proveedor');
        $sheet->setCellValue('D1', 'Nombre Proveedor');
        $sheet->setCellValue('E1', 'ID Reunión Zoom');
        $sheet->setCellValue('F1', 'Código Reunión');

        $date_now   = date('Y-m-d H:i:s');
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT e.id, p.nit, e.title, p.name FROM event e INNER JOIN provider p WHERE e.deleted_at IS NULL AND e.final_date >= '$date_now' AND e.deleted_At IS NULL AND p.deleted_At IS NULL");
        $statement->execute();
        $avalaible_events = $statement->fetchAll();
        $count            = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++) {
            $event_name = $avalaible_events[$count]["title"];
            $event_id   = $avalaible_events[$count]["id"];
            $prov_nit   = $avalaible_events[$count]["nit"];
            $prov_name  = $avalaible_events[$count]["name"];
            $sheet->getCell('A' . $i)->setValue($event_id);
            $sheet->getCell('B' . $i)->setValue($event_name);
            $sheet->getCell('C' . $i)->setValue($prov_nit);
            $sheet->getCell('D' . $i)->setValue($prov_name);
            $sheet->getCell('E' . $i)->setValue(' ');
            $sheet->getCell('F' . $i)->setValue(' ');
            $count++;
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'prov_event_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/admin/event/export_prov_ev_sample/{event}", name="export_prov_ev_sample")
     *
     */
    public function export_prov_ev_sample(Request $request, $event)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Formato Proveedores Eventos ");
        $sheet->setCellValue('A1', 'ID Evento');
        $sheet->setCellValue('B1', 'Nombre Evento');
        $sheet->setCellValue('C1', 'Nit Proveedor');
        $sheet->setCellValue('D1', 'Nombre Proveedor');
        $sheet->setCellValue('E1', 'ID Reunión Zoom');
        $sheet->setCellValue('F1', 'Código Reunión');

        $date_now   = date('Y-m-d H:i:s');
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT e.id, p.nit, e.title, p.name FROM event e INNER JOIN provider p WHERE e.deleted_at IS NULL AND e.id = $event AND e.final_date >= '$date_now' AND e.deleted_At IS NULL AND p.deleted_At IS NULL");
        $statement->execute();
        $avalaible_events = $statement->fetchAll();
        $count            = 0;

        for ($i = 2; $i < count($avalaible_events) + 2; $i++) {
            $event_name = $avalaible_events[$count]["title"];
            $event_id   = $avalaible_events[$count]["id"];
            $prov_nit   = $avalaible_events[$count]["nit"];
            $prov_name  = $avalaible_events[$count]["name"];
            $sheet->getCell('A' . $i)->setValue($event_id);
            $sheet->getCell('B' . $i)->setValue($event_name);
            $sheet->getCell('C' . $i)->setValue($prov_nit);
            $sheet->getCell('D' . $i)->setValue($prov_name);
            $sheet->getCell('E' . $i)->setValue(' ');
            $sheet->getCell('F' . $i)->setValue(' ');
            $count++;
        }

        $writer    = new Xlsx($spreadsheet);
        $fileName  = 'prov_event_sample.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     *
     * @param Request $request
     * @Route("/admin/event/event_provider_import", name="event_provider_import")
     */
    public function event_provider_import(Request $request, $event_id)
    {
        $event = $this->getDoctrine()->getRepository(Event::class)->find($event_id);

        if ($request->getMethod() == 'POST')
            if ($_FILES["file"]["size"] < 10000000) //5Mb
                return $this->xslx($request->files->get('file'), $event_id);

        return $this->render('admin/event_provider/import.html.twig', compact('event'));
    }


    /**
     *
     * @param Request $request
     * @Route("/admin/event/event_providerGen_import", name="event_providerGen_import")
     */
    public function event_providerGen_import(Request $request)
    {
        if ($request->getMethod() == 'POST')
            if ($_FILES["file"]["size"] < 10000000) //5Mb
                return $this->xslxGen($request->files->get('file'));

        return $this->render('admin/event_provider/importGen.html.twig');
    }

    /**
     * @Route("/event/upload-excel", name="transferXlsx")
     *
     * @throws \Exception
     */
    public function xslxGen($inputFileName)
    {

        $spreadsheet = IOFactory::load($inputFileName);
        $row         = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData   = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row) {
            if ($Row['A'] != null && $Row['C'] != null) {
                $prov_temp  = $this->loadProvider(strval($Row['C']));
                $event_temp = $this->loadEvent(intval($Row['A']));

                if (!empty($prov_temp) && !empty($event_temp)) {
                    $zoom_code = $Row['E'] != null ? $Row['E'] : '';
                    $zoom_number = $Row['F'] != null ? $Row['F'] : '';

                    $provider = $this->getDoctrine()->getRepository(Provider::class)->find($prov_temp);
                    $event = $this->getDoctrine()->getRepository(Event::class)->find($event_temp);
                    // $event_existant = $entityManager->getRepository(EventProvider::class)->findOneBy(array('event' => $event, 'provider' => $provider));
                    $transfers      = new EventProvider();
                    // if (!$event_existant)
                    // {
                    $transfers->setZoomNumber($zoom_number);
                    $transfers->setZoomCode($zoom_code);
                    $transfers->setEvent($event);
                    $transfers->setProvider($provider);
                    $entityManager->persist($transfers);
                    $entityManager->flush();
                    // }
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('event_index');
    }

    /**
     * @Route("/event/upload-excel", name="transferXlsx")
     *
     * @throws \Exception
     */
    public function xslx($inputFileName, $ev_id)
    {
        $spreadsheet   = IOFactory::load($inputFileName);
        $row           = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData     = $spreadsheet->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($sheetData as $Row) {
            if ($Row['A'] != null && $Row['C'] != null) {
                $prov_temp  = $this->loadProvider(strval($Row['C']));
                $event_temp = $this->loadEvent(intval($Row['A']));
                if (!empty($prov_temp) && !empty($event_temp)) {
                    $zoom_code = $Row['E'] != null ? $Row['E'] : '';
                    $zoom_number = $Row['F'] != null ? $Row['F'] : '';

                    $provider  = $this->getDoctrine()->getRepository(Provider::class)->find($prov_temp);
                    $event     = $this->getDoctrine()->getRepository(Event::class)->find($event_temp);
                    // $event_existant = $entityManager->getRepository(EventProvider::class)->findOneBy(array('event' => $event, 'provider' => $provider));
                    $transfers = new EventProvider();
                    // if (!$event_existant)
                    // {
                    $transfers->setZoomNumber($zoom_number);
                    $transfers->setZoomCode($zoom_code);
                    $transfers->setEvent($event);
                    $transfers->setProvider($provider);
                    $entityManager->persist($transfers);
                    $entityManager->flush();
                    // }
                }
            }
        }

        $this->session->getFlashBag()->add('success', 'Datos importados satisfactoriamente');

        return $this->redirectToRoute('event_edit', ['id' => $ev_id]);
    }


    public function event_provider_save(Request $request, $event_id)
    {
        $id            = $request->request->get('id');
        $dat           = $request->request->get('dat');
        $entityManager = $this->getDoctrine()->getManager();

        $event    = $this->getDoctrine()->getRepository(Event::class)->find($event_id);
        $provider = $this->getDoctrine()->getRepository(Provider::class)->find($dat['provider_id']);

        if ($id)
            $ev_provider = $this->getDoctrine()->getRepository(EventProvider::class)->find($id);
        else
            $ev_provider = new EventProvider();

        $ev_provider->setEvent($event);
        $ev_provider->setProvider($provider);
        $ev_provider->setZoomCode($dat['zoom_code']);
        $ev_provider->setZoomNumber($dat['zoom_number']);
        $ev_provider->setRegion($dat['region']);

        $entityManager->persist($ev_provider);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    public function event_provider_delete(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $evt_provider  = $this->getDoctrine()->getRepository(EventProvider::class)->find($id);


        $entityManager->remove($evt_provider);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    public function loadProvider($prov_nit)
    {
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM provider WHERE nit LIKE '%$prov_nit%'");
        $statement->execute();
        $provider_temp = $statement->fetchAll();
        if ($provider_temp) {
            return $provider_temp[0]['id'];
        } else {
            return false;
        }
    }

    public function loadEvent($event_id)
    {
        $event_id = intval($event_id);
        $em         = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM event WHERE id = $event_id");
        $statement->execute();
        $event_temp = $statement->fetchAll();

        if ($event_temp) {
            return $event_temp[0]['id'];
        } else {
            return false;
        }
    }

    public function event_provider_get($ev_provider_id)
    {
        $event_prov = $this->getDoctrine()->getRepository(EventProvider::class)->find($ev_provider_id);

        return $this->json([
            'id'          => $ev_provider_id,
            'provider_id' => $event_prov->getProvider()->getId(),
            'zoom_code'   => $event_prov->getZoomCode(),
            'zoom_number' => $event_prov->getZoomNumber(),
            'region'      => $event_prov->getRegion()
        ]);
    }

    /**
     * @Route("admin/event/show_event_deleted", name="show_event_deleted")
     */
    public function showEventDeleted(Request $request): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $table = $this->createDataTable()
            ->add('id', TextColumn::class, ['field' => 'e.id', 'label' => '#', 'className' => 'w-20 text-center'])
            ->add('name', TextColumn::class, ['field' => 'e.title', 'label' => 'Nombre evento'])
            ->add('nit', DateTimeColumn::class, ['field' => 'e.start_date', 'label' => 'Fecha inicio', 'render' => function ($date) {
                return sprintf(date('F j, Y', strtotime($date)));
            }])
            ->add('representant', DateTimeColumn::class, ['field' => 'e.final_date', 'label' => 'Fecha finalización', 'render' => function ($date) {
                return sprintf(date('F j, Y', strtotime($date)));
            }])
            ->add('email', TextColumn::class, ['field' => 'e.percent', 'label' => 'Meta Proveedores', 'className' => 'text-center'])
            ->add('phone', TextColumn::class, ['field' => 'e.description', 'label' => 'Descripción'])
            ->add('options', TextColumn::class, ['label' => 'Opciones', 'className' => 'w-120', 'field' => 'e.id', 'render' => function ($event_id) {
                return sprintf('<a class="btn btn-sm btn-success" href="' . $this->generateUrl('restore_event', ['id' => $event_id]) . '">Restaurar</a>');
            }])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Event::class,
                'query'  => function (QueryBuilder $builder) {
                    $builder->select('e')->from(Event::class, 'e')
                        ->where('e.deleted_at IS NOT NULL')
                        ->orderBy('e.start_date', 'ASC')
                        ->orderBy('e.id', 'ASC');
                }
            ])
            ->handleRequest($request);

        if ($table->isCallback())
            return $table->getResponse();

        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/event/deleted.html.twig', [
            'datatable' => $table,
            'message'   => $message
        ]);

        $filters->enable('soft_deleteable');
    }

    /**
     * @Route("admin/event/restore_event/{id}", name="restore_event")
     */
    public function restoreEventsDeleted($id): Response
    {
        $em      = $this->getDoctrine()->getManager();
        $filters = $em->getFilters();
        $filters->disable('soft_deleteable');

        $entityManager = $this->getDoctrine()->getManager();
        $event         = $this->getDoctrine()->getRepository(Event::class)->find($id);
        $event->setDeletedAt(NULL);

        $event->setDeletedAt(NULL);
        $event->setUpdatedById($this->getUser()->getId());
        $event->setDeletedById(NULL);

        $entityManager->persist($event);


        $entityManager->flush();
        $filters->enable('soft_deleteable');
        $this->session->getFlashBag()->add('success', 'Evento activado satisfactoriamente');

        return $this->redirectToRoute('show_event_deleted');
        //return new Response(Response::HTTP_ACCEPTED);
    }
}
