<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\BannerImage;
use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class BannerController extends AbstractController
{
    private $session;

    public function __construct()
    {
        $this->session = new Session(new NativeSessionStorage(), new AttributeBag());
    }

    /**
     * @Route("/admin/banner", name="banner")
     */
    public function index(): Response
    {
        $images = $this->getDoctrine()->getRepository(BannerImage::class)->findBy(['banner' => 1], ['position' => 'ASC']);
        
        /*Show flash message*/
        $message = '';

        foreach ($this->session->getFlashBag()->get('success', []) as $msg)
            $message .= $msg;

        return $this->render('admin/banner/index.html.twig', [
            'images'    => $images,
            'message'   => $message,
            'banner_id' => 1
        ]);
    }

    /**
     * @Route("/banner/upload/{banner_id?}", name="upload_banner")
     */
    public function uploadFile(Request $request, MaelInterventionImageManager $imageManager, $banner_id)
    {
        if ($request->files->get('file'))
        {
            $entityManager = $this->getDoctrine()->getManager();

            $filename = date('Ymdhis').rand(0, 9);
            $img      = $imageManager->make($request->files->get('file'));
            $mimes    = ['image/jpeg' => 'jpg',
                         'image/png'  => 'png',
                         'image/gif'  => 'gif'];

            $ext = $mimes[$img->mime];

            $img->fit(1920, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('upload/banner/b'.$filename.'.'.$ext);

            $img->fit(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('upload/banner/s'.$filename.'.'.$ext);

            if ($banner_id > 0)
                $banner = $this->getDoctrine()->getRepository(Banner::class)->find($banner_id);
            else
            {
                $banner = new Banner();
                $banner->setName('Event Banner');
                $entityManager->persist($banner);
                $entityManager->flush();

                $banner_id = $banner->getId();
            }

            $image = new BannerImage();
            $image->setFilename($filename.'.'.$ext);
            $image->setBanner($banner);

            $entityManager->persist($image);
            $entityManager->flush();

            return $this->json(['banner_id' => $banner_id, 'id' => $image->getId(), 'filename' => $filename.'.'.$ext, 'filepath' => 'upload/banner/s'.$filename.'.'.$ext]);
        }
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    public function getFile($id)
    {
        $img = $this->getDoctrine()->getRepository(BannerImage::class)->find($id);

        return $this->json(['id' => $img->getId(), 'link' => $img->getLink(), 'description' => $img->getDescription()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    public function updateFile(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $data          = $request->request->get('dat');

        $img = $this->getDoctrine()->getRepository(BannerImage::class)->find($id);

        $img->setDescription($data['description']);
        $img->setLink($data['link']);

        $entityManager->persist($img);
        $entityManager->flush();

        return $this->json(['id' => $img->getId(), 'link' => $img->getLink(), 'description' => $img->getDescription()]);
    }

    /**
     * @param $id
     * @return Response
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    public function deleteFile($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $banner_img    = $this->getDoctrine()->getRepository(BannerImage::class)->find($id);
        $entityManager->remove($banner_img);
        $entityManager->flush();

        return new Response(Response::HTTP_ACCEPTED);
    }

    /**
     * @param $id
     * @return Response
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    public function sortFiles(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $images        = $request->request->get('data');

        foreach ($images as $key => $img_id)
        {
            $img = $this->getDoctrine()->getRepository(BannerImage::class)->find($img_id);

            $img->setPosition(intval($key) + 1);

            $entityManager->persist($img);
            $entityManager->flush();
        }

        return new Response(Response::HTTP_ACCEPTED);
    }
}
