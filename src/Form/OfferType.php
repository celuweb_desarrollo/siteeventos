<?php

namespace App\Form;

use App\Entity\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apply')
            ->add('discount')
            ->add('file_url')
            ->add('createdAt', HiddenType::class)
            ->add('updatedAt', HiddenType::class)
            ->add('deletedAt', HiddenType::class)
            ->add('createdById', HiddenType::class)
            ->add('updatedById', HiddenType::class)
            ->add('deletedById', HiddenType::class)//->add('provider_id')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => Offer::class,
                               ]);
    }
}
