<?php

namespace App\Form;

use App\Entity\Associated;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssociatedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_id_id')
            ->add('name')
            ->add('code')
            ->add('created_at')
            ->add('updated_at')
            ->add('deleted_at')
            ->add('created_by_id')
            ->add('updated_by_id')
            ->add('deleted_by_id')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Associated::class,
        ]);
    }
}
