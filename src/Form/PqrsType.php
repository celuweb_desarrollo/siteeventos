<?php

namespace App\Form;

use App\Entity\Pqrs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class PqrsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nombre: ',
                'attr'  => [
                    'class' => 'form-control ',
                    'html5' => FALSE,
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email: ',
                'attr'  => [
                    'class' => 'form-control ',
                    'html5' => FALSE,
                ],
            ])
            ->add('phone', TextType::class, ['label' => 'Teléfono: '])
            ->add('message', TextType::class, ['label' => 'Mensaje: '])
            ->add('date', DateType::class, [
                'label'    => 'Fecha registro :',
                'required' => TRUE,
                'widget'   => 'single_text',
                'attr'     => [
                    'class'        => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5'        => FALSE,
                ],
            ])
            ->add('type', IntegerType::class, ['label' => 'Tipo PQRS: '])
            ->add('created_at', HiddenType::class)
            ->add('updated_at', HiddenType::class)
            ->add('deleted_at', HiddenType::class)
            ->add('created_by_id', HiddenType::class)
            ->add('updated_by_id', HiddenType::class)
            ->add('deleted_by_id', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => Pqrs::class,
                               ]);
    }
}
