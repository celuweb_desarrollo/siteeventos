<?php

namespace App\Form;

use App\Entity\Premiaton;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PremiatonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('provider_id_id')
            ->add('logo')
            ->add('date', DateTimeType::class, [
                'label'    => 'Fecha Premiaton :',
                'required' => TRUE,
                'widget'   => 'single_text',
                'attr'     => [
                    'class'        => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5'        => FALSE,
                ],
            ])
            ->add('created_at', HiddenType::class)
            ->add('updated_at', HiddenType::class)
            ->add('deleted_at', HiddenType::class)
            ->add('created_by_id', HiddenType::class)
            ->add('updated_by_id', HiddenType::class)
            ->add('deleted_by_id', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => Premiaton::class,
                               ]);
    }
}
