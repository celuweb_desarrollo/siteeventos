<?php

namespace App\Form;

use App\Entity\Winner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WinnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('premiaton_id_id')
            ->add('associated_id_id')
            ->add('created_at', HiddenType::class)
            ->add('deleted_at', HiddenType::class)
            ->add('created_by_id', HiddenType::class)
            ->add('updated_by_id', HiddenType::class)
            ->add('delete_by_id', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => Winner::class,
                               ]);
    }
}
