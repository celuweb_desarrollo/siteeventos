<?php

namespace App\Form;

use App\Entity\EventProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nombre evento: ',
                'attr'  => [
                    'class' => 'form-control ',
                    'html5' => FALSE,
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Descripcion : ',
                'attr'  => [
                    'class' => 'form-control ',
                    'html5' => FALSE,
                ],
            ])
            ->add('start_date', DateType::class, [
                'label'    => 'Fecha Inicio :',
                'required' => TRUE,
                'widget'   => 'single_text',
                'attr'     => [
                    'class'        => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5'        => FALSE,
                ],
            ])
            ->add('finish_date', DateType::class, [
                'label'    => 'Fecha Final :',
                'required' => TRUE,
                'widget'   => 'single_text',
                'attr'     => [
                    'class'        => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5'        => FALSE,
                ],
            ])
            ->add('percent', TextType::class, [
                'label' => 'Porcentaje : ',
                'attr'  => [
                    'class' => 'form-control ',
                    'html5' => FALSE,
                ],
            ])
            ->add('created_at', HiddenType::class)
            ->add('updated_at', HiddenType::class)
            ->add('deleted_at', HiddenType::class)
            ->add('createdById', HiddenType::class)
            ->add('updatedById', HiddenType::class)
            ->add('deletedById', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => EventProvider::class,
                               ]);
    }
}
