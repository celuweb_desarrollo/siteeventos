<?php

namespace App\Repository;

use App\Entity\UserContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserContent[]    findAll()
 * @method UserContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserContent::class);
    }

    public  function findByEventAndUser($event_id, $user_id){

        return $this->createQueryBuilder('uc')
            ->select('uc.id')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user_id)
            ->andWhere('e.id = :event_id')
            ->setParameter('event_id', $event_id)
            ->join('uc.event', 'e')
            ->join('uc.user', 'u')
            ->getQuery()
            ->getOneOrNullResult();
    }


    // /**
    //  * @return UserContent[] Returns an array of UserContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserContent
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
