<?php

namespace App\Repository;

use App\Entity\IncentiveProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @method IncentiveProvider|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method IncentiveProvider|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method IncentiveProvider[]    findAll()
 * @method IncentiveProvider[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class IncentiveProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncentiveProvider::class);
    }

    /**
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    function validate_banner()
    {
        $session          = new Session();
        $event_id         = $session->get('event_id');
        $incentive_banner = $this->findOneBy(['event' => $event_id], ['id' => 'DESC']);

        if (!$session->has('viewed_incentive_banner'))
        {
            $viewed_incentive_banners = [];
            $session->set('viewed_incentive_banner', $viewed_incentive_banners);
        } else
            $viewed_incentive_banners = $session->get('viewed_incentive_banner');

        if($incentive_banner){

            if (!in_array($incentive_banner->getLogo(), $viewed_incentive_banners))
            {
                $session->set('banner_incentive', $incentive_banner->getLogo());
                array_push($viewed_incentive_banners, $incentive_banner->getLogo());
                $session->set('viewed_incentive_banner', $viewed_incentive_banners);
            } else
                $session->set('banner_incentive', '');
        }


    }
    // /**
    //  * @return IncentiveProvider[] Returns an array of IncentiveProvider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IncentiveProvider
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
