<?php

namespace App\Repository;

use App\Entity\WinnersProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WinnersProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method WinnersProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method WinnersProvider[]    findAll()
 * @method WinnersProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WinnersProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WinnersProvider::class);
    }

    // /**
    //  * @return WinnersProvider[] Returns an array of WinnersProvider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WinnersProvider
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
