<?php

namespace App\Repository;

use App\Entity\ProviderUserEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderUserEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderUserEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderUserEvent[]    findAll()
 * @method ProviderUserEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderUserEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderUserEvent::class);
    }

    public function findUserProvider($event_id, $user_id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.event = :event_id')
            ->andWhere('p.user = :user_id')
            ->setParameter('event_id', $event_id)
            ->setParameter('user_id', $user_id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getProvidersVisitedOne($event_id, $provider_id, $user_id){

        return $this->createQueryBuilder('e')
                        ->select('e.id')
                        ->andWhere('e.event = :event_id')
                        ->andWhere('e.provider = :provider_id')
                        ->andWhere('e.user = :user_id')
                        ->setParameter('event_id', $event_id)
                        ->setParameter('provider_id', $provider_id)
                        ->setParameter('user_id', $user_id)
                        ->getQuery()
                        ->getOneOrNullResult();

    }

    // /**
    //  * @return ProviderUserEvent[] Returns an array of ProviderUserEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderUserEvent
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
