<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Event|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method Event|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function all()
    {
        return $this->createQueryBuilder('e')
            ->select('e.id', 'e.title', 'e.start_date', 'e.final_date')
            ->andWhere('e.active = :status')
            ->setParameter('status', 1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findEvent($id)
    {
        return $this->createQueryBuilder('e')
            ->select('e.id', 'e.title', 'e.start_date', 'e.final_date')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function allEventsActive(){

        $date = new \DateTime('now');

        return $this->createQueryBuilder('e')
            ->andWhere('e.active = :status')
            ->setParameter('status', 1)
            ->andWhere('e.final_date >= :dateF')
            ->setParameter('dateF', $date->format('Y-m-d').' 23:59:59')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
