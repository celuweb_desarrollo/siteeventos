<?php

namespace App\Repository;

use App\Entity\Offer;
use App\Entity\Provider;
use App\Entity\EventProvider;
use App\Entity\ProviderUserEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventProvider|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method EventProvider|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method EventProvider[]    findAll()
 * @method EventProvider[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class EventProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventProvider::class);
    }

    // /**
    //  * @return EventProvider[] Returns an array of EventProvider objects
    //  */

    public function findByEvent($event_id)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.event = :val')
            ->setParameter('val', $event_id)
            ->getQuery()
            ->getResult();
    }


    public function findById($evt_provider_id)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.id = :val')
            ->setParameter('val', $evt_provider_id)
            ->getQuery()
            ->getResult();
    }


    public function getEventProviders($event_id)
    {
        $providers = $this->createQueryBuilder('e')
            ->select('e.id')
            ->where('e.event = :event_id')
            ->setParameter('event_id', $event_id)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($providers as $pro)
            array_push($result, $pro['id']);

        return $result;
    }

    /**
     * @param $event_id
     * @return array
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     */
    public function getProvidersByName($event_id)
    {
        $providers = $this->createQueryBuilder('e')
            ->select('p.name')
            ->leftJoin(Provider::class, 'p', 'WITH', 'e.provider = p')
            ->where('e.event = :event_id')
            ->setParameter('event_id', $event_id)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($providers as $pro)
            array_push($result, $pro['name']);

        return $result;
    }

    public function getProviders($event_id, $user_id)
    {

        $buil =  $this->createQueryBuilder('e')
            ->select('p.id', 'p.name', 'p.phone', 'p.image')
            ->addSelect('d.id AS visited')
            ->addSelect('o.id AS offer')
            ->addSelect('e.zoom_code', 'e.zoom_number')
            ->andWhere('e.event = :event_id')
            ->setParameter('event_id', $event_id)
            ->leftJoin(Provider::class, 'p', 'WITH', 'e.provider = p')
            ->leftJoin(
                ProviderUserEvent::class,
                'd',
                'WITH',
                'd.event = '.$event_id.' AND d.provider = e.provider AND d.user = '.$user_id)
            ->leftJoin(
                Offer::class,
                'o',
                'WITH',
                'o.provider = e.provider AND o.event = e.event')
            ->distinct('p.id', 'd.provider_id')
            ->getQuery();

        dd($buil);

    }

    public function getProvidersSearch($event_id, $user_id, $search)
    {

        return $this->createQueryBuilder('e')
            ->select('p.id', 'p.name', 'p.phone', 'p.image')
            ->addSelect('d.id AS visited')
            ->addSelect('e.zoom_code', 'e.zoom_number')
            ->addSelect('o.id AS offer')
            ->andWhere('e.event = :event_id')
            ->andWhere('p.name LIKE :search')
            ->setParameter('event_id', $event_id)
            ->setParameter('search', '%'.$search.'%')
            ->leftJoin(Provider::class, 'p', 'WITH', 'e.provider = p')
            ->leftJoin(
                ProviderUserEvent::class,
                'd',
                'WITH',
                'd.event = '.$event_id.' AND d.provider = e.provider AND d.user = '.$user_id)
            ->leftJoin(
                Offer::class,
                'o',
                'WITH',
                'o.provider = e.provider AND o.event = e.event')
            ->getQuery()
            ->getResult();
    }


     public function findOneById($value): ?EventProvider
     {
         return $this->createQueryBuilder('e')
             ->select('p.id', 'p.name', 'p.phone', 'p.image', 'p.email')
             ->addSelect('e.zoom_code', 'e.zoom_number')
             ->andWhere('e.id = :id')
             ->leftJoin(Provider::class, 'p', 'WITH', 'e.provider_id = p')
             ->setParameter('id', $value)
             ->getQuery()
             ->getOneOrNullResult();
     }


     public function findProviderByEventProvider($value){

         return $this->createQueryBuilder('e')
             ->select('p.id', 'p.name', 'p.phone', 'p.image', 'p.email')
             ->addSelect('e.zoom_code', 'e.zoom_number')
             ->andWhere('e.id = :id')
             ->setParameter('id', $value)
             ->leftJoin(Provider::class, 'p', 'WITH', 'e.provider = p')
             ->getQuery()
             ->getOneOrNullResult();
     }


    /*
    public function findOneBySomeField($value): ?EventProvider
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
