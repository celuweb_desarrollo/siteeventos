<?php

namespace App\Repository;

use App\Entity\Banner;
use App\Entity\BannerImage;
use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BannerImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method BannerImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method BannerImage[]    findAll()
 * @method BannerImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BannerImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BannerImage::class);
    }


     /**
      * @return BannerImage[] Returns an array of BannerImage objects
      */
    public function findByEventStatus($status = 1)
    {
        return $this->createQueryBuilder('bi')
            ->andWhere('ev.active = :status')
            ->setParameter('status', $status)
            ->innerJoin(Banner::class, 'b', 'WITH', 'bi.banner = b')
            ->innerJoin(Event::class, 'ev', 'WITH', 'ev.banner = b')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return BannerImage[] Returns an array of BannerImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BannerImage
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
