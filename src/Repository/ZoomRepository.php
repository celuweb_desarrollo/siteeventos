<?php

namespace App\Repository;

use App\Entity\Zoom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Zoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zoom[]    findAll()
 * @method Zoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zoom::class);
    }

    // /**
    //  * @return Zoom[] Returns an array of Zoom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Zoom
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
