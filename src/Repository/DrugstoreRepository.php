<?php

namespace App\Repository;

use App\Entity\Drugstore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Drugstore|null find($id, $lockMode = null, $lockVersion = null)
 * @method Drugstore|null findOneBy(array $criteria, array $orderBy = null)
 * @method Drugstore[]    findAll()
 * @method Drugstore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DrugstoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Drugstore::class);
    }

    public function findByCode($code)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.code_drugstore = :val')
            ->setParameter('val', $code)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Drugstore[] Returns an array of Drugstore objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Drugstore
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
