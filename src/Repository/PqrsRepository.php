<?php

namespace App\Repository;

use App\Entity\Pqrs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pqrs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pqrs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pqrs[]    findAll()
 * @method Pqrs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PqrsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pqrs::class);
    }

    // /**
    //  * @return Pqrs[] Returns an array of Pqrs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pqrs
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
