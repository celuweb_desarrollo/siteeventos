<?php

namespace App\Repository;

use App\Entity\EventProvider;
use App\Entity\Provider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Provider|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method Provider|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method Provider[]    findAll()
 * @method Provider[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class ProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Provider::class);
    }

    public function findProvider($id, $event_id)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id', 'p.name', 'p.phone', 'p.image', 'p.email')
            ->addSelect('ev.zoom_code', 'ev.zoom_number')
            ->leftJoin(EventProvider::class, 'ev', 'WITH', 'ev.provider = p AND ev.event = '.$event_id)
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

    }

    public function getByExcept($excepts_id)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id', 'p.name')
            // ->where('p.id NOT IN ('.join(',', $excepts_id).')')
            ->orderBy('p.name', 'asc')
            ->getQuery()
            ->getResult();

        // ->leftJoin(EventProvider::class, 'ep', 'WITH', 'ep.provider = p AND ep.event = '.$event_id)
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findByEmail($value, $id)
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.email = :val')
            ->setParameter('val', $value);

        if ($id > 0)
        {

            $query->andWhere('p.id != :id')
                ->setParameter('id', $id);
        }

        return $query->getQuery()->setMaxResults(1)->getResult();
    }


    public function getProviderOfEventProvider($provider_id)
    {
        return $this->createQueryBuilder('ep')
            ->select('p.id', 'p.name', 'p.phone', 'p.image', 'p.email')
            ->addSelect('ep.zoom_code', 'ep.zoom_number')
            ->andWhere('ep.provider_id = :provider_id')
            ->setParameter('provider_id', $provider_id)
            ->leftJoin(Provider::class, 'p', 'WITH', 'ep.provider_id = p')
            ->getQuery()
            ->getOneOrNullResult();
    }

//
//    public function findProviderByEventProvider($event_provider_id)
//    {
//        return $this->createQueryBuilder('p')
//            ->select('p.id', 'p.name', 'p.phone', 'p.image', 'p.email')
//            ->addSelect('ev.zoom_code', 'ev.zoom_number')
//            ->andWhere('ev.provider_id = :id')
//            ->setParameter('p.id', $event_provider_id)
//            ->leftJoin(EventProvider::class, 'ev', 'WITH', 'ev.provider = p')
//            ->getQuery()
//            ->getOneOrNullResult();
//    }
    // /**
    //  * @return Provider[] Returns an array of Provider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Provider
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
