<?php

namespace App\Repository;

use App\Entity\Premiaton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @method Premiaton|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method Premiaton|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method Premiaton[]    findAll()
 * @method Premiaton[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class PremiatonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Premiaton::class);
    }

    public function all()
    {

        return $this->createQueryBuilder('p')
            ->select('p.start_date', 'e.title', 'e.percent')
            ->join('p.event_id', 'e')
            ->join('p.event_id', 'e')
            ->getQuery()
            ->getResult();

    }

    /**
     * Created by  <Ingeniero>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <ingsebasalvarez@gmail.com>
     * Date:
     */
    function validate_banner()
    {
        $session          = new Session();
        $event_id         = $session->get('event_id');
        $premiaton_banner = $this->findOneBy(['event_id' => $event_id], ['id' => 'DESC']);

        if (!$session->has('viewed_banner'))
        {
            $viewed_banners = [];
            $session->set('viewed_banner', $viewed_banners);

        } else{
            $viewed_banners = $session->get('viewed_banner');
        }

        if ($premiaton_banner && !in_array($premiaton_banner->getLogo(), $viewed_banners))
        {
            $session->set('banner_image', $premiaton_banner->getLogo());
            array_push($viewed_banners, $premiaton_banner->getLogo());
            $session->set('viewed_banner', $viewed_banners);
        } else
            $session->set('banner_image', '');
    }

    // /**
    //  * @return Premiaton[] Returns an array of Premiaton objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Premiaton
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
