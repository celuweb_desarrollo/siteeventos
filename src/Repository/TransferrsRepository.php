<?php

namespace App\Repository;

use App\Entity\Transferrs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transferrs|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method Transferrs|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method Transferrs[]    findAll()
 * @method Transferrs[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class TransferrsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transferrs::class);
    }

    public function findByProvider($provider_id)
    {
        return $this->createQueryBuilder('t')
            ->select('t.transfer', 't.phone')
            ->andWhere('t.provider = :provider_id')
            ->setParameter('provider_id', $provider_id)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Transferrs[] Returns an array of Transferrs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transferrs
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
