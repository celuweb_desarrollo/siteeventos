<?php

namespace App\Repository;

use App\Entity\Associated;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Associated|null find($id, $lockMode = null, $lockVersion = null)
 * @method Associated|null findOneBy(array $criteria, array $orderBy = null)
 * @method Associated[]    findAll()
 * @method Associated[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssociatedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Associated::class);
    }

    public function findById($id)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.user = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Associated[] Returns an array of Associated objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Associated
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
