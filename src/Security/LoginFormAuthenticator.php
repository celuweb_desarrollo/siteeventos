<?php

namespace App\Security;

use App\Entity\Associated;
use App\Entity\Drugstore;
use App\Entity\Profile;
use App\Entity\Session;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class LoginFormAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE           = 'app_login';
    public const LOGIN_ASSOCIATED      = 'associate_login';
    private const AUTHENTICATION_LOGIN = '_security.route_login';
    private const URL_API              = 'https://sipasociados.coopidrogas.com.co/api_third_parties/login_check';

    private $client;
    private $entityManager;
    private $urlGenerator;
    private $session;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(HttpClientInterface $client, SessionInterface $session, EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager    = $entityManager;
        $this->urlGenerator     = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder  = $passwordEncoder;
        $this->client           = $client;
        $this->session          = $session;
    }

    public function supports(Request $request)
    {
        $redirect = $request->attributes->get('_route') == self::LOGIN_ASSOCIATED ? false : true;

        return ($redirect
                        ? self::LOGIN_ROUTE
                        : self::LOGIN_ASSOCIATED) === $request->attributes->get('_route') && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'email'      => $request->request->get('email'),
            'password'   => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
            'form'       => $request->request->get('form'),
        ];

        $request->getSession()->set(Security::LAST_USERNAME, $credentials['email']);

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        $form  = $credentials['form'] == self::LOGIN_ASSOCIATED ? true : false;

        if (!$this->csrfTokenManager->isTokenValid($token))
            throw new InvalidCsrfTokenException();

        $user = $this->entityManager->getRepository(User::class)->findByRoleAndEmail($credentials['email'], ($form ? 'ROLE_ASSOCIATED' : 'ROLE_ADMIN') );

        if($form && !$user) {

            $drugstore  = $this->entityManager->getRepository(Drugstore::class)->findOneBy(['code_drugstore' => $credentials['email']]);

            if(isset($drugstore)){

                $user       = new User();
                $profile    = new Profile();
                $associated = new Associated();
                $encoded    = $this->passwordEncoder->encodePassword($user, $credentials['email']);

                $profile->setIdentification('00000');
                $profile->setFullName($drugstore->getNameDrugstore());
                $profile->setPhone('1234567');

                $user->setEmail($drugstore->getCodeDrugstore());
                $user->setSessionToken($token);
                $user->setPassword($encoded);
                $user->setRoles(['ROLE_ASSOCIATED']);
                $user->setCreatedAt(new \DateTime('now'));
                $user->setUpdatedAt(new \DateTime('now'));
                $user->setIsActive(1);
                $user->setIsSuperAdmin(0);
                $user->setIsVerified(1);
                $user->setProfile($profile);

                $associated->setName($drugstore->getNameDrugstore());
                $associated->setCode($drugstore->getCodeDrugstore());
                $associated->setCreatedAt(new \DateTime('now'));
                $associated->setUpdatedAt(new \DateTime('now'));
                $associated->setUser($user);

                $this->entityManager->persist($user);
                $this->entityManager->persist($profile);
                $this->entityManager->persist($associated);
                $this->entityManager->flush();

            }else{
                throw new CustomUserMessageAuthenticationException('No se encontró el usuario en la base de datos. Por favor comunícate con tu asesor.');// fail authentication with a custom error
            }

//            $response = $this->client->request('POST', self::URL_API,[
//                'headers' => [
//                    'Accept'        => 'application/json',
//                    'Content-Type'  => 'application/json'
//                ],
//                'body'    =>'{
//                    "username" : "'.$credentials['email'].'",
//                    "password" : "'.$credentials['password'].'"
//                }'
//            ]);
//
//            if($response->getStatusCode() == 200 ){
//
//                $token      = $response->toArray()['token'];
//                $info       = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $token)[1]))));
//                $dat        = $info->info_basica;
//                $associated = $this->entityManager->getRepository(Associated::class)->findOneBy(['code' => $info->info_basica->codigo]);

//
//                if( ! isset($associated) ){
//
//                    $user       = new User();
//                    $profile    = new Profile();
//                    $associated = new Associated();
//                    $encoded    = $this->passwordEncoder->encodePassword($user, $credentials['password']);
//
//                    $profile->setIdentification($dat->nit);
//                    $profile->setFullName($dat->asociado);
//                    $profile->setPhone($dat->telefono);
//
//                    $user->setEmail($dat->codigo);
//                    $user->setSessionToken($token);
//                    $user->setPassword($encoded);
//                    $user->setRoles(['ROLE_ASSOCIATED']);
//                    $user->setCreatedAt(new \DateTime('now'));
//                    $user->setUpdatedAt(new \DateTime('now'));
//                    $user->setIsActive(1);
//                    $user->setIsSuperAdmin(0);
//                    $user->setIsVerified(1);
//                    $user->setProfile($profile);
//
//                    $associated->setName($dat->asociado);
//                    $associated->setCode($dat->codigo);
//                    $associated->setCreatedAt(new \DateTime('now'));
//                    $associated->setUpdatedAt(new \DateTime('now'));
//                    $associated->setUser($user);
//
//                    $this->entityManager->persist($user);
//                    $this->entityManager->persist($profile);
//                    $this->entityManager->persist($associated);
//                    $this->entityManager->flush();
//                }
//            }
        }

        if (! ($user??null)) {
            throw new CustomUserMessageAuthenticationException('Credenciales inválidas.');// fail authentication with a custom error
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if ( $credentials['form'] == 'associate_login' )
            return true;

        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if($request->attributes->get('_route') == self::LOGIN_ASSOCIATED && in_array('ROLE_ASSOCIATED', $token->getUser()->getRoles()) ){
            $request->getSession()->set(self::AUTHENTICATION_LOGIN, 'associated');
            return new RedirectResponse($this->urlGenerator->generate('associate_events'));
        }

        $tokenSession = md5(uniqid(microtime(), true));
        $request->getSession()->set('token_session', $tokenSession);
        $session      = new Session();
        $session->setUser($token->getUser());
        $session->setStart(new \DateTime('now'));
        $session->setInSession(1);
        $session->setTokenSession($tokenSession);
        $this->entityManager->persist($session);
        $this->entityManager->flush();

        $request->getSession()->set(self::AUTHENTICATION_LOGIN, 'admin');
        return new RedirectResponse($this->urlGenerator->generate('event_index'));
    }

    /**
     * Override to change what happens after a bad username/password is submitted.
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->hasSession()) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        }

       $url = $this->urlGenerator->generate($request->attributes->get('_route') == self::LOGIN_ASSOCIATED
                                                                                                  ? self::LOGIN_ASSOCIATED
                                                                                                  : self::LOGIN_ROUTE);
        return new RedirectResponse($url);
    }


    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
